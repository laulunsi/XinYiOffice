﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:AdvicePayment
	/// </summary>
	public partial class AdvicePayment
	{
		public AdvicePayment()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "AdvicePayment"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from AdvicePayment");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.AdvicePayment model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into AdvicePayment(");
			strSql.Append("DocumentNumber,DocumentDescription,MethodPayment,SupplierName,ClientName,PayeeFullName,PayeeAccountId,PaymentDate,TargetDate,Remark,PaymentAmount,PaymentBankAccountId,Sate,CreateAccountId,CreateTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@DocumentNumber,@DocumentDescription,@MethodPayment,@SupplierName,@ClientName,@PayeeFullName,@PayeeAccountId,@PaymentDate,@TargetDate,@Remark,@PaymentAmount,@PaymentBankAccountId,@Sate,@CreateAccountId,@CreateTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@DocumentNumber", SqlDbType.VarChar,4000),
					new SqlParameter("@DocumentDescription", SqlDbType.VarChar,4000),
					new SqlParameter("@MethodPayment", SqlDbType.Int,4),
					new SqlParameter("@SupplierName", SqlDbType.VarChar,4000),
					new SqlParameter("@ClientName", SqlDbType.VarChar,4000),
					new SqlParameter("@PayeeFullName", SqlDbType.VarChar,4000),
					new SqlParameter("@PayeeAccountId", SqlDbType.Int,4),
					new SqlParameter("@PaymentDate", SqlDbType.DateTime),
					new SqlParameter("@TargetDate", SqlDbType.DateTime),
					new SqlParameter("@Remark", SqlDbType.VarChar,4000),
					new SqlParameter("@PaymentAmount", SqlDbType.Float,8),
					new SqlParameter("@PaymentBankAccountId", SqlDbType.Int,4),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.DocumentNumber;
			parameters[1].Value = model.DocumentDescription;
			parameters[2].Value = model.MethodPayment;
			parameters[3].Value = model.SupplierName;
			parameters[4].Value = model.ClientName;
			parameters[5].Value = model.PayeeFullName;
			parameters[6].Value = model.PayeeAccountId;
			parameters[7].Value = model.PaymentDate;
			parameters[8].Value = model.TargetDate;
			parameters[9].Value = model.Remark;
			parameters[10].Value = model.PaymentAmount;
			parameters[11].Value = model.PaymentBankAccountId;
			parameters[12].Value = model.Sate;
			parameters[13].Value = model.CreateAccountId;
			parameters[14].Value = model.CreateTime;
			parameters[15].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.AdvicePayment model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update AdvicePayment set ");
			strSql.Append("DocumentNumber=@DocumentNumber,");
			strSql.Append("DocumentDescription=@DocumentDescription,");
			strSql.Append("MethodPayment=@MethodPayment,");
			strSql.Append("SupplierName=@SupplierName,");
			strSql.Append("ClientName=@ClientName,");
			strSql.Append("PayeeFullName=@PayeeFullName,");
			strSql.Append("PayeeAccountId=@PayeeAccountId,");
			strSql.Append("PaymentDate=@PaymentDate,");
			strSql.Append("TargetDate=@TargetDate,");
			strSql.Append("Remark=@Remark,");
			strSql.Append("PaymentAmount=@PaymentAmount,");
			strSql.Append("PaymentBankAccountId=@PaymentBankAccountId,");
			strSql.Append("Sate=@Sate,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@DocumentNumber", SqlDbType.VarChar,4000),
					new SqlParameter("@DocumentDescription", SqlDbType.VarChar,4000),
					new SqlParameter("@MethodPayment", SqlDbType.Int,4),
					new SqlParameter("@SupplierName", SqlDbType.VarChar,4000),
					new SqlParameter("@ClientName", SqlDbType.VarChar,4000),
					new SqlParameter("@PayeeFullName", SqlDbType.VarChar,4000),
					new SqlParameter("@PayeeAccountId", SqlDbType.Int,4),
					new SqlParameter("@PaymentDate", SqlDbType.DateTime),
					new SqlParameter("@TargetDate", SqlDbType.DateTime),
					new SqlParameter("@Remark", SqlDbType.VarChar,4000),
					new SqlParameter("@PaymentAmount", SqlDbType.Float,8),
					new SqlParameter("@PaymentBankAccountId", SqlDbType.Int,4),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.DocumentNumber;
			parameters[1].Value = model.DocumentDescription;
			parameters[2].Value = model.MethodPayment;
			parameters[3].Value = model.SupplierName;
			parameters[4].Value = model.ClientName;
			parameters[5].Value = model.PayeeFullName;
			parameters[6].Value = model.PayeeAccountId;
			parameters[7].Value = model.PaymentDate;
			parameters[8].Value = model.TargetDate;
			parameters[9].Value = model.Remark;
			parameters[10].Value = model.PaymentAmount;
			parameters[11].Value = model.PaymentBankAccountId;
			parameters[12].Value = model.Sate;
			parameters[13].Value = model.CreateAccountId;
			parameters[14].Value = model.CreateTime;
			parameters[15].Value = model.TenantId;
			parameters[16].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from AdvicePayment ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from AdvicePayment ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.AdvicePayment GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,DocumentNumber,DocumentDescription,MethodPayment,SupplierName,ClientName,PayeeFullName,PayeeAccountId,PaymentDate,TargetDate,Remark,PaymentAmount,PaymentBankAccountId,Sate,CreateAccountId,CreateTime,TenantId from AdvicePayment ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.AdvicePayment model=new XinYiOffice.Model.AdvicePayment();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DocumentNumber"]!=null && ds.Tables[0].Rows[0]["DocumentNumber"].ToString()!="")
				{
					model.DocumentNumber=ds.Tables[0].Rows[0]["DocumentNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["DocumentDescription"]!=null && ds.Tables[0].Rows[0]["DocumentDescription"].ToString()!="")
				{
					model.DocumentDescription=ds.Tables[0].Rows[0]["DocumentDescription"].ToString();
				}
				if(ds.Tables[0].Rows[0]["MethodPayment"]!=null && ds.Tables[0].Rows[0]["MethodPayment"].ToString()!="")
				{
					model.MethodPayment=int.Parse(ds.Tables[0].Rows[0]["MethodPayment"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SupplierName"]!=null && ds.Tables[0].Rows[0]["SupplierName"].ToString()!="")
				{
					model.SupplierName=ds.Tables[0].Rows[0]["SupplierName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ClientName"]!=null && ds.Tables[0].Rows[0]["ClientName"].ToString()!="")
				{
					model.ClientName=ds.Tables[0].Rows[0]["ClientName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PayeeFullName"]!=null && ds.Tables[0].Rows[0]["PayeeFullName"].ToString()!="")
				{
					model.PayeeFullName=ds.Tables[0].Rows[0]["PayeeFullName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PayeeAccountId"]!=null && ds.Tables[0].Rows[0]["PayeeAccountId"].ToString()!="")
				{
					model.PayeeAccountId=int.Parse(ds.Tables[0].Rows[0]["PayeeAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PaymentDate"]!=null && ds.Tables[0].Rows[0]["PaymentDate"].ToString()!="")
				{
					model.PaymentDate=DateTime.Parse(ds.Tables[0].Rows[0]["PaymentDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TargetDate"]!=null && ds.Tables[0].Rows[0]["TargetDate"].ToString()!="")
				{
					model.TargetDate=DateTime.Parse(ds.Tables[0].Rows[0]["TargetDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Remark"]!=null && ds.Tables[0].Rows[0]["Remark"].ToString()!="")
				{
					model.Remark=ds.Tables[0].Rows[0]["Remark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PaymentAmount"]!=null && ds.Tables[0].Rows[0]["PaymentAmount"].ToString()!="")
				{
					model.PaymentAmount=decimal.Parse(ds.Tables[0].Rows[0]["PaymentAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PaymentBankAccountId"]!=null && ds.Tables[0].Rows[0]["PaymentBankAccountId"].ToString()!="")
				{
					model.PaymentBankAccountId=int.Parse(ds.Tables[0].Rows[0]["PaymentBankAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Sate"]!=null && ds.Tables[0].Rows[0]["Sate"].ToString()!="")
				{
					model.Sate=int.Parse(ds.Tables[0].Rows[0]["Sate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,DocumentNumber,DocumentDescription,MethodPayment,SupplierName,ClientName,PayeeFullName,PayeeAccountId,PaymentDate,TargetDate,Remark,PaymentAmount,PaymentBankAccountId,Sate,CreateAccountId,CreateTime,TenantId ");
			strSql.Append(" FROM AdvicePayment ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,DocumentNumber,DocumentDescription,MethodPayment,SupplierName,ClientName,PayeeFullName,PayeeAccountId,PaymentDate,TargetDate,Remark,PaymentAmount,PaymentBankAccountId,Sate,CreateAccountId,CreateTime,TenantId ");
			strSql.Append(" FROM AdvicePayment ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM AdvicePayment ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from AdvicePayment T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "AdvicePayment";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

