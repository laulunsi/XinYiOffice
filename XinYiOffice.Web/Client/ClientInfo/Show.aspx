﻿<%@ Page Language="C#"  MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Client.ClientInfo.Show" Title="显示页" %>
<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

<script>
    $(function () {

        var phon = $("#<%=btnPhone.ClientID%>");
        var id = phon.attr("data-cid");

        phon.click(function () {

            var show = $.layer({
                type: 2,
                border:[2],
                maxmin: true,
                shadeClose: true,
                title: '客户回访',
                shade: [0.1, '#fff'],
                offset: ['20px', ''],
                area: ['600px', '450px'],
                iframe: { src: 'AddClientTracking.aspx?sj=' + Math.random() + '&clientid=' + id }
            });

        });



    })
</script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <form id="form1" runat="server">
<div class="setup_box" style=" margin-bottom:0">
<div class="h_title"><h4>基本信息</h4></div>
   <table cellSpacing="0" cellPadding="0" width="100%" border="0" class="plan_table">
	<tr>
	<td height="25" width="13%" align="right" class="table_left">所属人职员：</td>
	<td height="25" width="36%" align="left">
        <asp:Literal ID="Litera_ByAccountId" runat="server"></asp:Literal>
        </td>
	<td width="25%" align="left" class="table_left">客户编号
	：
        </td>
	<td width="26%" align="left"><asp:Literal ID="Literal_IntNumber" runat="server"></asp:Literal></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		客户名称
	：</td>
	<td height="25" align="left">
        <b style="color:#06C;"><asp:Literal ID="Literal_Name" runat="server"></asp:Literal></b>
        </td>
	<td height="25" align="left" class="table_left">权限属性
	：      </td>
	<td height="25" align="left"><asp:Literal ID="Literal_Power" runat="server"></asp:Literal></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		热度
	：</td>
	<td height="25" align="left">
        <asp:Literal ID="Literal_Heat" runat="server"></asp:Literal>
        </td>
	<td height="25" align="left" class="table_left">信用等级
	：
        </td>
	<td height="25" align="left"><asp:Literal ID="Literal_QualityRating" runat="server"></asp:Literal></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		类型
	：</td>
	<td height="25" align="left">
        <asp:Literal ID="Literal_ClientType" runat="server"></asp:Literal>
        </td>
	<td height="25" align="left" class="table_left">价值评估
	：
        </td>
	<td height="25" align="left"><asp:Literal ID="Literal_ValueAssessment" runat="server"></asp:Literal></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		所属行业
	：</td>
	<td height="25" align="left">
        <asp:Literal ID="Literal_Industry" runat="server"></asp:Literal>
        </td>
	<td height="25" align="left" class="table_left">关系等级
	：
        </td>
	<td height="25" align="left"><asp:Literal ID="Literal_RelBetweenGrade" runat="server"></asp:Literal></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		来源
	：</td>
	<td height="25" align="left">
        <asp:Literal ID="Literal_Source" runat="server"></asp:Literal>
        </td>
	<td height="25" align="left" class="table_left">客户级别
	：
        </td>
	<td height="25" align="left"><asp:Literal ID="Literal_CustomerLevel" runat="server"></asp:Literal></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">标签：</td>
	<td height="25" align="left">
        <asp:Literal ID="Literal_Tag" runat="server"></asp:Literal>
        </td>
	<td height="25" align="left" class="table_left">发展阶段：
        </td>
	<td height="25" align="left"><asp:Literal ID="Literal_Seedtime" runat="server"></asp:Literal></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		开户行
	：</td>
	<td height="25" align="left">
        <asp:Literal ID="Literal_BankDeposit" runat="server"></asp:Literal>
        </td>
	<td height="25" align="left" class="table_left">上级客户
	：
&nbsp; </td>
	<td height="25" align="left"><asp:Literal ID="Literal_SupClientInfoId" runat="server"></asp:Literal></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		纳税号
	：</td>
	<td height="25" align="left">
        <asp:Literal ID="Literal_TaxNo" runat="server"></asp:Literal>
        </td>
	<td height="25" align="left" class="table_left">来源营销计划：
        </td>
	<td height="25" align="left"><asp:Literal ID="Literal_MarketingPlanId" runat="server"></asp:Literal></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">接受短信手机
	：</td>
	<td height="25" colspan="3" align="left">
	  <asp:Literal ID="Literal_MobilePhone" runat="server"></asp:Literal>
	  </td>
	</tr>
	</table>
<div class="h_title"><h4>备注</h4></div>
            <table width="100%" border="0" cellPadding="0" cellSpacing="0" class="plan_table">
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 银行帐号
      ：</td>
    <td height="25" align="left">
        <asp:Literal ID="Literal_BankAccount" runat="server"></asp:Literal>
        </td>
    <td height="25" align="left" class="table_left">省份
      ：      
      </td>
    <td align="left"><asp:Literal ID="Literal_Province" runat="server"></asp:Literal></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 国家地区
      ：</td>
    <td height="25" align="left">
        <asp:Literal ID="Literal_CountriesRegions" runat="server"></asp:Literal>
        </td>
    <td height="25" align="left" class="table_left">城市
      ：      
      </td>
    <td height="25" align="left"><asp:Literal ID="Literal_City" runat="server"></asp:Literal></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 电话
      ：</td>
    <td height="25" align="left">
        <asp:Literal ID="Literal_Phone" runat="server"></asp:Literal>
        &nbsp;&nbsp;<asp:Button ID="btnPhone" runat="server" Text="+添加客户回访" style=" width:120px;"   OnClientClick="return false;" />
        </td>
    <td height="25" align="left" class="table_left">区县
      ：      
      </td>
    <td height="25" align="left"><asp:Literal ID="Literal_County" runat="server"></asp:Literal></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 传真
      ：</td>
    <td height="25" align="left">
        <asp:Literal ID="Literal_Fax" runat="server"></asp:Literal>
        </td>
    <td height="25" align="left" class="table_left">地址
      ：      
      </td>
    <td height="25" align="left"><asp:Literal ID="Literal_Address" runat="server"></asp:Literal></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 邮件
      ：</td>
    <td height="25" colspan="3" align="left">
      <asp:Literal ID="Literal_Email" runat="server"></asp:Literal>
    </td>
    </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 邮编
      ：</td>
    <td height="25" colspan="3" align="left">
      <asp:Literal ID="Literal_ZipCode" runat="server"></asp:Literal>
    </td>
    </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left">公司简介
      ：</td>
    <td height="25" colspan="3" align="left">
        <asp:Literal ID="Literal_CompanyProfile" runat="server"></asp:Literal>
        </td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left">备注
      ：</td>
    <td height="25" colspan="3" align="left">
        <asp:Literal ID="Literal_Remarks" runat="server"></asp:Literal>
        </td>
    </tr>
  
    <tr>
    <td height="25" width="13%" align="right" class="table_left"> 负责人
      ：</td>
    <td height="25" colspan="3" align="left">
        <asp:Literal ID="Literal_GeneralManager" runat="server"></asp:Literal>
        </td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 企业法人
      ：</td>
    <td height="25" colspan="3" align="left">
        <asp:Literal ID="Literal_BusinessEntity" runat="server"></asp:Literal>
        </td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 营业执照
      ：</td>
    <td height="25" colspan="3" align="left">
        <asp:Literal ID="Literal_BusinessLicence" runat="server"></asp:Literal>
        </td>
  </tr>
  
</table>


<div class="h_title"><h4>联系人</h4></div>
<asp:Repeater ID="repClientLiaisons" runat="server">
   <ItemTemplate>
   
<table width="100%" border="0" cellPadding="0" cellSpacing="0" class="plan_table">

  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 联系人：</td>
    <td width="37%" height="25" align="left"><a href="/Client/ClientLiaisons/Show.aspx?id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "FullName")%></a></td>
    <td width="25%" height="25" align="left" class="table_left"> 性别:  </td>
    <td width="25%" align="left"><%#DataBinder.Eval(Container.DataItem, "SexName")%></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 手机号码:</td>
    <td height="25" align="left">&nbsp;&nbsp;&nbsp;</td>
    <td height="25" align="left" class="table_left">固定电话：</td>
    <td height="25" align="left"><%#DataBinder.Eval(Container.DataItem, "OtherPhone")%>&nbsp;</td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> QQ:</td>
    <td height="25" align="left">&nbsp;</td>
    <td height="25" align="left" class="table_left">联系人分类:</td>
    <td height="25" align="left"><%#DataBinder.Eval(Container.DataItem, "ClassiFicationName")%></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 电子邮件：</td>
    <td height="25" colspan="3" align="left"><%#DataBinder.Eval(Container.DataItem, "Email")%></td>
    </tr>

</table>
   </ItemTemplate>
</asp:Repeater>

<div class="h_title"><h4>销售机会</h4></div>

<div align="center">
<div class="marketing_box" style="margin-top:0px;">
<div class="btn_search_box" style=" display:none;">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear"><a href="add.aspx">添加</a><a href="javascript:;" class="edit">修改</a><a href="javascript:;" class="tdel">删除</a></div>
                <div class="del clear"></div>
            </div>
</div>
<div class="marketing_table">
<table cellpadding="0" cellspacing="0" >
<asp:Repeater ID="repOpp" runat="server">
<HeaderTemplate>
<tr><th>机会名称</th><th>当前热度</th></tr>
</HeaderTemplate>
   <ItemTemplate>
   <tr><td style="padding-left:0px;"><a href="/Client/SalesOpportunities/Show.aspx?id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "OppName")%></a></td><td style="padding-left:0px;">当前热度</td></tr>

   </ItemTemplate>
</asp:Repeater>
</table>
</div>
</div>
</div>

</div>

      <div class="clear btn_box">
      <a href="javascript:history.go(-1);;" class="cancel">返回</a>
      </div>

    </form>

</asp:Content>











