﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Basic;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Client.SalesOpportunities
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("SALESOPPORTUNITIES_LIST_VIEW"))
            {
                NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            //XinYiOffice.BLL.SalesOpportunities bll = new XinYiOffice.BLL.SalesOpportunities();
            //XinYiOffice.Model.SalesOpportunities model = bll.GetModel(Id);

            DataSet ds=ClientServer.GetSalesOpportunitiesList(string.Format("Id={0}",Id),CurrentTenantId);
            if(ds!=null&&ds.Tables[0].Rows.Count>0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                this.lblId.Text =SafeConvert.ToString(dr["Id"]);
                this.lblOppName.Text = SafeConvert.ToString(dr["OppName"]);
                this.lblCustomerService.Text = SafeConvert.ToString(dr["ClientName"]);
                this.lblPersonalAccountId.Text = SafeConvert.ToString(dr["FullName"]);
                this.lblMarketingPlanId.Text = SafeConvert.ToString(dr["ProgramName"]);
                this.lblSource.Text = SafeConvert.ToString(dr["SourceName"]);
                this.lblRequirement.Text = SafeConvert.ToString(dr["Requirement"]);
                this.lblEstAmount.Text = SafeConvert.ToString(dr["EstAmount"]);
                this.lblCurrencyNotes.Text = SafeConvert.ToString(dr["CurrencyNotes"]);
                this.lblFeasibility.Text = SafeConvert.ToString(dr["Feasibility"]);
                this.lblPresentStage.Text = SafeConvert.ToString(dr["PresentStageName"]);
                this.lblNextStep.Text = SafeConvert.ToString(dr["NextStepName"]);
                this.lblSate.Text = SafeConvert.ToString(dr["SateName"]);
                this.lblStageRemarks.Text = SafeConvert.ToString(dr["StageRemarks"]);
                this.lblOccTime.Text = SafeConvert.ToString(dr["OccTime"]);
                this.lblEstDate.Text = SafeConvert.ToString(dr["EstDate"]);
                this.lblRefreshAccountId.Text = SafeConvert.ToString(dr["RefreshAccountId"]);
                this.lblCreateAccountId.Text =SafeConvert.ToString(dr["CreateAccountId"]);
                this.lblCreateTime.Text = SafeConvert.ToString(dr["CreateTime"]);
                this.lblRefreshTime.Text = SafeConvert.ToString(dr["RefreshTime"]);

            }
            
           
           
            
            
           

        }


    }
}
