﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Ins.Institution
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.Institution bll = new XinYiOffice.BLL.Institution();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("INSTITUTION_MANAGE") && ValidatePermission("INSTITUTION_MANAGE_LIST")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
               
                BindData();
            }
        }
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        #region gridView
                        
        public void BindData()
        {
            repInstitution.DataSource = new BLL.Institution().GetList(string.Format(" TenantId={0} ",CurrentTenantId));
            repInstitution.DataBind();
        }
        #endregion





    }
}
