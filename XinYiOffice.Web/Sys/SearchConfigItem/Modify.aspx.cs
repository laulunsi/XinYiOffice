﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Sys.SearchConfigItem
{
    public partial class Modify : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }


        private void InitData()
        {


        }

        private void ShowInfo(int Id)
        {

            InitData();

            XinYiOffice.BLL.SearchConfigItem bll = new XinYiOffice.BLL.SearchConfigItem();
            XinYiOffice.Model.SearchConfigItem model = bll.GetModel(Id);
            this.HiddenField_id.Value = model.Id.ToString();
            this.txtDisplayName.Text = model.DisplayName;

            Model.SearchConfig sc = new BLL.SearchConfig().GetModel(SafeConvert.ToInt(model.SearchConfigId, 0));
            if (sc != null)
            {
                DropDownList_txtFieldName.DataSource = AppDataCacheServer.GetAllColumsByTableName(sc.TableName);
                DropDownList_txtFieldName.DataTextField = "Name";
                DropDownList_txtFieldName.DataValueField = "Name";
                DropDownList_txtFieldName.DataBind();
                DropDownList_txtFieldName.SelectedValue = model.FieldName;
            }
            else
            {
                xytools.web_alert("参数错误！");
                return;
            }


            this.HiddenField_SearchDataSoureId.Value = model.SearchDataSoureId.ToString();
            this.txtSort.Text = model.Sort.ToString();
            DropDownList_ValueType.SelectedValue = model.ValueType.ToString();
            DropDownList_ValueRelChar.SelectedValue = model.ValueRelChar;
            this.txtPrefixChar.Text = model.PrefixChar;
            DropDownList_PrefixRelChar.SelectedValue = model.PrefixRelChar;
            this.txtSuffixChar.Text = model.SuffixChar;
            DropDownList_SuffixRelChar.SelectedValue = model.SuffixRelChar;
            this.HiddenField_SearchConfigId.Value = model.SearchConfigId.ToString();
            DropDownList_UseValueType.SelectedValue = model.UseValueType.ToString();

            this.txtDefaultValue.Text = model.DefaultValue;

            #region //设置搜索数据源
            if (SafeConvert.ToInt(model.SearchDataSoureId)!=0)
            {
                XinYiOffice.Model.SearchDataSoure model_sds = new BLL.SearchDataSoure().GetModel(SafeConvert.ToInt(model.SearchDataSoureId));
                DropDownList_DataSoureType.SelectedValue = model_sds.DataSoureType.ToString();
                txtDataSoureConntion.Text = model_sds.DataSoureConntion;
                DropDownList_DataType.SelectedValue = model_sds.DataType.ToString();
                txtDisplayValue.Text = model_sds.DisplayValue;
                txtTrueValue.Text = model_sds.TrueValue;
                DropDownList_FormType.SelectedValue = model_sds.FormType.ToString();
                txtFormStyle.Text = model_sds.FormStyle;
                txtFormClass.Text = model_sds.FormClass;
            }
            #endregion
        }
 


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.HiddenField_id.Value);
            string DisplayName = this.txtDisplayName.Text;
            string FieldName = this.DropDownList_txtFieldName.SelectedValue;
            int SearchDataSoureId = int.Parse(this.HiddenField_SearchDataSoureId.Value);
            int Sort = int.Parse(this.txtSort.Text);
            int ValueType = int.Parse(this.DropDownList_ValueType.SelectedValue);
            string ValueRelChar = this.DropDownList_ValueRelChar.SelectedValue;
            string PrefixChar = this.txtPrefixChar.Text;
            string PrefixRelChar = this.DropDownList_PrefixRelChar.SelectedValue;
            string SuffixChar = this.txtSuffixChar.Text;
            string SuffixRelChar = this.DropDownList_SuffixRelChar.SelectedValue;
            int SearchConfigId = int.Parse(this.HiddenField_SearchConfigId.Value);
            int UseValueType = int.Parse(this.DropDownList_UseValueType.SelectedValue);
            string DefaultValue = this.txtDefaultValue.Text;

            int RefreshAccountId = CurrentAccountId;
            DateTime RefreshTime = DateTime.Now;


            XinYiOffice.Model.SearchConfigItem model = new XinYiOffice.Model.SearchConfigItem();
            model.Id = Id;
            model.DisplayName = DisplayName;
            model.FieldName = FieldName;
            model.SearchDataSoureId = SearchDataSoureId;
            model.Sort = Sort;
            model.ValueType = ValueType;
            model.ValueRelChar = ValueRelChar;
            model.PrefixChar = PrefixChar;
            model.PrefixRelChar = PrefixRelChar;
            model.SuffixChar = SuffixChar;
            model.SuffixRelChar = SuffixRelChar;
            model.SearchConfigId = SearchConfigId;
            model.UseValueType = UseValueType;
            model.DefaultValue = DefaultValue;

            model.RefreshAccountId = RefreshAccountId;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.SearchConfigItem bll = new XinYiOffice.BLL.SearchConfigItem();

            #region 更新数据源
            if (SafeConvert.ToInt(DropDownList_DataSoureType.SelectedValue) != 0)
            {
                XinYiOffice.Model.SearchDataSoure model_sds = new XinYiOffice.Model.SearchDataSoure();
                model_sds.Id = SafeConvert.ToInt(HiddenField_SearchDataSoureId.Value);
                model_sds.DataSoureType = SafeConvert.ToInt(DropDownList_DataSoureType.SelectedValue);
                model_sds.DataSoureConntion = txtDataSoureConntion.Text;
                model_sds.DataType = SafeConvert.ToInt(DropDownList_DataType.SelectedValue, 1);
                model_sds.DisplayValue = txtDisplayValue.Text;
                model_sds.TrueValue = txtTrueValue.Text;
                model_sds.FormType = SafeConvert.ToInt(DropDownList_FormType.SelectedValue);
                model_sds.FormStyle = txtFormStyle.Text;
                model_sds.FormClass = txtFormClass.Text;

                model_sds.RefreshAccountId = CurrentAccountId;

                model_sds.RefreshTime = DateTime.Now;

                XinYiOffice.BLL.SearchDataSoure bll_sds = new XinYiOffice.BLL.SearchDataSoure();

                if (SearchDataSoureId != 0)
                {
                    bll_sds.Update(model_sds);
                }
                else
                {
                    model.SearchDataSoureId = bll_sds.Add(model_sds);//更新时若没有 数据源id,则新创建一个
                }

            }
            else
            {
                //等于0则不使用任何数据源,应该删除
                if (SearchDataSoureId != 0)
                {
                    new BLL.SearchDataSoure().Delete(SafeConvert.ToInt(SearchDataSoureId));
                }
            }
            #endregion

            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx?SearchConfigId=" + SearchConfigId);

        }
    }
}
