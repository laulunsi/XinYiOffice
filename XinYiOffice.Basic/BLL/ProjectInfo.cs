﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 项目表 项目
	/// </summary>
	public partial class ProjectInfo
	{
		private readonly XinYiOffice.DAL.ProjectInfo dal=new XinYiOffice.DAL.ProjectInfo();
		public ProjectInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.ProjectInfo model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.ProjectInfo model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.ProjectInfo GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.ProjectInfo GetModelByCache(int Id)
		{
			
			string CacheKey = "ProjectInfoModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.ProjectInfo)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.ProjectInfo> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.ProjectInfo> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.ProjectInfo> modelList = new List<XinYiOffice.Model.ProjectInfo>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.ProjectInfo model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.ProjectInfo();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["ParentProjectId"]!=null && dt.Rows[n]["ParentProjectId"].ToString()!="")
					{
						model.ParentProjectId=int.Parse(dt.Rows[n]["ParentProjectId"].ToString());
					}
					if(dt.Rows[n]["ProjectPropertie"]!=null && dt.Rows[n]["ProjectPropertie"].ToString()!="")
					{
					model.ProjectPropertie=dt.Rows[n]["ProjectPropertie"].ToString();
					}
					if(dt.Rows[n]["ProjectTypeId"]!=null && dt.Rows[n]["ProjectTypeId"].ToString()!="")
					{
						model.ProjectTypeId=int.Parse(dt.Rows[n]["ProjectTypeId"].ToString());
					}
					if(dt.Rows[n]["Title"]!=null && dt.Rows[n]["Title"].ToString()!="")
					{
					model.Title=dt.Rows[n]["Title"].ToString();
					}
					if(dt.Rows[n]["Name"]!=null && dt.Rows[n]["Name"].ToString()!="")
					{
					model.Name=dt.Rows[n]["Name"].ToString();
					}
					if(dt.Rows[n]["ProjectManagerAccountId"]!=null && dt.Rows[n]["ProjectManagerAccountId"].ToString()!="")
					{
						model.ProjectManagerAccountId=int.Parse(dt.Rows[n]["ProjectManagerAccountId"].ToString());
					}
					if(dt.Rows[n]["OrganiserAccountId"]!=null && dt.Rows[n]["OrganiserAccountId"].ToString()!="")
					{
						model.OrganiserAccountId=int.Parse(dt.Rows[n]["OrganiserAccountId"].ToString());
					}
					if(dt.Rows[n]["ExecutorAccountId"]!=null && dt.Rows[n]["ExecutorAccountId"].ToString()!="")
					{
						model.ExecutorAccountId=int.Parse(dt.Rows[n]["ExecutorAccountId"].ToString());
					}
					if(dt.Rows[n]["CompletionTime"]!=null && dt.Rows[n]["CompletionTime"].ToString()!="")
					{
						model.CompletionTime=DateTime.Parse(dt.Rows[n]["CompletionTime"].ToString());
					}
					if(dt.Rows[n]["ActualFinishTime"]!=null && dt.Rows[n]["ActualFinishTime"].ToString()!="")
					{
						model.ActualFinishTime=DateTime.Parse(dt.Rows[n]["ActualFinishTime"].ToString());
					}
					if(dt.Rows[n]["EstDisbursement"]!=null && dt.Rows[n]["EstDisbursement"].ToString()!="")
					{
						model.EstDisbursement=decimal.Parse(dt.Rows[n]["EstDisbursement"].ToString());
					}
					if(dt.Rows[n]["ActualPayment"]!=null && dt.Rows[n]["ActualPayment"].ToString()!="")
					{
						model.ActualPayment=decimal.Parse(dt.Rows[n]["ActualPayment"].ToString());
					}
					if(dt.Rows[n]["ActualStartTime"]!=null && dt.Rows[n]["ActualStartTime"].ToString()!="")
					{
						model.ActualStartTime=DateTime.Parse(dt.Rows[n]["ActualStartTime"].ToString());
					}
					if(dt.Rows[n]["EstTimeStart"]!=null && dt.Rows[n]["EstTimeStart"].ToString()!="")
					{
						model.EstTimeStart=DateTime.Parse(dt.Rows[n]["EstTimeStart"].ToString());
					}
					if(dt.Rows[n]["CompleteWorkDay"]!=null && dt.Rows[n]["CompleteWorkDay"].ToString()!="")
					{
						model.CompleteWorkDay=int.Parse(dt.Rows[n]["CompleteWorkDay"].ToString());
					}
					if(dt.Rows[n]["ActualWorkingDay"]!=null && dt.Rows[n]["ActualWorkingDay"].ToString()!="")
					{
						model.ActualWorkingDay=int.Parse(dt.Rows[n]["ActualWorkingDay"].ToString());
					}
					if(dt.Rows[n]["State"]!=null && dt.Rows[n]["State"].ToString()!="")
					{
						model.State=int.Parse(dt.Rows[n]["State"].ToString());
					}
					if(dt.Rows[n]["JobDescription"]!=null && dt.Rows[n]["JobDescription"].ToString()!="")
					{
					model.JobDescription=dt.Rows[n]["JobDescription"].ToString();
					}
					if(dt.Rows[n]["TaskDescription"]!=null && dt.Rows[n]["TaskDescription"].ToString()!="")
					{
					model.TaskDescription=dt.Rows[n]["TaskDescription"].ToString();
					}
					if(dt.Rows[n]["URL"]!=null && dt.Rows[n]["URL"].ToString()!="")
					{
					model.URL=dt.Rows[n]["URL"].ToString();
					}
					if(dt.Rows[n]["EstExpenses"]!=null && dt.Rows[n]["EstExpenses"].ToString()!="")
					{
						model.EstExpenses=decimal.Parse(dt.Rows[n]["EstExpenses"].ToString());
					}
					if(dt.Rows[n]["ActualExpenditure"]!=null && dt.Rows[n]["ActualExpenditure"].ToString()!="")
					{
						model.ActualExpenditure=decimal.Parse(dt.Rows[n]["ActualExpenditure"].ToString());
					}
					if(dt.Rows[n]["AnticipatedRevenue"]!=null && dt.Rows[n]["AnticipatedRevenue"].ToString()!="")
					{
						model.AnticipatedRevenue=decimal.Parse(dt.Rows[n]["AnticipatedRevenue"].ToString());
					}
					if(dt.Rows[n]["Income"]!=null && dt.Rows[n]["Income"].ToString()!="")
					{
						model.Income=decimal.Parse(dt.Rows[n]["Income"].ToString());
					}
					if(dt.Rows[n]["PredecessorTaskProjectId"]!=null && dt.Rows[n]["PredecessorTaskProjectId"].ToString()!="")
					{
						model.PredecessorTaskProjectId=int.Parse(dt.Rows[n]["PredecessorTaskProjectId"].ToString());
					}
					if(dt.Rows[n]["SurveyorAccountId"]!=null && dt.Rows[n]["SurveyorAccountId"].ToString()!="")
					{
						model.SurveyorAccountId=int.Parse(dt.Rows[n]["SurveyorAccountId"].ToString());
					}
					if(dt.Rows[n]["BySalesOpportunitiesId"]!=null && dt.Rows[n]["BySalesOpportunitiesId"].ToString()!="")
					{
						model.BySalesOpportunitiesId=int.Parse(dt.Rows[n]["BySalesOpportunitiesId"].ToString());
					}
					if(dt.Rows[n]["ByClientInfoId"]!=null && dt.Rows[n]["ByClientInfoId"].ToString()!="")
					{
						model.ByClientInfoId=int.Parse(dt.Rows[n]["ByClientInfoId"].ToString());
					}
					if(dt.Rows[n]["Remarks"]!=null && dt.Rows[n]["Remarks"].ToString()!="")
					{
					model.Remarks=dt.Rows[n]["Remarks"].ToString();
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["RefreshAccountId"]!=null && dt.Rows[n]["RefreshAccountId"].ToString()!="")
					{
						model.RefreshAccountId=int.Parse(dt.Rows[n]["RefreshAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["RefreshTime"]!=null && dt.Rows[n]["RefreshTime"].ToString()!="")
					{
						model.RefreshTime=DateTime.Parse(dt.Rows[n]["RefreshTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

