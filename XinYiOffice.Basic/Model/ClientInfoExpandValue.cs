﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 客户扩展信息表
	/// </summary>
	[Serializable]
	public partial class ClientInfoExpandValue
	{
		public ClientInfoExpandValue()
		{}
		#region Model
		private int _id;
		private int? _clientinfoexpandid;
		private string _value;
		private int? _clientinfoid;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 扩展id
		/// </summary>
		public int? ClientInfoExpandId
		{
			set{ _clientinfoexpandid=value;}
			get{return _clientinfoexpandid;}
		}
		/// <summary>
		/// 扩展值
		/// </summary>
		public string Value
		{
			set{ _value=value;}
			get{return _value;}
		}
		/// <summary>
		/// 关联客户id
		/// </summary>
		public int? ClientInfoId
		{
			set{ _clientinfoid=value;}
			get{return _clientinfoid;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

