﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.ProjectInfo
{
    public partial class Modify : BasicPage
    {
        public DataTable dtProjectList;
        public DateTime dtProjectCreate;
        public int lay;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("PROJECTINFO_LIST_EDIT"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    
                    ShowInfo(Id);
                }
            }
        }

        #region 项目下拉列表
        protected void SetDropDownListProject(int pid, ref DropDownList ddl)
        {
            DataRow[] dr = dtProjectList.Select(string.Format("ParentProjectId={0}", pid));
            int Id = 0;
            int ProjectId = 0;


            foreach (DataRow _dr in dr)
            {
                Id = SafeConvert.ToInt(_dr["Id"], 1);
                ProjectId = Id;

                lay++;

                string ProjectTitle = Nex(lay, ProjectId, pid, SafeConvert.ToString(_dr["Title"]));
                ddl.Items.Add(new ListItem(ProjectTitle, ProjectId.ToString()));

                SetDropDownListProject(Id, ref ddl);

                lay--;
            }
        }

        public string Nex(int x, int bid, int pid, string cname)
        {
            StringBuilder s = new StringBuilder();

            string kg = HttpUtility.HtmlDecode("&nbsp;");

            char nbsp = (char)0xA0;


            for (int i = 0; i < x; i++)
            {
                kg += kg;

                nbsp += nbsp;
            }

            if (pid == 0)
            {
                s.Append("" + cname);
            }
            else
            {
                if (!GetCustomInExist(bid))
                {
                    //不存在子级
                    s.Append(kg + "├" + cname);
                }
                else
                {//存在子级
                    s.Append(kg + "└" + cname);
                }
            }

            return s.ToString();
        }

        protected bool GetCustomInExist(int pid)
        {
            DataRow[] dr = dtProjectList.Select(string.Format("Id={0}", pid));

            if (dr.Length > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        protected void InitData()
        {
            DateTime dtNow = dtProjectCreate;//当前项目创建月
            ddl_Year.SelectedValue = SafeConvert.ToString(dtNow.Year);
            ddl_month.SelectedValue = SafeConvert.ToString(dtNow.Month);

            //绑定项目树形
            DataSet ds = new BLL.ProjectInfo().GetList(string.Format("DATEPART(year,CreateTime)='{0}' and DATEPART(month,CreateTime)='{1}' and TenantId={2} ", dtNow.Year, dtNow.Month,CurrentTenantId));
            if (ds != null && ds.Tables.Count > 0)
            {
                dtProjectList = ds.Tables[0];
                SetDropDownListProject(0, ref DropDownList_ParentProjectId);
            }

            ddl_ProjectTypeId.DataSource = new BLL.ProjectTypes().GetList(string.Format(" TenantId={0} ",CurrentTenantId));
            ddl_ProjectTypeId.DataTextField = "TypeName";
            ddl_ProjectTypeId.DataValueField = "Id";
            ddl_ProjectTypeId.DataBind();

            SetDropDownList("State", ref DropDownList_State);
            SetDropDownList("ProjectPropertie", ref DropDownList_ProjectPropertie);
        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.ProjectInfoDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        private void ShowInfo(int Id)
        {
            DataTable dt=ProjectInfoServer.GetProjectList(string.Format("Id={0}",Id),CurrentTenantId);
            DataRow dr=dt.Rows[0];

            if(dr!=null)
            {
                dtProjectCreate = SafeConvert.ToDateTime(dr["CreateTime"]);
                InitData();

                //绑定下拉框
                DropDownList_ParentProjectId.SelectedValue = SafeConvert.ToString(dr["ParentProjectId"]);
                ddl_ProjectTypeId.SelectedValue = SafeConvert.ToString(dr["ProjectTypeId"]);
                DropDownList_ProjectPropertie.SelectedValue = SafeConvert.ToString(dr["ProjectPropertie"]); ;
                DropDownList_State.SelectedValue = SafeConvert.ToString(dr["State"]);
                ddl_PredecessorTaskProjectId.SelectedValue = SafeConvert.ToString(dr["PredecessorTaskProjectId"]);
                hid_CreateAccountId.Value = SafeConvert.ToString(dr["CreateAccountId"]);
                HiddenField_CreateTime.Value = SafeConvert.ToString(dr["CreateTime"]);

                //隐藏域的值绑定
                HiddenField_CurrentProjectId.Value = SafeConvert.ToString(dr["Id"]);
                hid_ParentProjectId.Value = SafeConvert.ToString(dr["ParentProjectId"]);
                hidProjectManagerAccountId.Value = SafeConvert.ToString(dr["ProjectManagerAccountId"]);
                HiddenField_OrganiserAccountId.Value = SafeConvert.ToString(dr["OrganiserAccountId"]);
                HiddenField_ExecutorAccountId.Value = SafeConvert.ToString(dr["ExecutorAccountId"]);
                hid_ByClientInfoId.Value = SafeConvert.ToString(dr["ByClientInfoId"]);
                hid_SurveyorAccountId.Value = SafeConvert.ToString(dr["SurveyorAccountId"]);
                hid_BySalesOpportunitiesId.Value = SafeConvert.ToString(dr["BySalesOpportunitiesId"]);

                //绑定文本框
                txtTitle.Text = SafeConvert.ToString(dr["Title"]);
                txtProjectManagerAccountId.Text = SafeConvert.ToString(dr["ProjectManagerAccountName"]);
                TextBox_OrganiserAccountId.Text = SafeConvert.ToString(dr["OrganiserAccountName"]);
                txtName.Text = SafeConvert.ToString(dr["Name"]);
                TextBox_ExecutorAccountId.Text = SafeConvert.ToString(dr["ExecutorName"]);
                txtJobDescription.Text = SafeConvert.ToString(dr["JobDescription"]);
                TextBox_URL.Text = SafeConvert.ToString(dr["URL"]);
                txtByClientInfoId.Text = SafeConvert.ToString(dr["ByClientInfoName"]);
                txtSurveyorAccountId.Text = SafeConvert.ToString(dr["SurveyorAccountName"]);
                txtBySalesOpportunitiesId.Text = SafeConvert.ToString(dr["BySalesOpportunitiesName"]);
                txtRemarks.Text = SafeConvert.ToString(dr["Remarks"]);

                //时间
                txtCompletionTime.Text = SafeConvert.ToDateTime(dr["CompletionTime"]).ToShortDateString();
                txtActualFinishTime.Text = SafeConvert.ToDateTime(dr["ActualFinishTime"]).ToShortDateString();
                txtEstTimeStart.Text = SafeConvert.ToDateTime(dr["EstTimeStart"]).ToShortDateString();
                txtActualStartTime.Text = SafeConvert.ToDateTime(dr["ActualStartTime"]).ToShortDateString();
                txtCompleteWorkDay.Text = SafeConvert.ToInt(dr["CompleteWorkDay"], 0).ToString();
                txtActualWorkingDay.Text = SafeConvert.ToInt(dr["ActualWorkingDay"], 0).ToString();
                //费用
                txtEstDisbursement.Text = SafeConvert.ToString(dr["EstDisbursement"]);
                txtActualPayment.Text = SafeConvert.ToString(dr["ActualPayment"]);
                txtEstExpenses.Text = SafeConvert.ToString(dr["EstExpenses"]);
                txtActualExpenditure.Text = SafeConvert.ToString(dr["ActualExpenditure"]);
                txtAnticipatedRevenue.Text = SafeConvert.ToString(dr["AnticipatedRevenue"]);
                txtIncome.Text = SafeConvert.ToString(dr["Income"]);
            }

        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = SafeConvert.ToInt(HiddenField_CurrentProjectId.Value, 0);
            int ParentProjectId = SafeConvert.ToInt(hid_ParentProjectId.Value, 0);
            int ProjectTypeId = SafeConvert.ToInt(ddl_ProjectTypeId.SelectedValue, 0);
            string Title = this.txtTitle.Text;
            string Name = this.txtName.Text;
            int ProjectManagerAccountId = SafeConvert.ToInt(hidProjectManagerAccountId.Value);
            DateTime CompletionTime = DateTime.Parse(this.txtCompletionTime.Text);
            DateTime ActualFinishTime = DateTime.Parse(this.txtActualFinishTime.Text);
            decimal EstDisbursement = decimal.Parse(this.txtEstDisbursement.Text);
            decimal ActualPayment = decimal.Parse(this.txtActualPayment.Text);
            DateTime ActualStartTime = DateTime.Parse(this.txtActualStartTime.Text);
            DateTime EstTimeStart = DateTime.Parse(this.txtEstTimeStart.Text);
            int CompleteWorkDay = int.Parse(this.txtCompleteWorkDay.Text);
            int ActualWorkingDay = int.Parse(this.txtActualWorkingDay.Text);

            int State = SafeConvert.ToInt(DropDownList_State.SelectedValue);
            string JobDescription = this.txtJobDescription.Text;
            string TaskDescription = this.txtJobDescription.Text;
            decimal EstExpenses = SafeConvert.ToDecimal(this.txtEstExpenses.Text);
            decimal ActualExpenditure = SafeConvert.ToDecimal(this.txtActualExpenditure.Text);
            decimal AnticipatedRevenue = SafeConvert.ToDecimal(this.txtAnticipatedRevenue.Text);
            decimal Income = decimal.Parse(this.txtIncome.Text);

            int PredecessorTaskProjectId = SafeConvert.ToInt(ddl_PredecessorTaskProjectId.SelectedValue);
            int SurveyorAccountId = SafeConvert.ToInt(hid_SurveyorAccountId.Value);
            int BySalesOpportunitiesId = SafeConvert.ToInt(hid_BySalesOpportunitiesId.Value);
            int ByClientInfoId = SafeConvert.ToInt(hid_ByClientInfoId.Value);
            string Remarks = this.txtRemarks.Text;

            int CreateAccountId = SafeConvert.ToInt(hid_CreateAccountId.Value);
            int RefreshAccountId = SafeConvert.ToInt(CurrentAccountId);
            DateTime CreateTime = SafeConvert.ToDateTime(HiddenField_CreateTime.Value);
            DateTime RefreshTime = DateTime.Now;


            XinYiOffice.Model.ProjectInfo model = new XinYiOffice.Model.ProjectInfo();
            model.Id = Id;
            model.ParentProjectId = ParentProjectId;
            model.ProjectPropertie = SafeConvert.ToString(DropDownList_ProjectPropertie.SelectedValue);
            model.ProjectTypeId = ProjectTypeId;
            model.Title = Title;
            model.Name = Name;
            model.ProjectManagerAccountId = ProjectManagerAccountId;
            model.CompletionTime = CompletionTime;
            model.ActualFinishTime = ActualFinishTime;
            model.EstDisbursement = EstDisbursement;
            model.ActualPayment = ActualPayment;
            model.ActualStartTime = ActualStartTime;
            model.EstTimeStart = EstTimeStart;
            model.CompleteWorkDay = CompleteWorkDay;
            model.ActualWorkingDay = ActualWorkingDay;
            model.State = State;
            model.JobDescription = JobDescription;
            model.TaskDescription = TaskDescription;
            model.EstExpenses = EstExpenses;
            model.ActualExpenditure = ActualExpenditure;
            model.AnticipatedRevenue = AnticipatedRevenue;
            model.Income = Income;
            model.PredecessorTaskProjectId = PredecessorTaskProjectId;
            model.SurveyorAccountId = SurveyorAccountId;
            model.BySalesOpportunitiesId = BySalesOpportunitiesId;
            model.ByClientInfoId = ByClientInfoId;

            model.OrganiserAccountId = SafeConvert.ToInt(HiddenField_OrganiserAccountId.Value);
            model.ExecutorAccountId=SafeConvert.ToInt(HiddenField_ExecutorAccountId.Value);

            model.Remarks = Remarks;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.ProjectInfo bll = new XinYiOffice.BLL.ProjectInfo();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");

        }
    }
}
