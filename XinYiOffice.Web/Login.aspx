﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="XinYiOffice.Web.Login" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>XinYiOffice - 新亿办公系统</title>
<link href="/css/init.css" type="text/css" rel="stylesheet" />
<link href="/css/login.css" type="text/css" rel="stylesheet" />
<script language="javascript" src="/js/jquery-1.8.2.js"></script>
<script language="javascript" src="/js/login.js"></script>
</head>

<body>
<input type="hidden" id="TenantId" name="TenantId" value="<%=TenantId%>" />
<div class="hd_msg"><%=msg%></div>
<h1></h1>
<div id="min">
    <ul id="login">
        <li class="clear user"><input name="txt_username" type="text" id="txt_username" value="" /></li>
        <li class="clear password"><input name="txt_pas" type="password" id="txt_pas" value="" /></li>
        <li class="clear yzm"><input type="text" value="验证码:" onclick="if(this.value=='验证码:'){this.value='';}" onblur="if(this.value==''){this.value='验证码:'}"/><img src="/Call/VerifyCode.aspx?rnd" id="vimg"/><a href="javascript:;" id="kanbuq">看不清，换一张</a></li>
        <li class="check clear"><label><input type="checkbox" value="" id="online" name="online"/><span>使我保持登录状态</span></label></li>
        <li class="sub clear"><input type="submit" value="登陆" id="btn" /><a href="javascript:;">取消</a><span id="span_msg"></span></li>
    </ul>
</div>
<div class="foot"><a href="http://www.xinyioffice.com" target="_blank" style="color:#CCC;">新亿乐天网络技术（北京）有限责任公司</a>
<script type="text/javascript" src="http://js.tongji.linezing.com/3248701/tongji.js"></script><noscript><a href="http://www.linezing.com"><img src="http://img.tongji.linezing.com/3248701/tongji.gif"/></a></noscript> 
</div>
<!--[if IE 6]>
<script src="/js/DD_belatedPNG_0.0.8a.js"></script>
<script>
	DD_belatedPNG.fix("*,img");
</script>
<![endif]-->

</body>
</html>
<html>
<head>
<title>判断IE版本并给出提示升级浏览器</title>
</head>
<style type="text/css">
       #ie6-warning{
       background:rgb(255,255,225) url("/jscss/demoimg/201006/warning.gif") no-repeat scroll 3px center;
       position:absolute;
       top:0;
       left:0;
       font-size:12px;
       color:#333;
       width:97%;
       padding: 2px 15px 2px 23px;
       text-align:left;
}
#ie6-warning a {
       text-decoration:none;
}
</style>
<body>

<!--[if lte IE 6]>
<div id="ie6-warning">您正在使用 Internet Explorer 6，在本页面的显示效果可能有差异。建议您升级到 <a href="http://www.microsoft.com/china/windows/internet-explorer/" target="_blank">Internet Explorer 8</a> 或以下浏览器： <a href="http://www.mozillaonline.com/">Firefox</a> / <a href="http://www.google.com/chrome/?hl=zh-CN">Chrome</a> / <a href="http://www.apple.com.cn/safari/">Safari</a> / <a href="http://www.operachina.com/">Opera</a>
</div>
<script type="text/javascript">
function position_fixed(el, eltop, elleft){  
       // check if this is IE6  
       if(!window.XMLHttpRequest)  
              window.onscroll = function(){  
                     el.style.top = (document.documentElement.scrollTop + eltop)+"px";  
                     el.style.left = (document.documentElement.scrollLeft + elleft)+"px";  
       }  
       else el.style.position = "fixed";  
}
       position_fixed(document.getElementById("ie6-warning"),0, 0);
</script>
<![endif]-->
