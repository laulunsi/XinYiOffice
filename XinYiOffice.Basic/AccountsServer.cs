﻿using System;
using System.Collections.Generic;
using System.Text;
using XinYiOffice.Common;
using System.Data;
using XinYiOffice.Config;
using System.Data.SqlClient;
using XinYiOffice.Common.Web;

namespace XinYiOffice.Basic
{
    public class AccountsServer
    {
        /// <summary>
        /// 会员状态
        /// </summary>
        public static int State
        {
            get { return _state; }
            set { _state = value; }
        }
        private static int _state;


        static AccountsServer()
        {
            
        }

        /// <summary>
        /// 获取头像
        /// </summary>
        /// <param name="HeadPortrait"></param>
        /// <returns></returns>
        public static string GetAccountsHeadPortrait(string HeadPortrait)
        {
            string _tem="/img/face/{0}.png";
            string _w = string.Format(_tem,12); ;
            int head = SafeConvert.ToInt(HeadPortrait, 0);
            if(head!=0)
            {
                _w=string.Format(_tem,head);
            }
            return _w;
        }

        //判断用户名密码是否正确
        public static bool LoginValidation(string UsersName, string UsersPassword, string TenantId, int cookie_day)
        {
            _state = -1;//用户名密码错误
            bool t = false;
            if (xytools.DataIsNoNull(UsersName, UsersPassword, TenantId))
            {
                //return false;
            }
            string mysql = string.Format("SELECT top 1 Id,AccountName,AccountPassword,NiceName,Sate,TenantId FROM [Accounts] where AccountName='{0}' and AccountPassword='{1}' and TenantId={2}", UsersName, UsersPassword, Common.DESEncrypt.Decrypt(TenantId));
            SqlDataReader dr = DbHelperSQL.ExecuteReader(mysql);

            while (dr.Read())
            {
                if (dr.HasRows)
                {
                    t = true;
                    int ui_ID = SafeConvert.ToInt(dr.GetValue(0));
                    string ui_Name = SafeConvert.ToString(dr.GetValue(1));
                    string ui_Password = SafeConvert.ToString(dr.GetValue(2));
                    string ui_NickName = SafeConvert.ToString(dr.GetValue(3));
                    int ui_State = SafeConvert.ToInt(dr.GetValue(4));
                    int _tenantId = SafeConvert.ToInt(dr.GetValue(5));

                    bool Isd = xytools.DataIsNoNull(ui_ID, ui_Name);
                    if (Isd)
                    {
                        string uid = ui_ID.ToString();

                        CookieHelper.Set("xyo_cookie_tenantid", TenantId, cookie_day);
                        CookieHelper.Set("xyo_cookie_aid", DESEncrypt.Encrypt(uid), cookie_day);
                        CookieHelper.Set("xyo_cookie_aname", ui_Name, cookie_day);
                        CookieHelper.Set("xyo_cookie_nickname", ui_NickName, cookie_day);


                        SessionHelper.Add("TenantId", _tenantId.ToString());//未加密数据
                        _state = ui_State;
                    }
                }
            }
            dr.Close();
            dr.Dispose();

            return t;
        }

        //读取Cookie里的用户id
        public static int GetCliCookie()
        {
            //string uc_uid=XinYiOffice.Common.CookieHelper.GetCookie("xyc_cookie_uc_uid");
            int aid = 0;
            try
            {
                string _aid = string.Empty;
                _aid = DESEncrypt.Decrypt(CookieHelper.GetCookie("xyo_cookie_aid"));
                if (!string.IsNullOrEmpty(_aid))
                {
                    aid = Convert.ToInt16(_aid.ToString());
                }
            }
            catch
            {
                //uid=0;
            }

            return aid;
        }

        public static string GetFullNameByAccountId(int aid)
        {
            string _w = string.Empty;

            string mysql = string.Format("select FullName from Accounts where Id={0}",aid);

            try
            {
                _w = DbHelperSQL.Query(mysql).Tables[0].Rows[0][0].ToString();
            }
            catch
            {
                _w = "异常姓名";
            }
            return _w;
        }

        /// <summary>
        /// 获取所有帐号信息
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAccountAllList(int TenantId)
        {
            DataTable _dt = null;
            DataSet ds = new BLL.Accounts().GetList(string.Format("TenantId={0}", TenantId));
            if (ds != null && ds.Tables.Count > 0)
            {
                _dt = ds.Tables[0];
            }

            return _dt;
        }

    }
}
