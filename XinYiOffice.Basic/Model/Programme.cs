﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 日程表
	/// </summary>
	[Serializable]
	public partial class Programme
	{
		public Programme()
		{}
		#region Model
		private int _id;
		private DateTime? _starttime;
		private DateTime? _endtime;
		private int? _scheduletype;
		private int? _projectinfoid;
		private int? _institutionid;
		private string _title;
		private string _locale;
		private string _note;
		private int? _isrepeat;
		private int? _repeatinterval;
		private DateTime? _uptodate;
		private int? _isremind;
		private int? _remindday;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 开始时间
		/// </summary>
		public DateTime? StartTime
		{
			set{ _starttime=value;}
			get{return _starttime;}
		}
		/// <summary>
		/// 结束时间
		/// </summary>
		public DateTime? EndTime
		{
			set{ _endtime=value;}
			get{return _endtime;}
		}
		/// <summary>
		/// 日程类型
		/// </summary>
		public int? ScheduleType
		{
			set{ _scheduletype=value;}
			get{return _scheduletype;}
		}
		/// <summary>
		/// 项目ID
		/// </summary>
		public int? ProjectInfoId
		{
			set{ _projectinfoid=value;}
			get{return _projectinfoid;}
		}
		/// <summary>
		/// 组织ID
		/// </summary>
		public int? InstitutionId
		{
			set{ _institutionid=value;}
			get{return _institutionid;}
		}
		/// <summary>
		/// 日程标题
		/// </summary>
		public string Title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 地点
		/// </summary>
		public string Locale
		{
			set{ _locale=value;}
			get{return _locale;}
		}
		/// <summary>
		/// 备注说明
		/// </summary>
		public string Note
		{
			set{ _note=value;}
			get{return _note;}
		}
		/// <summary>
		/// 是否重复 0
		/// </summary>
		public int? IsRepeat
		{
			set{ _isrepeat=value;}
			get{return _isrepeat;}
		}
		/// <summary>
		/// 重复间隔类型 1-每天,2-每周
		/// </summary>
		public int? RepeatInterval
		{
			set{ _repeatinterval=value;}
			get{return _repeatinterval;}
		}
		/// <summary>
		/// 重复截至日期
		/// </summary>
		public DateTime? UpToDate
		{
			set{ _uptodate=value;}
			get{return _uptodate;}
		}
		/// <summary>
		/// 是否提前提醒 1-提前
		/// </summary>
		public int? IsRemind
		{
			set{ _isremind=value;}
			get{return _isremind;}
		}
		/// <summary>
		/// 提前几天 提前几天
		/// </summary>
		public int? RemindDay
		{
			set{ _remindday=value;}
			get{return _remindday;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

