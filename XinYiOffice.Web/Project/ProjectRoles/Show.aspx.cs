﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.ProjectRoles
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("PROJECTROLES_LIST_VIEW"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.ProjectRoles bll = new XinYiOffice.BLL.ProjectRoles();
            XinYiOffice.Model.ProjectRoles model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.lblName.Text = model.Name;
            this.lblNameEn.Text = model.NameEn;
            this.lblDESCRIBE.Text = model.DESCRIBE;
            this.lblRoleId.Text = model.RoleId.ToString();
            this.lblCreateAccountId.Text = model.CreateAccountId.ToString();
            this.lblRefreshAccountId.Text = model.RefreshAccountId.ToString();
            this.lblCreateTime.Text = model.CreateTime.ToString();
            this.lblRefreshTime.Text = model.RefreshTime.ToString();

        }


    }
}
