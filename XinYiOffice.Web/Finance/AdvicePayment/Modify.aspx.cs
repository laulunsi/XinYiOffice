﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Finance.AdvicePayment
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("ADVICEPAYMENT_MANAGE") &&
(base.ValidatePermission("ADVICEPAYMENT_MANAGE_LIST_SATE1_EDIT") || base.ValidatePermission("ADVICEPAYMENT_MANAGE_LIST_SATE0_EDIT"))
))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.AdvicePayment bll = new XinYiOffice.BLL.AdvicePayment();
            XinYiOffice.Model.AdvicePayment model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtDocumentNumber.Text = model.DocumentNumber;
            this.txtDocumentDescription.Text = model.DocumentDescription;
            //this.txtMethodPayment.Text = model.MethodPayment.ToString();
            this.txtSupplierName.Text = model.SupplierName;
            this.txtClientName.Text = model.ClientName;
            this.txtPayeeFullName.Text = model.PayeeFullName;
            //this.txtPayeeAccountId.Text = model.PayeeAccountId.ToString();
            this.txtPaymentDate.Text = model.PaymentDate.ToString();
            this.txtTargetDate.Text = model.TargetDate.ToString();
            this.txtRemark.Text = model.Remark;
            this.txtPaymentAmount.Text = model.PaymentAmount.ToString();
            //this.txtPaymentBankAccountId.Text = model.PaymentBankAccountId.ToString();
            //this.txtSate.Text = model.Sate.ToString();
            this.txtCreateAccountId.Value = model.CreateAccountId.ToString();
            this.txtCreateTime.Value = model.CreateTime.ToString();


            InitData();

            DropDownList_PayeeAccountId.SelectedValue = model.PayeeAccountId.ToString();
            DropDownList_PaymentBankAccountId.SelectedValue = model.PaymentBankAccountId.ToString();
            DropDownList_MethodPayment.SelectedValue = model.MethodPayment.ToString();
            DropDownList_Sate.SelectedValue = model.Sate.ToString();
        }



        protected void InitData()
        {
            txtDocumentNumber.Text = string.Format("AP-{0}", DateTime.Now.ToString("yyyyMMddhhssmm"));

            DataTable dtAccountList = AccountsServer.GetAccountAllList(CurrentTenantId);

            DropDownList_PayeeAccountId.DataSource = dtAccountList;
            DropDownList_PayeeAccountId.DataTextField = "FullName";
            DropDownList_PayeeAccountId.DataValueField = "Id";
            DropDownList_PayeeAccountId.DataBind();

            //银行帐号
            DropDownList_PaymentBankAccountId.DataSource = new BLL.BankAccount().GetList(string.Format("1=1 and TenantId={0} ",CurrentTenantId)); ;
            DropDownList_PaymentBankAccountId.DataTextField = "BankAccountName";
            DropDownList_PaymentBankAccountId.DataValueField = "Id";
            DropDownList_PaymentBankAccountId.Items.Insert(0, new ListItem("暂无可选", "0"));
            DropDownList_PaymentBankAccountId.DataBind();

            SetDropDownList_AdvicePayment("MethodPayment", ref DropDownList_MethodPayment);
            SetDropDownList_AdvicePayment("Sate", ref DropDownList_Sate);
        }

        protected void SetDropDownList_AdvicePayment(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.AdvicePaymentDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string DocumentNumber = this.txtDocumentNumber.Text;
            string DocumentDescription = this.txtDocumentDescription.Text;
            int MethodPayment = SafeConvert.ToInt(DropDownList_MethodPayment.SelectedValue);
            string SupplierName = this.txtSupplierName.Text;
            string ClientName = this.txtClientName.Text;
            string PayeeFullName = this.txtPayeeFullName.Text;
            int PayeeAccountId = SafeConvert.ToInt(DropDownList_PayeeAccountId.SelectedValue);
            DateTime PaymentDate = DateTime.Parse(this.txtPaymentDate.Text);
            DateTime TargetDate = DateTime.Parse(this.txtTargetDate.Text);
            string Remark = this.txtRemark.Text;
            decimal PaymentAmount = decimal.Parse(this.txtPaymentAmount.Text);
            int PaymentBankAccountId = SafeConvert.ToInt(DropDownList_PaymentBankAccountId.SelectedValue);
            int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue);
            int CreateAccountId = int.Parse(this.txtCreateAccountId.Value);
            DateTime CreateTime = DateTime.Parse(this.txtCreateTime.Value);


            XinYiOffice.Model.AdvicePayment model = new XinYiOffice.Model.AdvicePayment();
            model.Id = Id;
            model.DocumentNumber = DocumentNumber;
            model.DocumentDescription = DocumentDescription;
            model.MethodPayment = MethodPayment;
            model.SupplierName = SupplierName;
            model.ClientName = ClientName;
            model.PayeeFullName = PayeeFullName;
            model.PayeeAccountId = PayeeAccountId;
            model.PaymentDate = PaymentDate;
            model.TargetDate = TargetDate;
            model.Remark = Remark;
            model.PaymentAmount = PaymentAmount;
            model.PaymentBankAccountId = PaymentBankAccountId;
            model.Sate = Sate;
            model.CreateAccountId = CreateAccountId;
            model.CreateTime = CreateTime;
            model.TenantId = CurrentTenantId;
            XinYiOffice.BLL.AdvicePayment bll = new XinYiOffice.BLL.AdvicePayment();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
