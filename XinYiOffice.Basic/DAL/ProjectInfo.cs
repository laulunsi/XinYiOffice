﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:ProjectInfo
	/// </summary>
	public partial class ProjectInfo
	{
		public ProjectInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "ProjectInfo"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ProjectInfo");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.ProjectInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ProjectInfo(");
			strSql.Append("ParentProjectId,ProjectPropertie,ProjectTypeId,Title,Name,ProjectManagerAccountId,OrganiserAccountId,ExecutorAccountId,CompletionTime,ActualFinishTime,EstDisbursement,ActualPayment,ActualStartTime,EstTimeStart,CompleteWorkDay,ActualWorkingDay,State,JobDescription,TaskDescription,URL,EstExpenses,ActualExpenditure,AnticipatedRevenue,Income,PredecessorTaskProjectId,SurveyorAccountId,BySalesOpportunitiesId,ByClientInfoId,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@ParentProjectId,@ProjectPropertie,@ProjectTypeId,@Title,@Name,@ProjectManagerAccountId,@OrganiserAccountId,@ExecutorAccountId,@CompletionTime,@ActualFinishTime,@EstDisbursement,@ActualPayment,@ActualStartTime,@EstTimeStart,@CompleteWorkDay,@ActualWorkingDay,@State,@JobDescription,@TaskDescription,@URL,@EstExpenses,@ActualExpenditure,@AnticipatedRevenue,@Income,@PredecessorTaskProjectId,@SurveyorAccountId,@BySalesOpportunitiesId,@ByClientInfoId,@Remarks,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ParentProjectId", SqlDbType.Int,4),
					new SqlParameter("@ProjectPropertie", SqlDbType.NChar,10),
					new SqlParameter("@ProjectTypeId", SqlDbType.Int,4),
					new SqlParameter("@Title", SqlDbType.VarChar,4000),
					new SqlParameter("@Name", SqlDbType.VarChar,4000),
					new SqlParameter("@ProjectManagerAccountId", SqlDbType.Int,4),
					new SqlParameter("@OrganiserAccountId", SqlDbType.Int,4),
					new SqlParameter("@ExecutorAccountId", SqlDbType.Int,4),
					new SqlParameter("@CompletionTime", SqlDbType.DateTime),
					new SqlParameter("@ActualFinishTime", SqlDbType.DateTime),
					new SqlParameter("@EstDisbursement", SqlDbType.Float,8),
					new SqlParameter("@ActualPayment", SqlDbType.Float,8),
					new SqlParameter("@ActualStartTime", SqlDbType.DateTime),
					new SqlParameter("@EstTimeStart", SqlDbType.DateTime),
					new SqlParameter("@CompleteWorkDay", SqlDbType.Int,4),
					new SqlParameter("@ActualWorkingDay", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@JobDescription", SqlDbType.VarChar,4000),
					new SqlParameter("@TaskDescription", SqlDbType.VarChar,4000),
					new SqlParameter("@URL", SqlDbType.VarChar,4000),
					new SqlParameter("@EstExpenses", SqlDbType.Float,8),
					new SqlParameter("@ActualExpenditure", SqlDbType.Float,8),
					new SqlParameter("@AnticipatedRevenue", SqlDbType.Float,8),
					new SqlParameter("@Income", SqlDbType.Float,8),
					new SqlParameter("@PredecessorTaskProjectId", SqlDbType.Int,4),
					new SqlParameter("@SurveyorAccountId", SqlDbType.Int,4),
					new SqlParameter("@BySalesOpportunitiesId", SqlDbType.Int,4),
					new SqlParameter("@ByClientInfoId", SqlDbType.Int,4),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.ParentProjectId;
			parameters[1].Value = model.ProjectPropertie;
			parameters[2].Value = model.ProjectTypeId;
			parameters[3].Value = model.Title;
			parameters[4].Value = model.Name;
			parameters[5].Value = model.ProjectManagerAccountId;
			parameters[6].Value = model.OrganiserAccountId;
			parameters[7].Value = model.ExecutorAccountId;
			parameters[8].Value = model.CompletionTime;
			parameters[9].Value = model.ActualFinishTime;
			parameters[10].Value = model.EstDisbursement;
			parameters[11].Value = model.ActualPayment;
			parameters[12].Value = model.ActualStartTime;
			parameters[13].Value = model.EstTimeStart;
			parameters[14].Value = model.CompleteWorkDay;
			parameters[15].Value = model.ActualWorkingDay;
			parameters[16].Value = model.State;
			parameters[17].Value = model.JobDescription;
			parameters[18].Value = model.TaskDescription;
			parameters[19].Value = model.URL;
			parameters[20].Value = model.EstExpenses;
			parameters[21].Value = model.ActualExpenditure;
			parameters[22].Value = model.AnticipatedRevenue;
			parameters[23].Value = model.Income;
			parameters[24].Value = model.PredecessorTaskProjectId;
			parameters[25].Value = model.SurveyorAccountId;
			parameters[26].Value = model.BySalesOpportunitiesId;
			parameters[27].Value = model.ByClientInfoId;
			parameters[28].Value = model.Remarks;
			parameters[29].Value = model.CreateAccountId;
			parameters[30].Value = model.RefreshAccountId;
			parameters[31].Value = model.CreateTime;
			parameters[32].Value = model.RefreshTime;
			parameters[33].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.ProjectInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ProjectInfo set ");
			strSql.Append("ParentProjectId=@ParentProjectId,");
			strSql.Append("ProjectPropertie=@ProjectPropertie,");
			strSql.Append("ProjectTypeId=@ProjectTypeId,");
			strSql.Append("Title=@Title,");
			strSql.Append("Name=@Name,");
			strSql.Append("ProjectManagerAccountId=@ProjectManagerAccountId,");
			strSql.Append("OrganiserAccountId=@OrganiserAccountId,");
			strSql.Append("ExecutorAccountId=@ExecutorAccountId,");
			strSql.Append("CompletionTime=@CompletionTime,");
			strSql.Append("ActualFinishTime=@ActualFinishTime,");
			strSql.Append("EstDisbursement=@EstDisbursement,");
			strSql.Append("ActualPayment=@ActualPayment,");
			strSql.Append("ActualStartTime=@ActualStartTime,");
			strSql.Append("EstTimeStart=@EstTimeStart,");
			strSql.Append("CompleteWorkDay=@CompleteWorkDay,");
			strSql.Append("ActualWorkingDay=@ActualWorkingDay,");
			strSql.Append("State=@State,");
			strSql.Append("JobDescription=@JobDescription,");
			strSql.Append("TaskDescription=@TaskDescription,");
			strSql.Append("URL=@URL,");
			strSql.Append("EstExpenses=@EstExpenses,");
			strSql.Append("ActualExpenditure=@ActualExpenditure,");
			strSql.Append("AnticipatedRevenue=@AnticipatedRevenue,");
			strSql.Append("Income=@Income,");
			strSql.Append("PredecessorTaskProjectId=@PredecessorTaskProjectId,");
			strSql.Append("SurveyorAccountId=@SurveyorAccountId,");
			strSql.Append("BySalesOpportunitiesId=@BySalesOpportunitiesId,");
			strSql.Append("ByClientInfoId=@ByClientInfoId,");
			strSql.Append("Remarks=@Remarks,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@ParentProjectId", SqlDbType.Int,4),
					new SqlParameter("@ProjectPropertie", SqlDbType.NChar,10),
					new SqlParameter("@ProjectTypeId", SqlDbType.Int,4),
					new SqlParameter("@Title", SqlDbType.VarChar,4000),
					new SqlParameter("@Name", SqlDbType.VarChar,4000),
					new SqlParameter("@ProjectManagerAccountId", SqlDbType.Int,4),
					new SqlParameter("@OrganiserAccountId", SqlDbType.Int,4),
					new SqlParameter("@ExecutorAccountId", SqlDbType.Int,4),
					new SqlParameter("@CompletionTime", SqlDbType.DateTime),
					new SqlParameter("@ActualFinishTime", SqlDbType.DateTime),
					new SqlParameter("@EstDisbursement", SqlDbType.Float,8),
					new SqlParameter("@ActualPayment", SqlDbType.Float,8),
					new SqlParameter("@ActualStartTime", SqlDbType.DateTime),
					new SqlParameter("@EstTimeStart", SqlDbType.DateTime),
					new SqlParameter("@CompleteWorkDay", SqlDbType.Int,4),
					new SqlParameter("@ActualWorkingDay", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@JobDescription", SqlDbType.VarChar,4000),
					new SqlParameter("@TaskDescription", SqlDbType.VarChar,4000),
					new SqlParameter("@URL", SqlDbType.VarChar,4000),
					new SqlParameter("@EstExpenses", SqlDbType.Float,8),
					new SqlParameter("@ActualExpenditure", SqlDbType.Float,8),
					new SqlParameter("@AnticipatedRevenue", SqlDbType.Float,8),
					new SqlParameter("@Income", SqlDbType.Float,8),
					new SqlParameter("@PredecessorTaskProjectId", SqlDbType.Int,4),
					new SqlParameter("@SurveyorAccountId", SqlDbType.Int,4),
					new SqlParameter("@BySalesOpportunitiesId", SqlDbType.Int,4),
					new SqlParameter("@ByClientInfoId", SqlDbType.Int,4),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.ParentProjectId;
			parameters[1].Value = model.ProjectPropertie;
			parameters[2].Value = model.ProjectTypeId;
			parameters[3].Value = model.Title;
			parameters[4].Value = model.Name;
			parameters[5].Value = model.ProjectManagerAccountId;
			parameters[6].Value = model.OrganiserAccountId;
			parameters[7].Value = model.ExecutorAccountId;
			parameters[8].Value = model.CompletionTime;
			parameters[9].Value = model.ActualFinishTime;
			parameters[10].Value = model.EstDisbursement;
			parameters[11].Value = model.ActualPayment;
			parameters[12].Value = model.ActualStartTime;
			parameters[13].Value = model.EstTimeStart;
			parameters[14].Value = model.CompleteWorkDay;
			parameters[15].Value = model.ActualWorkingDay;
			parameters[16].Value = model.State;
			parameters[17].Value = model.JobDescription;
			parameters[18].Value = model.TaskDescription;
			parameters[19].Value = model.URL;
			parameters[20].Value = model.EstExpenses;
			parameters[21].Value = model.ActualExpenditure;
			parameters[22].Value = model.AnticipatedRevenue;
			parameters[23].Value = model.Income;
			parameters[24].Value = model.PredecessorTaskProjectId;
			parameters[25].Value = model.SurveyorAccountId;
			parameters[26].Value = model.BySalesOpportunitiesId;
			parameters[27].Value = model.ByClientInfoId;
			parameters[28].Value = model.Remarks;
			parameters[29].Value = model.CreateAccountId;
			parameters[30].Value = model.RefreshAccountId;
			parameters[31].Value = model.CreateTime;
			parameters[32].Value = model.RefreshTime;
			parameters[33].Value = model.TenantId;
			parameters[34].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ProjectInfo ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ProjectInfo ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.ProjectInfo GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,ParentProjectId,ProjectPropertie,ProjectTypeId,Title,Name,ProjectManagerAccountId,OrganiserAccountId,ExecutorAccountId,CompletionTime,ActualFinishTime,EstDisbursement,ActualPayment,ActualStartTime,EstTimeStart,CompleteWorkDay,ActualWorkingDay,State,JobDescription,TaskDescription,URL,EstExpenses,ActualExpenditure,AnticipatedRevenue,Income,PredecessorTaskProjectId,SurveyorAccountId,BySalesOpportunitiesId,ByClientInfoId,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId from ProjectInfo ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.ProjectInfo model=new XinYiOffice.Model.ProjectInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ParentProjectId"]!=null && ds.Tables[0].Rows[0]["ParentProjectId"].ToString()!="")
				{
					model.ParentProjectId=int.Parse(ds.Tables[0].Rows[0]["ParentProjectId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ProjectPropertie"]!=null && ds.Tables[0].Rows[0]["ProjectPropertie"].ToString()!="")
				{
					model.ProjectPropertie=ds.Tables[0].Rows[0]["ProjectPropertie"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ProjectTypeId"]!=null && ds.Tables[0].Rows[0]["ProjectTypeId"].ToString()!="")
				{
					model.ProjectTypeId=int.Parse(ds.Tables[0].Rows[0]["ProjectTypeId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Title"]!=null && ds.Tables[0].Rows[0]["Title"].ToString()!="")
				{
					model.Title=ds.Tables[0].Rows[0]["Title"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Name"]!=null && ds.Tables[0].Rows[0]["Name"].ToString()!="")
				{
					model.Name=ds.Tables[0].Rows[0]["Name"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ProjectManagerAccountId"]!=null && ds.Tables[0].Rows[0]["ProjectManagerAccountId"].ToString()!="")
				{
					model.ProjectManagerAccountId=int.Parse(ds.Tables[0].Rows[0]["ProjectManagerAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["OrganiserAccountId"]!=null && ds.Tables[0].Rows[0]["OrganiserAccountId"].ToString()!="")
				{
					model.OrganiserAccountId=int.Parse(ds.Tables[0].Rows[0]["OrganiserAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ExecutorAccountId"]!=null && ds.Tables[0].Rows[0]["ExecutorAccountId"].ToString()!="")
				{
					model.ExecutorAccountId=int.Parse(ds.Tables[0].Rows[0]["ExecutorAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CompletionTime"]!=null && ds.Tables[0].Rows[0]["CompletionTime"].ToString()!="")
				{
					model.CompletionTime=DateTime.Parse(ds.Tables[0].Rows[0]["CompletionTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ActualFinishTime"]!=null && ds.Tables[0].Rows[0]["ActualFinishTime"].ToString()!="")
				{
					model.ActualFinishTime=DateTime.Parse(ds.Tables[0].Rows[0]["ActualFinishTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EstDisbursement"]!=null && ds.Tables[0].Rows[0]["EstDisbursement"].ToString()!="")
				{
					model.EstDisbursement=decimal.Parse(ds.Tables[0].Rows[0]["EstDisbursement"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ActualPayment"]!=null && ds.Tables[0].Rows[0]["ActualPayment"].ToString()!="")
				{
					model.ActualPayment=decimal.Parse(ds.Tables[0].Rows[0]["ActualPayment"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ActualStartTime"]!=null && ds.Tables[0].Rows[0]["ActualStartTime"].ToString()!="")
				{
					model.ActualStartTime=DateTime.Parse(ds.Tables[0].Rows[0]["ActualStartTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EstTimeStart"]!=null && ds.Tables[0].Rows[0]["EstTimeStart"].ToString()!="")
				{
					model.EstTimeStart=DateTime.Parse(ds.Tables[0].Rows[0]["EstTimeStart"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CompleteWorkDay"]!=null && ds.Tables[0].Rows[0]["CompleteWorkDay"].ToString()!="")
				{
					model.CompleteWorkDay=int.Parse(ds.Tables[0].Rows[0]["CompleteWorkDay"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ActualWorkingDay"]!=null && ds.Tables[0].Rows[0]["ActualWorkingDay"].ToString()!="")
				{
					model.ActualWorkingDay=int.Parse(ds.Tables[0].Rows[0]["ActualWorkingDay"].ToString());
				}
				if(ds.Tables[0].Rows[0]["State"]!=null && ds.Tables[0].Rows[0]["State"].ToString()!="")
				{
					model.State=int.Parse(ds.Tables[0].Rows[0]["State"].ToString());
				}
				if(ds.Tables[0].Rows[0]["JobDescription"]!=null && ds.Tables[0].Rows[0]["JobDescription"].ToString()!="")
				{
					model.JobDescription=ds.Tables[0].Rows[0]["JobDescription"].ToString();
				}
				if(ds.Tables[0].Rows[0]["TaskDescription"]!=null && ds.Tables[0].Rows[0]["TaskDescription"].ToString()!="")
				{
					model.TaskDescription=ds.Tables[0].Rows[0]["TaskDescription"].ToString();
				}
				if(ds.Tables[0].Rows[0]["URL"]!=null && ds.Tables[0].Rows[0]["URL"].ToString()!="")
				{
					model.URL=ds.Tables[0].Rows[0]["URL"].ToString();
				}
				if(ds.Tables[0].Rows[0]["EstExpenses"]!=null && ds.Tables[0].Rows[0]["EstExpenses"].ToString()!="")
				{
					model.EstExpenses=decimal.Parse(ds.Tables[0].Rows[0]["EstExpenses"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ActualExpenditure"]!=null && ds.Tables[0].Rows[0]["ActualExpenditure"].ToString()!="")
				{
					model.ActualExpenditure=decimal.Parse(ds.Tables[0].Rows[0]["ActualExpenditure"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AnticipatedRevenue"]!=null && ds.Tables[0].Rows[0]["AnticipatedRevenue"].ToString()!="")
				{
					model.AnticipatedRevenue=decimal.Parse(ds.Tables[0].Rows[0]["AnticipatedRevenue"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Income"]!=null && ds.Tables[0].Rows[0]["Income"].ToString()!="")
				{
					model.Income=decimal.Parse(ds.Tables[0].Rows[0]["Income"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PredecessorTaskProjectId"]!=null && ds.Tables[0].Rows[0]["PredecessorTaskProjectId"].ToString()!="")
				{
					model.PredecessorTaskProjectId=int.Parse(ds.Tables[0].Rows[0]["PredecessorTaskProjectId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SurveyorAccountId"]!=null && ds.Tables[0].Rows[0]["SurveyorAccountId"].ToString()!="")
				{
					model.SurveyorAccountId=int.Parse(ds.Tables[0].Rows[0]["SurveyorAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BySalesOpportunitiesId"]!=null && ds.Tables[0].Rows[0]["BySalesOpportunitiesId"].ToString()!="")
				{
					model.BySalesOpportunitiesId=int.Parse(ds.Tables[0].Rows[0]["BySalesOpportunitiesId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ByClientInfoId"]!=null && ds.Tables[0].Rows[0]["ByClientInfoId"].ToString()!="")
				{
					model.ByClientInfoId=int.Parse(ds.Tables[0].Rows[0]["ByClientInfoId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Remarks"]!=null && ds.Tables[0].Rows[0]["Remarks"].ToString()!="")
				{
					model.Remarks=ds.Tables[0].Rows[0]["Remarks"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,ParentProjectId,ProjectPropertie,ProjectTypeId,Title,Name,ProjectManagerAccountId,OrganiserAccountId,ExecutorAccountId,CompletionTime,ActualFinishTime,EstDisbursement,ActualPayment,ActualStartTime,EstTimeStart,CompleteWorkDay,ActualWorkingDay,State,JobDescription,TaskDescription,URL,EstExpenses,ActualExpenditure,AnticipatedRevenue,Income,PredecessorTaskProjectId,SurveyorAccountId,BySalesOpportunitiesId,ByClientInfoId,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM ProjectInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,ParentProjectId,ProjectPropertie,ProjectTypeId,Title,Name,ProjectManagerAccountId,OrganiserAccountId,ExecutorAccountId,CompletionTime,ActualFinishTime,EstDisbursement,ActualPayment,ActualStartTime,EstTimeStart,CompleteWorkDay,ActualWorkingDay,State,JobDescription,TaskDescription,URL,EstExpenses,ActualExpenditure,AnticipatedRevenue,Income,PredecessorTaskProjectId,SurveyorAccountId,BySalesOpportunitiesId,ByClientInfoId,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM ProjectInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ProjectInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from ProjectInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ProjectInfo";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

