﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace XinYiOffice.Web.Sys.SearchConfig
{
    public partial class Show : Page
    {        
        		public string strid=""; 
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
				{
					strid = Request.Params["id"];
					int Id=(Convert.ToInt32(strid));
					ShowInfo(Id);
				}
			}
		}
		
	private void ShowInfo(int Id)
	{
		XinYiOffice.BLL.SearchConfig bll=new XinYiOffice.BLL.SearchConfig();
		XinYiOffice.Model.SearchConfig model=bll.GetModel(Id);
		this.lblId.Text=model.Id.ToString();
		this.lblGuid.Text=model.Guid;
		this.lblName.Text=model.Name;
		this.lblTableName.Text=model.TableName;
		this.lblCreateAccountId.Text=model.CreateAccountId.ToString();
		this.lblRefreshAccountId.Text=model.RefreshAccountId.ToString();
		this.lblCreateTime.Text=model.CreateTime.ToString();
		this.lblRefreshTime.Text=model.RefreshTime.ToString();

	}


    }
}
