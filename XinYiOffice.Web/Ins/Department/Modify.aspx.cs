﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Ins.Department
{
    public partial class Modify : BasicPage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("DEPARTMENT_MANAGE") && ValidatePermission("DEPARTMENT_MANAGE_LIST_EDIT")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.Department bll = new XinYiOffice.BLL.Department();
            XinYiOffice.Model.Department model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtDepartmentName.Text = model.DepartmentName;
            this.txtDepartmentCon.Text = model.DepartmentCon;

            DropDownList_txtInstitutionId.DataSource = new BLL.Institution().GetAllList();
            DropDownList_txtInstitutionId.DataValueField = "Id";
            DropDownList_txtInstitutionId.DataTextField = "OrganizationName";
            DropDownList_txtInstitutionId.DataBind();
            DropDownList_txtInstitutionId.Items.Insert(0, new ListItem("请选择所属机构", "0"));

            this.DropDownList_txtInstitutionId.SelectedValue = model.InstitutionId.ToString();
            HiddenField_txtTorchbearerAccountId.Value = model.InstitutionId.ToString();

            this.txtTorchbearerAccountId.Text = model.TorchbearerAccountId.ToString();
            this.txtPhoneCode.Text = model.PhoneCode;
            this.txtPhoneCode2.Text = model.PhoneCode2;
            this.txtFax.Text = model.Fax;
            this.txtZipCode.Text = model.ZipCode;
            this.txtAddress.Text = model.Address;
            this.txtCreateAccountId.Value = model.CreateAccountId.ToString();
            this.txtRefreshAccountId.Value = model.RefreshAccountId.ToString();
            this.txtCreateTime.Value = model.CreateTime.ToString();
            this.txtRefreshTime.Value = model.RefreshTime.ToString();

        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string DepartmentName = this.txtDepartmentName.Text;
            string DepartmentCon = this.txtDepartmentCon.Text;
            int InstitutionId = SafeConvert.ToInt(this.DropDownList_txtInstitutionId.SelectedValue);

            int TorchbearerAccountId = int.Parse(this.txtTorchbearerAccountId.Text);
            string PhoneCode = this.txtPhoneCode.Text;
            string PhoneCode2 = this.txtPhoneCode2.Text;
            string Fax = this.txtFax.Text;
            string ZipCode = this.txtZipCode.Text;
            string Address = this.txtAddress.Text;
            int CreateAccountId = int.Parse(this.txtCreateAccountId.Value);
            int RefreshAccountId = int.Parse(this.txtRefreshAccountId.Value);
            DateTime CreateTime = DateTime.Parse(this.txtCreateTime.Value);
            DateTime RefreshTime = DateTime.Parse(this.txtRefreshTime.Value);


            XinYiOffice.Model.Department model = new XinYiOffice.Model.Department();
            model.Id = Id;
            model.DepartmentName = DepartmentName;
            model.DepartmentCon = DepartmentCon;
            model.InstitutionId = InstitutionId;
            model.TorchbearerAccountId = TorchbearerAccountId;
            model.PhoneCode = PhoneCode;
            model.PhoneCode2 = PhoneCode2;
            model.Fax = Fax;
            model.ZipCode = ZipCode;
            model.Address = Address;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.Department bll = new XinYiOffice.BLL.Department();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
