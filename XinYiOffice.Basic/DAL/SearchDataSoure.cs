﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:SearchDataSoure
	/// </summary>
	public partial class SearchDataSoure
	{
		public SearchDataSoure()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "SearchDataSoure"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from SearchDataSoure");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.SearchDataSoure model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SearchDataSoure(");
			strSql.Append("DataSoureType,DataSoureConntion,DataType,DisplayValue,TrueValue,FormType,FormStyle,FormClass,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@DataSoureType,@DataSoureConntion,@DataType,@DisplayValue,@TrueValue,@FormType,@FormStyle,@FormClass,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@DataSoureType", SqlDbType.Int,4),
					new SqlParameter("@DataSoureConntion", SqlDbType.VarChar,4000),
					new SqlParameter("@DataType", SqlDbType.Int,4),
					new SqlParameter("@DisplayValue", SqlDbType.VarChar,4000),
					new SqlParameter("@TrueValue", SqlDbType.VarChar,4000),
					new SqlParameter("@FormType", SqlDbType.Int,4),
					new SqlParameter("@FormStyle", SqlDbType.VarChar,4000),
					new SqlParameter("@FormClass", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.DataSoureType;
			parameters[1].Value = model.DataSoureConntion;
			parameters[2].Value = model.DataType;
			parameters[3].Value = model.DisplayValue;
			parameters[4].Value = model.TrueValue;
			parameters[5].Value = model.FormType;
			parameters[6].Value = model.FormStyle;
			parameters[7].Value = model.FormClass;
			parameters[8].Value = model.CreateAccountId;
			parameters[9].Value = model.RefreshAccountId;
			parameters[10].Value = model.CreateTime;
			parameters[11].Value = model.RefreshTime;
			parameters[12].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.SearchDataSoure model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SearchDataSoure set ");
			strSql.Append("DataSoureType=@DataSoureType,");
			strSql.Append("DataSoureConntion=@DataSoureConntion,");
			strSql.Append("DataType=@DataType,");
			strSql.Append("DisplayValue=@DisplayValue,");
			strSql.Append("TrueValue=@TrueValue,");
			strSql.Append("FormType=@FormType,");
			strSql.Append("FormStyle=@FormStyle,");
			strSql.Append("FormClass=@FormClass,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@DataSoureType", SqlDbType.Int,4),
					new SqlParameter("@DataSoureConntion", SqlDbType.VarChar,4000),
					new SqlParameter("@DataType", SqlDbType.Int,4),
					new SqlParameter("@DisplayValue", SqlDbType.VarChar,4000),
					new SqlParameter("@TrueValue", SqlDbType.VarChar,4000),
					new SqlParameter("@FormType", SqlDbType.Int,4),
					new SqlParameter("@FormStyle", SqlDbType.VarChar,4000),
					new SqlParameter("@FormClass", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.DataSoureType;
			parameters[1].Value = model.DataSoureConntion;
			parameters[2].Value = model.DataType;
			parameters[3].Value = model.DisplayValue;
			parameters[4].Value = model.TrueValue;
			parameters[5].Value = model.FormType;
			parameters[6].Value = model.FormStyle;
			parameters[7].Value = model.FormClass;
			parameters[8].Value = model.CreateAccountId;
			parameters[9].Value = model.RefreshAccountId;
			parameters[10].Value = model.CreateTime;
			parameters[11].Value = model.RefreshTime;
			parameters[12].Value = model.TenantId;
			parameters[13].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SearchDataSoure ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SearchDataSoure ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.SearchDataSoure GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,DataSoureType,DataSoureConntion,DataType,DisplayValue,TrueValue,FormType,FormStyle,FormClass,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId from SearchDataSoure ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.SearchDataSoure model=new XinYiOffice.Model.SearchDataSoure();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DataSoureType"]!=null && ds.Tables[0].Rows[0]["DataSoureType"].ToString()!="")
				{
					model.DataSoureType=int.Parse(ds.Tables[0].Rows[0]["DataSoureType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DataSoureConntion"]!=null && ds.Tables[0].Rows[0]["DataSoureConntion"].ToString()!="")
				{
					model.DataSoureConntion=ds.Tables[0].Rows[0]["DataSoureConntion"].ToString();
				}
				if(ds.Tables[0].Rows[0]["DataType"]!=null && ds.Tables[0].Rows[0]["DataType"].ToString()!="")
				{
					model.DataType=int.Parse(ds.Tables[0].Rows[0]["DataType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DisplayValue"]!=null && ds.Tables[0].Rows[0]["DisplayValue"].ToString()!="")
				{
					model.DisplayValue=ds.Tables[0].Rows[0]["DisplayValue"].ToString();
				}
				if(ds.Tables[0].Rows[0]["TrueValue"]!=null && ds.Tables[0].Rows[0]["TrueValue"].ToString()!="")
				{
					model.TrueValue=ds.Tables[0].Rows[0]["TrueValue"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FormType"]!=null && ds.Tables[0].Rows[0]["FormType"].ToString()!="")
				{
					model.FormType=int.Parse(ds.Tables[0].Rows[0]["FormType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["FormStyle"]!=null && ds.Tables[0].Rows[0]["FormStyle"].ToString()!="")
				{
					model.FormStyle=ds.Tables[0].Rows[0]["FormStyle"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FormClass"]!=null && ds.Tables[0].Rows[0]["FormClass"].ToString()!="")
				{
					model.FormClass=ds.Tables[0].Rows[0]["FormClass"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,DataSoureType,DataSoureConntion,DataType,DisplayValue,TrueValue,FormType,FormStyle,FormClass,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM SearchDataSoure ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,DataSoureType,DataSoureConntion,DataType,DisplayValue,TrueValue,FormType,FormStyle,FormClass,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM SearchDataSoure ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM SearchDataSoure ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from SearchDataSoure T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "SearchDataSoure";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

