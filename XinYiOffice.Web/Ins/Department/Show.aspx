﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"   AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Ins.Department.Show" Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">
<div class="setup_box" style=" margin-bottom:0">
<div class="h_title"><h4>部门信息</h4></div>
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		部门名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDepartmentName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		部门简介
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDepartmentCon" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属机构
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblInstitutionId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		负责人
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblTorchbearerAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		电话号码
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPhoneCode" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		电话号码
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPhoneCode2" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		传真
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblFax" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		邮编
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblZipCode" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		地址
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblAddress" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		添加者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		更新者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRefreshAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		创建时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateTime" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		更新时间 ：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRefreshTime" runat="server"></asp:Label>
	</td></tr>
</table>
            </form>
</div>


            </asp:Content>
