﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Finance.MakeCollections
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.ValidatePermission("MAKECOLLECTIONS_MANAGE_LIST_ADD"))
            {
                base.NoPermissionPage();
            }

            if (!IsPostBack)
            {
                InitData();
            }
        }

        protected void InitData()
        {
            txtDocumentNumber.Text = string.Format("MC-{0}", DateTime.Now.ToString("yyyyMMddhhssmm"));

            DataTable dtAccountList = AccountsServer.GetAccountAllList(CurrentTenantId);

            DropDownList_GatheringAccountId.DataSource = dtAccountList;
            DropDownList_GatheringAccountId.DataTextField = "FullName";
            DropDownList_GatheringAccountId.DataValueField = "Id";
            DropDownList_GatheringAccountId.DataBind();

            //银行帐号
            DropDownList_GatheringBankAccountId.DataSource = new BLL.BankAccount().GetList(string.Format("1=1 and TenantId={0}", CurrentTenantId)); ;
            DropDownList_GatheringBankAccountId.DataTextField = "BankAccountName";
            DropDownList_GatheringBankAccountId.DataValueField = "Id";
            DropDownList_GatheringBankAccountId.Items.Insert(0, new ListItem("暂无可选", "0"));
            DropDownList_GatheringBankAccountId.DataBind();

            SetDropDownList_MakeCollections("MethodPayment", ref DropDownList_MethodPayment);
            SetDropDownList_MakeCollections("Sate", ref DropDownList_Sate);
        }

        protected void SetDropDownList_MakeCollections(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.MakeCollectionsDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

            try
            {
                string DocumentNumber = this.txtDocumentNumber.Text;
                string DocumentDescription = this.txtDocumentDescription.Text;
                int MethodPayment = SafeConvert.ToInt(DropDownList_MethodPayment.SelectedValue);
                string SupplierName = this.txtSupplierName.Text;
                string GatheringFullName = this.txtGatheringFullName.Text;
                int GatheringAccountId = SafeConvert.ToInt(DropDownList_GatheringAccountId.SelectedValue);
                DateTime ReceiptDate = SafeConvert.ToDateTime(this.txtReceiptDate.Text);
                DateTime TargetDate = SafeConvert.ToDateTime(this.txtTargetDate.Text);
                string Remark = this.txtRemark.Text;
                string ClientName = this.txtClientName.Text;
                decimal GatheringAmount = SafeConvert.ToDecimal(txtGatheringAmount.Text);
                int GatheringBankAccountId = SafeConvert.ToInt(this.DropDownList_GatheringBankAccountId.SelectedValue, 0);
                int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue);
                int CreateAccountId = SafeConvert.ToInt(CurrentAccountId);
                DateTime CreateTime = DateTime.Now;

                XinYiOffice.Model.MakeCollections model = new XinYiOffice.Model.MakeCollections();
                model.DocumentNumber = DocumentNumber;
                model.DocumentDescription = DocumentDescription;
                model.MethodPayment = MethodPayment;
                model.SupplierName = SupplierName;
                model.GatheringFullName = GatheringFullName;
                model.GatheringAccountId = GatheringAccountId;
                model.ReceiptDate = ReceiptDate;
                model.TargetDate = TargetDate;
                model.Remark = Remark;
                model.ClientName = ClientName;
                model.GatheringAmount = GatheringAmount;
                model.GatheringBankAccountId = GatheringBankAccountId;
                model.Sate = Sate;
                model.CreateAccountId = CreateAccountId;
                model.CreateTime = CreateTime;
                model.TenantId = CurrentTenantId;

                if (Sate == 1)
                {

                    #region //更新银行帐号资金
                    try
                    {
                        Model.BankAccount ba = new Model.BankAccount();
                        ba = new BLL.BankAccount().GetModel(SafeConvert.ToInt(model.GatheringBankAccountId));
                        ba.CashIn = ba.CashIn + SafeConvert.ToDecimal(model.GatheringAmount);
                        ba.TenantId = CurrentTenantId;
                        new BLL.BankAccount().Update(ba);
                    }
                    catch (Exception ex)
                    {
                        EasyLog.WriteLog(ex);

                    }
                    finally
                    {
                    }
                    #endregion
                }


                XinYiOffice.BLL.MakeCollections bll = new XinYiOffice.BLL.MakeCollections();
                bll.Add(model);

                xytools.web_alert("保存成功！", "List.aspx?state=" + Sate);

            }
            catch (Exception ex)
            {
                EasyLog.WriteLog(ex);
            }
            finally
            {

            }
        }
    }
}
