﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Discuss
{
    public partial class Add : BasicPage
    {
        public int ProjectId = 0;
        public int ClassId = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("DISCUSS_LIST_ADD"))
            {
                base.NoPermissionPage();
            }

            if(!IsPostBack)
            {
                InitData();
            }
        }

        public void InitData()
        {
            ProjectId = SafeConvert.ToInt(xytools.url_get("ProjectId"), 0);
            ClassId = SafeConvert.ToInt(xytools.url_get("ClassId"), 0);

            Model.ProjectInfo pi = new Model.ProjectInfo();
            Model.DiscussClass dis=new Model.DiscussClass();

            pi = new BLL.ProjectInfo().GetModel(ProjectId);
            dis=new BLL.DiscussClass().GetModel(ClassId);

            Label_Project.Text = pi.Title;
            HiddenField_ProjectId.Value =pi.Id.ToString();

            Label_DiscussClass.Text = dis.Name;
            HiddenField_ClassId.Value = dis.Id.ToString();

        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int ProjectInfoId = SafeConvert.ToInt(HiddenField_ProjectId.Value);
            int DiscussClassId = SafeConvert.ToInt(HiddenField_ClassId.Value);
            string Title = this.txtTitle.Text;
            string Con = this.txtCon.Text;
            int IsTop = SafeConvert.ToInt(DropDownList_IsTop.SelectedValue,0);
            int IsShow = SafeConvert.ToInt(DropDownList_IsShow.SelectedValue,0);
            int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue,0);
            int Sort = int.Parse(this.txtSort.Text);


            XinYiOffice.Model.Discuss model = new XinYiOffice.Model.Discuss();
            model.ParentDiscussId = 0;
            model.ProjectInfoId = ProjectInfoId;
            model.DiscussClassId = DiscussClassId;
            model.Title = Title;
            model.Con = Con;
            model.IsTop = IsTop;
            model.IsShow = IsShow;
            model.Sate = Sate;
            model.Sort = Sort;
            model.CreateAccountId = CurrentAccountId;
            model.RefreshAccountId = CurrentAccountId;
            model.CreateTime = DateTime.Now ;
            model.RefreshTime =DateTime.Now;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.Discuss bll = new XinYiOffice.BLL.Discuss();
            bll.Add(model);
            xytools.web_alert("保存成功！", string.Format("list.aspx?ProjectId={0}&ClassId={1}", model.ProjectInfoId, model.DiscussClassId));
        }
    }
}
