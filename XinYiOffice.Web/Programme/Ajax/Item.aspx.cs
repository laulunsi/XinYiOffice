﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Programme.Ajax
{
    public partial class Item : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string action = xytools.url_get("action");
            switch (action)
            {
                case "addProgramme":
                    SetProgramme();
                    break;
            }
        }

        protected void SetProgramme()
        {
            try
            {

                Model.Programme p = new Model.Programme();
                p.CreateAccountId = CurrentAccountId;
                p.CreateTime = DateTime.Now;
                p.EndTime = SafeConvert.ToDateTime(xytools.url_get("txt_EndTime") + " " + xytools.url_get("sel_EndTime2"));
                p.InstitutionId = 0;
                p.IsRepeat = SafeConvert.ToInt(xytools.url_get("rad_IsRepeat"));
                p.Locale = SafeConvert.ToString(xytools.url_get("sel_Locale"));
                p.Note = SafeConvert.ToString(xytools.url_get("txt_Note"));
                p.ProjectInfoId = 0;
                p.RefreshAccountId = CurrentAccountId;
                p.RefreshTime = DateTime.Now;
                p.RepeatInterval = SafeConvert.ToInt(xytools.url_get("sel_RepeatInterval"));
                p.ScheduleType = SafeConvert.ToInt(xytools.url_get("sel_ScheduleType"));
                p.StartTime = SafeConvert.ToDateTime(xytools.url_get("txt_StartTime") + " " + xytools.url_get("sel_StartTime2"));
                p.Title = SafeConvert.ToString(xytools.url_get("txt_Title"));

                p.IsRemind = SafeConvert.ToInt(xytools.url_get("rad_IsRemind"));
                p.RemindDay = SafeConvert.ToInt(xytools.url_get("txt_RemindDay"));
                p.UpToDate = SafeConvert.ToDateTime(xytools.url_get("txt_UpToDate"));
                p.TenantId = CurrentTenantId;

                p.Id=new BLL.Programme().Add(p);

                //日程组添加 hidMembersAccountId
                string hidMembersAccountId = SafeConvert.ToString(xytools.url_get("hidMembersAccountId"));
                if (!string.IsNullOrEmpty(hidMembersAccountId))
                {
                    string[] MembersAccountId = hidMembersAccountId.TrimEnd(',').Split(',');

                    if (MembersAccountId.Length == 1)
                    {
                        //自己定义
                        Model.ProgrammeMembers pm = new Model.ProgrammeMembers();
                        pm.AccountId = CurrentAccountId;
                        pm.ByAccountId = 0;
                        pm.CreateAccountId = CurrentAccountId;
                        pm.CreateTime = DateTime.Now;
                        pm.ProgrammeId = p.Id;
                        pm.RefreshAccountId = CurrentAccountId;
                        pm.RefreshTime = DateTime.Now;
                        pm.TenantId = CurrentTenantId;

                        new BLL.ProgrammeMembers().Add(pm);
                    }
                    else
                    {
                        foreach (string _id in MembersAccountId)
                        {
                            Model.ProgrammeMembers pm = new Model.ProgrammeMembers();
                            pm.AccountId = SafeConvert.ToInt(_id);
                            pm.ByAccountId =CurrentAccountId;
                            pm.CreateAccountId = CurrentAccountId;
                            pm.CreateTime = DateTime.Now;
                            pm.ProgrammeId = p.Id;
                            pm.RefreshAccountId = CurrentAccountId;
                            pm.RefreshTime = DateTime.Now;
                            pm.TenantId = CurrentTenantId;

                            new BLL.ProgrammeMembers().Add(pm);
                        }
                    }


                }
                else
                {
                    //自己定义
                    Model.ProgrammeMembers pm = new Model.ProgrammeMembers();
                    pm.AccountId = CurrentAccountId;
                    pm.ByAccountId = 0;
                    pm.CreateAccountId = CurrentAccountId;
                    pm.CreateTime = DateTime.Now;
                    pm.ProgrammeId=p.Id;
                    pm.RefreshAccountId = CurrentAccountId;
                    pm.RefreshTime = DateTime.Now;
                    pm.TenantId = CurrentTenantId;

                    new BLL.ProgrammeMembers().Add(pm);
                }

                xytools.write("1");
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
                xytools.write("-1");
            }
            finally
            { 
            }


        }
    }
}