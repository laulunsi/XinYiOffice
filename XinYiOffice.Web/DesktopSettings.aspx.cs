﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;
using System.Data;

using XinYiOffice.Common;

namespace XinYiOffice.Web
{
    public partial class DesktopSettings : BasicPage
    {
        public DataTable myDesktop;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!ValidatePermission("DESKTOPPLUG_BASIC")) 
            {
                base.NoPermissionPage();
            }

            if (!IsPostBack)
            {
                InitData();
            }
        }

        protected void InitData()
        {
            myDesktop = DesktopServer.GetMyDesktop(CurrentAccountId,CurrentTenantId);
            repMyDesktopSettings.DataSource = DesktopServer.GetDesktopAllPlug();
            repMyDesktopSettings.DataBind();
        }


        protected void repMyDesktopSettings_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;

            CheckBox cb = (CheckBox)e.Item.FindControl("ck") as CheckBox;
            TextBox tb = (TextBox)e.Item.FindControl("txt_sort") as TextBox;
            HiddenField hidDesktopId = (HiddenField)e.Item.FindControl("hid_DesktopId") as HiddenField;
            HiddenField hidDesktopPlugId = (HiddenField)e.Item.FindControl("hid_DesktopPlugId") as HiddenField;

            hidDesktopPlugId.Value = SafeConvert.ToString(drv["Id"]);

            if (!ValidatePermission("DESKTOPPLUG_" + hidDesktopPlugId.Value))
            {
                e.Item.Visible = false;
                
                return;
               
            }

            DataRow[] IsExist = myDesktop.Select(string.Format("DesktopPlugId={0}", drv["Id"]));
            
            if (IsExist.Length > 0)
            {
                cb.Checked = true;
                tb.Text = SafeConvert.ToString(IsExist[0]["Sort"]);
                hidDesktopId.Value = SafeConvert.ToString(IsExist[0]["Id"]);
            }
            else
            {
                cb.Checked = false;
                tb.Text = "0";
                hidDesktopId.Value = "0";
            }

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < repMyDesktopSettings.Items.Count; i++)
            {
                CheckBox cb = (CheckBox)repMyDesktopSettings.Items[i].FindControl("ck") as CheckBox;
                TextBox tb = (TextBox)repMyDesktopSettings.Items[i].FindControl("txt_sort") as TextBox;
                HiddenField hidDesktopId = (HiddenField)repMyDesktopSettings.Items[i].FindControl("hid_DesktopId") as HiddenField;
                HiddenField hidDesktopPlugId = (HiddenField)repMyDesktopSettings.Items[i].FindControl("hid_DesktopPlugId") as HiddenField;

                if (cb.Checked)
                {
                    if (hidDesktopId.Value != "0")
                    {
                        Model.Desktop _desktop = new BLL.Desktop().GetModel(SafeConvert.ToInt(hidDesktopId.Value));
                        _desktop.RefreshAccountId = CurrentAccountId;
                        _desktop.Sort = SafeConvert.ToInt(tb.Text, 1);//更新排序
                        _desktop.TenantId = CurrentTenantId;

                        new BLL.Desktop().Update(_desktop);
                    }
                    else
                    {
                        //新建
                        Model.Desktop _desktop = new Model.Desktop();
                        _desktop.AccountId = CurrentAccountId;
                        _desktop.CreateAccountId = CurrentAccountId;
                        _desktop.CreateTime = DateTime.Now;
                        _desktop.DesktopPlugId = SafeConvert.ToInt(hidDesktopPlugId.Value);
                        _desktop.Sort = SafeConvert.ToInt(tb.Text, 1);
                        _desktop.TenantId = CurrentTenantId;

                        new BLL.Desktop().Add(_desktop);
                    }
                }
                else
                {
                    //若已经存在则删除
                    if (hidDesktopId.Value != "0")
                    {
                        new BLL.Desktop().Delete(SafeConvert.ToInt(hidDesktopId.Value));
                    }

                }

            }

            xytools.web_alert_new_url("保存成功!");
        }



    }
}