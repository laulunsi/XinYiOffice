﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Web.UI;
using XinYiOffice.Common;

namespace XinYiOffice.Web.Finance.PayrollControl
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("PAYROLLCONTROL_MANAGE") && base.ValidatePermission("PAYROLLCONTROL_MANAGE_LIST_VIEW")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            //XinYiOffice.BLL.PayrollControl bll = new XinYiOffice.BLL.PayrollControl();
            //XinYiOffice.Model.PayrollControl model = bll.GetModel(Id);

            try
            {

                DataSet ds = DbHelperSQL.Query(string.Format("select * from  vPayrollControl where Id={0}", Id));
                DataRow dr = ds.Tables[0].Rows[0];


                this.lblId.Text = SafeConvert.ToString(dr["Id"]);
                this.lblPayDate.Text = SafeConvert.ToString(dr["PayDate"]);
                this.lblBasePay.Text = SafeConvert.ToString(dr["BasePay"]);
                this.lblAttendanceWages.Text = SafeConvert.ToString(dr["AttendanceWages"]);
                this.lblCommunicationExpense.Text = SafeConvert.ToString(dr["CommunicationExpense"]);
                this.lblPersonalIncomeTax.Text = SafeConvert.ToString(dr["PersonalIncomeTax"]);
                this.lblHousingFund.Text = SafeConvert.ToString(dr["HousingFund"]);
                this.lblResidualAmount.Text = SafeConvert.ToString(dr["ResidualAmount"]);
                this.lblAllWages.Text = SafeConvert.ToString(dr["AllWages"]);
                this.lblOvertimeWage.Text = SafeConvert.ToString(dr["OvertimeWage"]);
                this.lblPercentageWages.Text = SafeConvert.ToString(dr["PercentageWages"]);
                this.lblOtherBenefits.Text = SafeConvert.ToString(dr["OtherBenefits"]);
                this.lblSocialSecurity.Text = SafeConvert.ToString(dr["SocialSecurity"]);
                this.lblDeductMoney.Text = SafeConvert.ToString(dr["DeductMoney"]);
                this.lblRepaymentAmount.Text = SafeConvert.ToString(dr["RepaymentAmount"]);
                this.lblActualPayment.Text = SafeConvert.ToString(dr["ActualPayment"]);
                this.lblBankAccountId.Text = SafeConvert.ToString(dr["BankAccountName"]);
                this.lblPersonnelAccount.Text = SafeConvert.ToString(dr["PersonnelAccountName"]);
                this.lblSate.Text = SafeConvert.ToString(dr["SateName"]);
                this.lblCreateAccountId.Text = SafeConvert.ToString(dr["CreateAccountName"]);
                this.lblCreateTime.Text = SafeConvert.ToString(dr["CreateTime"]);

            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
            }
            finally
            { 
            }


        }


    }
}
