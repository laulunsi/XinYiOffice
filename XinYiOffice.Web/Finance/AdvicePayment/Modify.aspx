﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Finance.AdvicePayment.Modify" Title="修改页" %>


<asp:Content ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>付款单修改</h4></div>

  <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		单据编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDocumentNumber" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		单据描述
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDocumentDescription" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款方式 ：</td>
	<td height="25" width="*" align="left">
	    <asp:DropDownList ID="DropDownList_MethodPayment" runat="server">
        </asp:DropDownList>
	&nbsp;现金,银行转
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		供应商
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSupplierName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		客户名称 ：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtClientName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款人
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPayeeFullName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款人帐号 ：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_PayeeAccountId" runat="server" class="chzn-select"   data-placeholder="点击选择人...">
        </asp:DropDownList>
	&nbsp;没有则为0
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		实际付款日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtPaymentDate" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		预定付款日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtTargetDate" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		摘要
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRemark" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPaymentAmount" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款银行账户
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_PaymentBankAccountId" runat="server" class="chzn-select"   data-placeholder="选择银行帐号" >
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态 ：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_Sate" runat="server">
        </asp:DropDownList>
	&nbsp;0-应付,1-已付 
        <asp:HiddenField ID="txtCreateTime" runat="server" />
        <asp:HiddenField ID="txtCreateAccountId" runat="server" />
	</td></tr>
	</table>

</div>

<div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>
    </form>

</asp:Content>

 