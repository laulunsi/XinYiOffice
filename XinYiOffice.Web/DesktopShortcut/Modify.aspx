﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.DesktopShortcut.Modify" Title="修改页" %>

<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		快捷方式名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		排序 1-10排序
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSort" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		菜单ID
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtMenuTreeId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		添加者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCreateAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		更新者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRefreshAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		创建时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtCreateTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		更新时间 最近一次编辑的人员I
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtRefreshTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
</table>
</asp:Content>

 