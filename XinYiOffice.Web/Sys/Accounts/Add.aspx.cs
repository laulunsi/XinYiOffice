﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.Accounts
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!(ValidatePermission("ACCOUNT_MANAGE") && ValidatePermission("ACCOUNT_LIST_ADD"))) 
            {
                base.NoPermissionPage();
            }
        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string AccountName = this.txtAccountName.Text;
            string AccountPassword = this.txtAccountPassword.Text;
            int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue, 0);
            string NiceName = this.txtNiceName.Text;

            string EnName = this.txtEnName.Text;
            int TimeZone = SafeConvert.ToInt(DropDownList_TimeZone.SelectedValue,0);
            string Email = this.txtEmail.Text;
            string HeadPortrait = SafeConvert.ToString(DropDownList_HeadPortrait.SelectedValue);
            string Phone = this.txtPhone.Text;
            int AccountType = SafeConvert.ToInt(DropDownList_AccountType.SelectedValue,0);
            int KeyId = SafeConvert.ToInt(HiddenField_KeyId.Value,0);

            XinYiOffice.Model.Accounts model = new XinYiOffice.Model.Accounts();
            model.AccountName = AccountName;
            model.AccountPassword = AccountPassword;
            model.Sate = Sate;
            model.NiceName = NiceName;
            model.FullName = txtFullName.Text;

            model.EnName = EnName;
            model.TimeZone = TimeZone;
            model.Email = Email;
            model.HeadPortrait = HeadPortrait;
            model.Phone = Phone;
            model.AccountType = AccountType;
            model.KeyId = KeyId;
            model.CreateAccountId = CurrentAccountId;
            model.CreateTime = DateTime.Now;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.Accounts bll = new XinYiOffice.BLL.Accounts();
            bll.Add(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
