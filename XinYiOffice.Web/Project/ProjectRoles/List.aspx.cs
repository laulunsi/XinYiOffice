﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Project.ProjectRoles
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.ProjectRoles bll = new XinYiOffice.BLL.ProjectRoles();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("PROJECTROLES_LIST")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                //gridView.BorderColor = ColorTranslator.FromHtml(Application[Session["Style"].ToString() + "xtable_bordercolorlight"].ToString());
                //gridView.HeaderStyle.BackColor = ColorTranslator.FromHtml(Application[Session["Style"].ToString() + "xtable_titlebgcolor"].ToString());
                //btnDelete.Attributes.Add("onclick", "return confirm(\"你确认要删除吗？\")");
                BindData();
            }
        }
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        
        #region gridView            
        public void BindData()
        {
            #region
            //if (!Context.User.Identity.IsAuthenticated)
            //{
            //    return;
            //}
            //AccountsPrincipal user = new AccountsPrincipal(Context.User.Identity.Name);
            //if (user.HasPermissionID(PermId_Modify))
            //{
            //    gridView.Columns[6].Visible = true;
            //}
            //if (user.HasPermissionID(PermId_Delete))
            //{
            //    gridView.Columns[7].Visible = true;
            //}
            #endregion

            DataSet ds = new DataSet();
            StringBuilder strWhere = new StringBuilder(" 1=1 ");

            repProjectRoles.DataSource = ProjectInfoServer.GetProjectRoles(strWhere.ToString(),CurrentTenantId);
            repProjectRoles.DataBind();
        }
        #endregion





    }
}
