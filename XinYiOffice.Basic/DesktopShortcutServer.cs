﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace XinYiOffice.Basic
{
    public class DesktopShortcutServer
    {
        /// <summary>
        /// 根据用户id获取我的桌面
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="topNum"></param>
        /// <returns></returns>
        public static DataTable GetMyDesktopShortcut(int accountId, int TenantId)
        {
            string mysql = string.Format("select * from vMyDesktopShortcut where CreateAccountId={0} and TenantId={1} order by Id desc", accountId, TenantId);

            return Common.DbHelperSQL.Query(mysql).Tables[0];
        }

        /// <summary>
        /// 根据用户id获取我的桌面
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="topNum"></param>
        /// <returns></returns>
        public static DataTable GetMyDesktopShortcut(int accountId, int top, int tenantId)
        {
            string mysql = string.Format("select top {1} * from vMyDesktopShortcut where CreateAccountId={0} and TenantId={2} order by Id desc", accountId, top, tenantId);

            return Common.DbHelperSQL.Query(mysql).Tables[0];
        }

    }
}
