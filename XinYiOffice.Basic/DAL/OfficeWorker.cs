﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:OfficeWorker
	/// </summary>
	public partial class OfficeWorker
	{
		public OfficeWorker()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "OfficeWorker"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from OfficeWorker");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.OfficeWorker model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into OfficeWorker(");
			strSql.Append("FullName,UsedName,Sex,Email,Tel,Phone,IntNumber,InstitutionId,DepartmentId,Position,Province,City,County,Street,ZipCode,BirthDate,ChinaID,Nationality,NativePlace,Phone1,Phone2,PoliticalStatus,EntryTime,EntranceMode,PostGrades,WageLevel,InsuranceWelfare,GraduateSchool,FormalSchooling,Major,EnglishLevel,PreWork,PrePosition,PreStartTime,PreEndTime,PreEpartment,TurnoverTime,Sate,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@FullName,@UsedName,@Sex,@Email,@Tel,@Phone,@IntNumber,@InstitutionId,@DepartmentId,@Position,@Province,@City,@County,@Street,@ZipCode,@BirthDate,@ChinaID,@Nationality,@NativePlace,@Phone1,@Phone2,@PoliticalStatus,@EntryTime,@EntranceMode,@PostGrades,@WageLevel,@InsuranceWelfare,@GraduateSchool,@FormalSchooling,@Major,@EnglishLevel,@PreWork,@PrePosition,@PreStartTime,@PreEndTime,@PreEpartment,@TurnoverTime,@Sate,@Remarks,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@FullName", SqlDbType.VarChar,4000),
					new SqlParameter("@UsedName", SqlDbType.VarChar,4000),
					new SqlParameter("@Sex", SqlDbType.Int,4),
					new SqlParameter("@Email", SqlDbType.VarChar,4000),
					new SqlParameter("@Tel", SqlDbType.VarChar,4000),
					new SqlParameter("@Phone", SqlDbType.VarChar,4000),
					new SqlParameter("@IntNumber", SqlDbType.VarChar,4000),
					new SqlParameter("@InstitutionId", SqlDbType.Int,4),
					new SqlParameter("@DepartmentId", SqlDbType.Int,4),
					new SqlParameter("@Position", SqlDbType.Int,4),
					new SqlParameter("@Province", SqlDbType.VarChar,4000),
					new SqlParameter("@City", SqlDbType.VarChar,4000),
					new SqlParameter("@County", SqlDbType.VarChar,4000),
					new SqlParameter("@Street", SqlDbType.VarChar,4000),
					new SqlParameter("@ZipCode", SqlDbType.VarChar,4000),
					new SqlParameter("@BirthDate", SqlDbType.VarChar,4000),
					new SqlParameter("@ChinaID", SqlDbType.VarChar,4000),
					new SqlParameter("@Nationality", SqlDbType.VarChar,4000),
					new SqlParameter("@NativePlace", SqlDbType.VarChar,4000),
					new SqlParameter("@Phone1", SqlDbType.VarChar,4000),
					new SqlParameter("@Phone2", SqlDbType.VarChar,4000),
					new SqlParameter("@PoliticalStatus", SqlDbType.VarChar,4000),
					new SqlParameter("@EntryTime", SqlDbType.VarChar,4000),
					new SqlParameter("@EntranceMode", SqlDbType.VarChar,4000),
					new SqlParameter("@PostGrades", SqlDbType.Int,4),
					new SqlParameter("@WageLevel", SqlDbType.Int,4),
					new SqlParameter("@InsuranceWelfare", SqlDbType.VarChar,4000),
					new SqlParameter("@GraduateSchool", SqlDbType.VarChar,4000),
					new SqlParameter("@FormalSchooling", SqlDbType.VarChar,4000),
					new SqlParameter("@Major", SqlDbType.VarChar,4000),
					new SqlParameter("@EnglishLevel", SqlDbType.VarChar,4000),
					new SqlParameter("@PreWork", SqlDbType.VarChar,4000),
					new SqlParameter("@PrePosition", SqlDbType.VarChar,4000),
					new SqlParameter("@PreStartTime", SqlDbType.VarChar,4000),
					new SqlParameter("@PreEndTime", SqlDbType.VarChar,4000),
					new SqlParameter("@PreEpartment", SqlDbType.VarChar,4000),
					new SqlParameter("@TurnoverTime", SqlDbType.VarChar,4000),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.FullName;
			parameters[1].Value = model.UsedName;
			parameters[2].Value = model.Sex;
			parameters[3].Value = model.Email;
			parameters[4].Value = model.Tel;
			parameters[5].Value = model.Phone;
			parameters[6].Value = model.IntNumber;
			parameters[7].Value = model.InstitutionId;
			parameters[8].Value = model.DepartmentId;
			parameters[9].Value = model.Position;
			parameters[10].Value = model.Province;
			parameters[11].Value = model.City;
			parameters[12].Value = model.County;
			parameters[13].Value = model.Street;
			parameters[14].Value = model.ZipCode;
			parameters[15].Value = model.BirthDate;
			parameters[16].Value = model.ChinaID;
			parameters[17].Value = model.Nationality;
			parameters[18].Value = model.NativePlace;
			parameters[19].Value = model.Phone1;
			parameters[20].Value = model.Phone2;
			parameters[21].Value = model.PoliticalStatus;
			parameters[22].Value = model.EntryTime;
			parameters[23].Value = model.EntranceMode;
			parameters[24].Value = model.PostGrades;
			parameters[25].Value = model.WageLevel;
			parameters[26].Value = model.InsuranceWelfare;
			parameters[27].Value = model.GraduateSchool;
			parameters[28].Value = model.FormalSchooling;
			parameters[29].Value = model.Major;
			parameters[30].Value = model.EnglishLevel;
			parameters[31].Value = model.PreWork;
			parameters[32].Value = model.PrePosition;
			parameters[33].Value = model.PreStartTime;
			parameters[34].Value = model.PreEndTime;
			parameters[35].Value = model.PreEpartment;
			parameters[36].Value = model.TurnoverTime;
			parameters[37].Value = model.Sate;
			parameters[38].Value = model.Remarks;
			parameters[39].Value = model.CreateAccountId;
			parameters[40].Value = model.RefreshAccountId;
			parameters[41].Value = model.CreateTime;
			parameters[42].Value = model.RefreshTime;
			parameters[43].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.OfficeWorker model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update OfficeWorker set ");
			strSql.Append("FullName=@FullName,");
			strSql.Append("UsedName=@UsedName,");
			strSql.Append("Sex=@Sex,");
			strSql.Append("Email=@Email,");
			strSql.Append("Tel=@Tel,");
			strSql.Append("Phone=@Phone,");
			strSql.Append("IntNumber=@IntNumber,");
			strSql.Append("InstitutionId=@InstitutionId,");
			strSql.Append("DepartmentId=@DepartmentId,");
			strSql.Append("Position=@Position,");
			strSql.Append("Province=@Province,");
			strSql.Append("City=@City,");
			strSql.Append("County=@County,");
			strSql.Append("Street=@Street,");
			strSql.Append("ZipCode=@ZipCode,");
			strSql.Append("BirthDate=@BirthDate,");
			strSql.Append("ChinaID=@ChinaID,");
			strSql.Append("Nationality=@Nationality,");
			strSql.Append("NativePlace=@NativePlace,");
			strSql.Append("Phone1=@Phone1,");
			strSql.Append("Phone2=@Phone2,");
			strSql.Append("PoliticalStatus=@PoliticalStatus,");
			strSql.Append("EntryTime=@EntryTime,");
			strSql.Append("EntranceMode=@EntranceMode,");
			strSql.Append("PostGrades=@PostGrades,");
			strSql.Append("WageLevel=@WageLevel,");
			strSql.Append("InsuranceWelfare=@InsuranceWelfare,");
			strSql.Append("GraduateSchool=@GraduateSchool,");
			strSql.Append("FormalSchooling=@FormalSchooling,");
			strSql.Append("Major=@Major,");
			strSql.Append("EnglishLevel=@EnglishLevel,");
			strSql.Append("PreWork=@PreWork,");
			strSql.Append("PrePosition=@PrePosition,");
			strSql.Append("PreStartTime=@PreStartTime,");
			strSql.Append("PreEndTime=@PreEndTime,");
			strSql.Append("PreEpartment=@PreEpartment,");
			strSql.Append("TurnoverTime=@TurnoverTime,");
			strSql.Append("Sate=@Sate,");
			strSql.Append("Remarks=@Remarks,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@FullName", SqlDbType.VarChar,4000),
					new SqlParameter("@UsedName", SqlDbType.VarChar,4000),
					new SqlParameter("@Sex", SqlDbType.Int,4),
					new SqlParameter("@Email", SqlDbType.VarChar,4000),
					new SqlParameter("@Tel", SqlDbType.VarChar,4000),
					new SqlParameter("@Phone", SqlDbType.VarChar,4000),
					new SqlParameter("@IntNumber", SqlDbType.VarChar,4000),
					new SqlParameter("@InstitutionId", SqlDbType.Int,4),
					new SqlParameter("@DepartmentId", SqlDbType.Int,4),
					new SqlParameter("@Position", SqlDbType.Int,4),
					new SqlParameter("@Province", SqlDbType.VarChar,4000),
					new SqlParameter("@City", SqlDbType.VarChar,4000),
					new SqlParameter("@County", SqlDbType.VarChar,4000),
					new SqlParameter("@Street", SqlDbType.VarChar,4000),
					new SqlParameter("@ZipCode", SqlDbType.VarChar,4000),
					new SqlParameter("@BirthDate", SqlDbType.VarChar,4000),
					new SqlParameter("@ChinaID", SqlDbType.VarChar,4000),
					new SqlParameter("@Nationality", SqlDbType.VarChar,4000),
					new SqlParameter("@NativePlace", SqlDbType.VarChar,4000),
					new SqlParameter("@Phone1", SqlDbType.VarChar,4000),
					new SqlParameter("@Phone2", SqlDbType.VarChar,4000),
					new SqlParameter("@PoliticalStatus", SqlDbType.VarChar,4000),
					new SqlParameter("@EntryTime", SqlDbType.VarChar,4000),
					new SqlParameter("@EntranceMode", SqlDbType.VarChar,4000),
					new SqlParameter("@PostGrades", SqlDbType.Int,4),
					new SqlParameter("@WageLevel", SqlDbType.Int,4),
					new SqlParameter("@InsuranceWelfare", SqlDbType.VarChar,4000),
					new SqlParameter("@GraduateSchool", SqlDbType.VarChar,4000),
					new SqlParameter("@FormalSchooling", SqlDbType.VarChar,4000),
					new SqlParameter("@Major", SqlDbType.VarChar,4000),
					new SqlParameter("@EnglishLevel", SqlDbType.VarChar,4000),
					new SqlParameter("@PreWork", SqlDbType.VarChar,4000),
					new SqlParameter("@PrePosition", SqlDbType.VarChar,4000),
					new SqlParameter("@PreStartTime", SqlDbType.VarChar,4000),
					new SqlParameter("@PreEndTime", SqlDbType.VarChar,4000),
					new SqlParameter("@PreEpartment", SqlDbType.VarChar,4000),
					new SqlParameter("@TurnoverTime", SqlDbType.VarChar,4000),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.FullName;
			parameters[1].Value = model.UsedName;
			parameters[2].Value = model.Sex;
			parameters[3].Value = model.Email;
			parameters[4].Value = model.Tel;
			parameters[5].Value = model.Phone;
			parameters[6].Value = model.IntNumber;
			parameters[7].Value = model.InstitutionId;
			parameters[8].Value = model.DepartmentId;
			parameters[9].Value = model.Position;
			parameters[10].Value = model.Province;
			parameters[11].Value = model.City;
			parameters[12].Value = model.County;
			parameters[13].Value = model.Street;
			parameters[14].Value = model.ZipCode;
			parameters[15].Value = model.BirthDate;
			parameters[16].Value = model.ChinaID;
			parameters[17].Value = model.Nationality;
			parameters[18].Value = model.NativePlace;
			parameters[19].Value = model.Phone1;
			parameters[20].Value = model.Phone2;
			parameters[21].Value = model.PoliticalStatus;
			parameters[22].Value = model.EntryTime;
			parameters[23].Value = model.EntranceMode;
			parameters[24].Value = model.PostGrades;
			parameters[25].Value = model.WageLevel;
			parameters[26].Value = model.InsuranceWelfare;
			parameters[27].Value = model.GraduateSchool;
			parameters[28].Value = model.FormalSchooling;
			parameters[29].Value = model.Major;
			parameters[30].Value = model.EnglishLevel;
			parameters[31].Value = model.PreWork;
			parameters[32].Value = model.PrePosition;
			parameters[33].Value = model.PreStartTime;
			parameters[34].Value = model.PreEndTime;
			parameters[35].Value = model.PreEpartment;
			parameters[36].Value = model.TurnoverTime;
			parameters[37].Value = model.Sate;
			parameters[38].Value = model.Remarks;
			parameters[39].Value = model.CreateAccountId;
			parameters[40].Value = model.RefreshAccountId;
			parameters[41].Value = model.CreateTime;
			parameters[42].Value = model.RefreshTime;
			parameters[43].Value = model.TenantId;
			parameters[44].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from OfficeWorker ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from OfficeWorker ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.OfficeWorker GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,FullName,UsedName,Sex,Email,Tel,Phone,IntNumber,InstitutionId,DepartmentId,Position,Province,City,County,Street,ZipCode,BirthDate,ChinaID,Nationality,NativePlace,Phone1,Phone2,PoliticalStatus,EntryTime,EntranceMode,PostGrades,WageLevel,InsuranceWelfare,GraduateSchool,FormalSchooling,Major,EnglishLevel,PreWork,PrePosition,PreStartTime,PreEndTime,PreEpartment,TurnoverTime,Sate,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId from OfficeWorker ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.OfficeWorker model=new XinYiOffice.Model.OfficeWorker();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["FullName"]!=null && ds.Tables[0].Rows[0]["FullName"].ToString()!="")
				{
					model.FullName=ds.Tables[0].Rows[0]["FullName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["UsedName"]!=null && ds.Tables[0].Rows[0]["UsedName"].ToString()!="")
				{
					model.UsedName=ds.Tables[0].Rows[0]["UsedName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Sex"]!=null && ds.Tables[0].Rows[0]["Sex"].ToString()!="")
				{
					model.Sex=int.Parse(ds.Tables[0].Rows[0]["Sex"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Email"]!=null && ds.Tables[0].Rows[0]["Email"].ToString()!="")
				{
					model.Email=ds.Tables[0].Rows[0]["Email"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Tel"]!=null && ds.Tables[0].Rows[0]["Tel"].ToString()!="")
				{
					model.Tel=ds.Tables[0].Rows[0]["Tel"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Phone"]!=null && ds.Tables[0].Rows[0]["Phone"].ToString()!="")
				{
					model.Phone=ds.Tables[0].Rows[0]["Phone"].ToString();
				}
				if(ds.Tables[0].Rows[0]["IntNumber"]!=null && ds.Tables[0].Rows[0]["IntNumber"].ToString()!="")
				{
					model.IntNumber=ds.Tables[0].Rows[0]["IntNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["InstitutionId"]!=null && ds.Tables[0].Rows[0]["InstitutionId"].ToString()!="")
				{
					model.InstitutionId=int.Parse(ds.Tables[0].Rows[0]["InstitutionId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DepartmentId"]!=null && ds.Tables[0].Rows[0]["DepartmentId"].ToString()!="")
				{
					model.DepartmentId=int.Parse(ds.Tables[0].Rows[0]["DepartmentId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Position"]!=null && ds.Tables[0].Rows[0]["Position"].ToString()!="")
				{
					model.Position=int.Parse(ds.Tables[0].Rows[0]["Position"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Province"]!=null && ds.Tables[0].Rows[0]["Province"].ToString()!="")
				{
					model.Province=ds.Tables[0].Rows[0]["Province"].ToString();
				}
				if(ds.Tables[0].Rows[0]["City"]!=null && ds.Tables[0].Rows[0]["City"].ToString()!="")
				{
					model.City=ds.Tables[0].Rows[0]["City"].ToString();
				}
				if(ds.Tables[0].Rows[0]["County"]!=null && ds.Tables[0].Rows[0]["County"].ToString()!="")
				{
					model.County=ds.Tables[0].Rows[0]["County"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Street"]!=null && ds.Tables[0].Rows[0]["Street"].ToString()!="")
				{
					model.Street=ds.Tables[0].Rows[0]["Street"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ZipCode"]!=null && ds.Tables[0].Rows[0]["ZipCode"].ToString()!="")
				{
					model.ZipCode=ds.Tables[0].Rows[0]["ZipCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BirthDate"]!=null && ds.Tables[0].Rows[0]["BirthDate"].ToString()!="")
				{
					model.BirthDate=ds.Tables[0].Rows[0]["BirthDate"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ChinaID"]!=null && ds.Tables[0].Rows[0]["ChinaID"].ToString()!="")
				{
					model.ChinaID=ds.Tables[0].Rows[0]["ChinaID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Nationality"]!=null && ds.Tables[0].Rows[0]["Nationality"].ToString()!="")
				{
					model.Nationality=ds.Tables[0].Rows[0]["Nationality"].ToString();
				}
				if(ds.Tables[0].Rows[0]["NativePlace"]!=null && ds.Tables[0].Rows[0]["NativePlace"].ToString()!="")
				{
					model.NativePlace=ds.Tables[0].Rows[0]["NativePlace"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Phone1"]!=null && ds.Tables[0].Rows[0]["Phone1"].ToString()!="")
				{
					model.Phone1=ds.Tables[0].Rows[0]["Phone1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Phone2"]!=null && ds.Tables[0].Rows[0]["Phone2"].ToString()!="")
				{
					model.Phone2=ds.Tables[0].Rows[0]["Phone2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PoliticalStatus"]!=null && ds.Tables[0].Rows[0]["PoliticalStatus"].ToString()!="")
				{
					model.PoliticalStatus=ds.Tables[0].Rows[0]["PoliticalStatus"].ToString();
				}
				if(ds.Tables[0].Rows[0]["EntryTime"]!=null && ds.Tables[0].Rows[0]["EntryTime"].ToString()!="")
				{
					model.EntryTime=ds.Tables[0].Rows[0]["EntryTime"].ToString();
				}
				if(ds.Tables[0].Rows[0]["EntranceMode"]!=null && ds.Tables[0].Rows[0]["EntranceMode"].ToString()!="")
				{
					model.EntranceMode=ds.Tables[0].Rows[0]["EntranceMode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PostGrades"]!=null && ds.Tables[0].Rows[0]["PostGrades"].ToString()!="")
				{
					model.PostGrades=int.Parse(ds.Tables[0].Rows[0]["PostGrades"].ToString());
				}
				if(ds.Tables[0].Rows[0]["WageLevel"]!=null && ds.Tables[0].Rows[0]["WageLevel"].ToString()!="")
				{
					model.WageLevel=int.Parse(ds.Tables[0].Rows[0]["WageLevel"].ToString());
				}
				if(ds.Tables[0].Rows[0]["InsuranceWelfare"]!=null && ds.Tables[0].Rows[0]["InsuranceWelfare"].ToString()!="")
				{
					model.InsuranceWelfare=ds.Tables[0].Rows[0]["InsuranceWelfare"].ToString();
				}
				if(ds.Tables[0].Rows[0]["GraduateSchool"]!=null && ds.Tables[0].Rows[0]["GraduateSchool"].ToString()!="")
				{
					model.GraduateSchool=ds.Tables[0].Rows[0]["GraduateSchool"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FormalSchooling"]!=null && ds.Tables[0].Rows[0]["FormalSchooling"].ToString()!="")
				{
					model.FormalSchooling=ds.Tables[0].Rows[0]["FormalSchooling"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Major"]!=null && ds.Tables[0].Rows[0]["Major"].ToString()!="")
				{
					model.Major=ds.Tables[0].Rows[0]["Major"].ToString();
				}
				if(ds.Tables[0].Rows[0]["EnglishLevel"]!=null && ds.Tables[0].Rows[0]["EnglishLevel"].ToString()!="")
				{
					model.EnglishLevel=ds.Tables[0].Rows[0]["EnglishLevel"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PreWork"]!=null && ds.Tables[0].Rows[0]["PreWork"].ToString()!="")
				{
					model.PreWork=ds.Tables[0].Rows[0]["PreWork"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PrePosition"]!=null && ds.Tables[0].Rows[0]["PrePosition"].ToString()!="")
				{
					model.PrePosition=ds.Tables[0].Rows[0]["PrePosition"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PreStartTime"]!=null && ds.Tables[0].Rows[0]["PreStartTime"].ToString()!="")
				{
					model.PreStartTime=ds.Tables[0].Rows[0]["PreStartTime"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PreEndTime"]!=null && ds.Tables[0].Rows[0]["PreEndTime"].ToString()!="")
				{
					model.PreEndTime=ds.Tables[0].Rows[0]["PreEndTime"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PreEpartment"]!=null && ds.Tables[0].Rows[0]["PreEpartment"].ToString()!="")
				{
					model.PreEpartment=ds.Tables[0].Rows[0]["PreEpartment"].ToString();
				}
				if(ds.Tables[0].Rows[0]["TurnoverTime"]!=null && ds.Tables[0].Rows[0]["TurnoverTime"].ToString()!="")
				{
					model.TurnoverTime=ds.Tables[0].Rows[0]["TurnoverTime"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Sate"]!=null && ds.Tables[0].Rows[0]["Sate"].ToString()!="")
				{
					model.Sate=int.Parse(ds.Tables[0].Rows[0]["Sate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Remarks"]!=null && ds.Tables[0].Rows[0]["Remarks"].ToString()!="")
				{
					model.Remarks=ds.Tables[0].Rows[0]["Remarks"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,FullName,UsedName,Sex,Email,Tel,Phone,IntNumber,InstitutionId,DepartmentId,Position,Province,City,County,Street,ZipCode,BirthDate,ChinaID,Nationality,NativePlace,Phone1,Phone2,PoliticalStatus,EntryTime,EntranceMode,PostGrades,WageLevel,InsuranceWelfare,GraduateSchool,FormalSchooling,Major,EnglishLevel,PreWork,PrePosition,PreStartTime,PreEndTime,PreEpartment,TurnoverTime,Sate,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM OfficeWorker ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,FullName,UsedName,Sex,Email,Tel,Phone,IntNumber,InstitutionId,DepartmentId,Position,Province,City,County,Street,ZipCode,BirthDate,ChinaID,Nationality,NativePlace,Phone1,Phone2,PoliticalStatus,EntryTime,EntranceMode,PostGrades,WageLevel,InsuranceWelfare,GraduateSchool,FormalSchooling,Major,EnglishLevel,PreWork,PrePosition,PreStartTime,PreEndTime,PreEpartment,TurnoverTime,Sate,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM OfficeWorker ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM OfficeWorker ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from OfficeWorker T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "OfficeWorker";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

