﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Basic;
using XinYiOffice.Common;
using System.Collections.Generic;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Client.ClientTracking
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("CLIENTTRACKING_LIST_VIEW"))
            {
                NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    
                    ShowInfo(Id);

                }
            }
        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.SalesOpportunitiesDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        protected void SetDropDownList_ClientInfo(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.ClientInfoDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        private void ShowInfo(int Id)
        {

            TextBox_PlanTime.Text = DateTime.Now.AddDays(3).ToShortDateString();

            try
            {
                DataSet ds = ClientServer.GetClientTracking(string.Format("Id={0}", Id));
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {

                    DataRow dr = ds.Tables[0].Rows[0];
                    
                    SetDropDownList("PresentStage", ref DropDownList_Current_SalesOpportunitiesSate);
                    SetDropDownList("PresentStage", ref DropDownList_SalesOpportunitiesSate);
                    SetDropDownList("Sate", ref DropDownList_OpportunitiesSate);

                    SetDropDownList_ClientInfo("Heat", ref DropDownList_ClientHeat);


                    LabelClientName.Text = SafeConvert.ToString(dr["ClientInfoName"]);

                    #region//联系人
                    Model.ClientLiaisons cll = new Model.ClientLiaisons();
                    List<Model.ClientLiaisons> cllList = new BLL.ClientLiaisons().GetModelList(string.Format("ClientInfoId={0}", SafeConvert.ToInt(dr["ClientInfoId"])));
                    if (cllList != null && cllList.Count > 0)
                    {
                        cll = cllList[0];
                        Label_Client_phone.Text = cll.Phone;
                        Label_CLLName.Text = cll.FullName;
                        Label_CLL_address.Text = cll.Address;
                        panCLL.Visible = true;
                    }
                    else
                    {
                        panCLL.Visible = false;
                    }



                    #endregion

                    HiddenField_Id.Value = dr["Id"].ToString();
                    //HiddenField_IsFollowUp.Value = dr["IsFollowUp"].ToString();
                    Label_Heat.Text = dr["Heat"].ToString();
                    Label_SalesOpportunitiesName.Text = dr["OppName"].ToString();
                    HiddenField_SalesOpportunitiesId.Value = dr["SalesOpportunitiesId"].ToString();
                    TextBox_BackNotes.Text = dr["BackNotes"].ToString();
                    Label_CreateTime.Text = SafeConvert.ToDateTime(dr["CreateTime"]).ToString("yyyy-MM-dd");

                    DropDownList_Current_SalesOpportunitiesSate.SelectedValue = dr["SalesOpportunitiesSate"].ToString();
                    DropDownList_SalesOpportunitiesSate.SelectedValue = dr["SalesOpportunitiesSate"].ToString();
                    DropDownList_Current_SalesOpportunitiesSate.Enabled = false;

                    DataSet dsHis = ClientServer.GetClientTracking(string.Format("SalesOpportunitiesId={0} and Id!={1}", dr["SalesOpportunitiesId"].ToString(), dr["Id"].ToString()));

                    if (dsHis != null && dsHis.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = dsHis.Tables[0];
                        dt.DefaultView.Sort = "Id asc";

                        repHis.DataSource = dt;
                        repHis.DataBind();
                    }
                    else
                    {
                        labHisMsg.Text = "暂没有历史记录!";
                        repHis.Visible = false;
                    }

                }
                else
                {
                    //表单异常
                }
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
            }


        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int IsFollowUp = SafeConvert.ToInt(RadioButtonList_IsFollowUp.SelectedValue);//-1-暂不处理,1-是,0-否
            int currentClientTrackingId = SafeConvert.ToInt(HiddenField_Id.Value);//当前客户跟进ID
            Model.ClientTracking ctCurrent = new BLL.ClientTracking().GetModel(currentClientTrackingId);
            ctCurrent.Sate=2;
            ctCurrent.BackNotes = TextBox_BackNotes.Text;
            ctCurrent.RefreshAccountId = CurrentAccountId;
            ctCurrent.RefreshTime = DateTime.Now;
            ctCurrent.TenantId = CurrentTenantId;

            Model.SalesOpportunities so = new BLL.SalesOpportunities().GetModel(SafeConvert.ToInt(HiddenField_SalesOpportunitiesId.Value,0));
            if (so != null)
            {
                so.RefreshAccountId = CurrentAccountId;
                so.RefreshTime = DateTime.Now;
                
            }

            if(IsFollowUp==-1)
            {
                //暂不处理

                ctCurrent.IsFollowUp = -1;
            }
            else if (IsFollowUp==1)
            {
                //继续跟进
                ctCurrent.IsFollowUp = 1;

                #region 添加跟进信息
                Model.ClientTracking ct = new Model.ClientTracking();
                ct.CreateAccountId = CurrentAccountId;
                ct.CreateTime = DateTime.Now;
                ct.IsFollowUp = 1;
                ct.PlanTime = SafeConvert.ToDateTime(TextBox_PlanTime.Text);
                ct.ReturnAccountId = CurrentAccountId;
                ct.SalesOpportunitiesId = SafeConvert.ToInt(HiddenField_SalesOpportunitiesId.Value);
                ct.SalesOpportunitiesSate = SafeConvert.ToInt(DropDownList_SalesOpportunitiesSate.SelectedValue);
                ct.Sate = 1;
                ct.ClientHeat = SafeConvert.ToInt(DropDownList_ClientHeat.SelectedValue);
                new BLL.ClientTracking().Add(ct);

                //销售机会 阶段
                so.PresentStage = SafeConvert.ToInt(DropDownList_SalesOpportunitiesSate.SelectedValue);
                #endregion
            }
            else if (IsFollowUp==0)
            {
                //不跟进
                ctCurrent.IsFollowUp =0;
                so.Sate = SafeConvert.ToInt(DropDownList_OpportunitiesSate.SelectedValue);
            }

            try
            {
                new BLL.ClientTracking().Update(ctCurrent);
                new BLL.SalesOpportunities().Update(so);
                xytools.web_alert("操作已成功!","list.aspx");
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
                xytools.web_alert("服务器异常!","list.aspx");
                
            }

        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            //取消回访
            int currentClientTrackingId = SafeConvert.ToInt(HiddenField_Id.Value);//当前客户跟进ID
            Model.ClientTracking ctCurrent = new BLL.ClientTracking().GetModel(currentClientTrackingId);
            ctCurrent.Sate = 3;

            try
            {
                new BLL.ClientTracking().Update(ctCurrent);
                xytools.web_alert("操作已成功!", "list.aspx");
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
                xytools.web_alert("服务器异常!", "list.aspx");
                
            }

        }

        public string EscString(string con,int i)
        {
            return xytools.CutString(con,i) ; 
        }

    }
}
