﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Internal.InternalLetter
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        protected void BindData()
        {
            HiddenField_InternalLetterId.Value = SafeConvert.ToString(xytools.url_get("intid"));

            DataTable dtAccountList = AccountsServer.GetAccountAllList(CurrentTenantId);
            foreach (DataRow dr in dtAccountList.Rows)
            {
                DropDownList_Account.Items.Add(new ListItem(string.Format("{0}({1})", SafeConvert.ToString(dr["FullName"]), SafeConvert.ToString(dr["AccountName"])), SafeConvert.ToString(dr["Id"])));
            }

        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {
                int InternalLetterId = SafeConvert.ToInt(HiddenField_InternalLetterId.Value, 0);
                string Title = this.txtTitle.Text;
                string Con = this.txtCon.Text;
 
                int AuthorAccountId = CurrentAccountId;

                int ImpDegree = SafeConvert.ToInt(RadioButtonList1.SelectedValue);
                int IsDelete = 0;
                string Remarks = string.Empty;
                int CreateAccountId = CurrentAccountId;
                int RefreshAccountId = CreateAccountId;
                DateTime CreateTime = DateTime.Now;
                DateTime RefreshTime = CreateTime;

                XinYiOffice.Model.InternalLetter model = new XinYiOffice.Model.InternalLetter();
                model.InternalLetterId = InternalLetterId;
                model.Title = Title;
                model.Con = Con;
                model.AuthorAccountId = AuthorAccountId;
                model.ImpDegree = ImpDegree;
                model.IsDelete = IsDelete;
                model.Remarks = Remarks;
                model.CreateAccountId = CreateAccountId;
                model.RefreshAccountId = RefreshAccountId;
                model.CreateTime = CreateTime;
                model.RefreshTime = RefreshTime;
                model.TenantId = CurrentTenantId;

                XinYiOffice.BLL.InternalLetter bll = new XinYiOffice.BLL.InternalLetter();
                model.Id = bll.Add(model);

                #region 上传附件
                ResAttachmentServer.UpLoadRes(Request.Files, "InternalLetter", model.Id.ToString(), CurrentAccount);
                #endregion

                #region //添加接收表
                string AccountList = HiddenField_InternalReceiver.Value;

                if (!string.IsNullOrEmpty(AccountList))
                {
                    string[] aid = AccountList.Split(',');
                    foreach (string _id in aid)
                    {
                        Model.InternalReceiver ir = new Model.InternalReceiver();
                        ir.CheckDate = null;
                        ir.CreateAccountId = CurrentAccountId;
                        ir.CreateTime = DateTime.Now;
                        ir.InternalLetterId = model.Id;
                        ir.IsDelete = 0;
                        ir.IsReply = 0;
                        ir.IsView = 0;
                        ir.LetterPosition = 1;
                        ir.PositionFoler = string.Empty;
                        ir.RecipientAccountId = SafeConvert.ToInt(_id);
                        ir.RecipientType = 1;
                        ir.RefreshAccountId = CurrentAccountId;
                        ir.RefreshTime = DateTime.Now;
                        ir.TenantId = CurrentTenantId;

                        try { new BLL.InternalReceiver().Add(ir); }
                        catch (Exception ex) { EasyLog.WriteLog(ex); }

                    }
                } 
                #endregion

                xytools.web_alert("保存成功！", "list.aspx");
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
            }
            
        }
    }
}
