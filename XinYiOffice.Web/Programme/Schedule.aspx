﻿<%@ Page  Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Schedule.aspx.cs" Inherits="XinYiOffice.Web.Programme.Schedule" %>

<asp:Content ContentPlaceHolderID="head"  runat="server">
<link rel="stylesheet" href="/css/validationEngine.jquery.css" type="text/css"/>
<script src="/js/jquery.validationEngine-cn.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine.js" type="text/javascript" ></script>


<link rel="stylesheet" href="/css/calendar.css"  type="text/css"/>
<script type="text/javascript" src="/js/calendar.js"></script>
<script>
    $(function () {

        $("#rad_IsRemind").click(function () {

            var t = $("#rad_IsRemind").attr("checked");

            if (t == "checked") { $("#div_Remind").show(); }
            else { $("#div_Remind").hide(); }


        });

        $("#rad_IsRepeat").click(function () {

            var t = $("#rad_IsRepeat").attr("checked");

            if (t == "checked") { $("#div_RepeatInterval").show(); }
            else { $("#div_RepeatInterval").hide(); }

        });

        $('.saveMembers').click(function () {

            $('#tcc1').hide(); $('#tcc2').show();
        });

        //        $("#f3").validationEngine({
        //            promptPosition: "topRight", // OPENNING BOX POSITION, IMPLEMENTED: topLeft, topRight, bottomLeft,  centerRight, bottomRight 
        //            success: false,
        //            failure: function () {   } 
        //        });
        $("#f3").validationEngine('attach', { promptPosition: "topLeft" });


        var sel_ScheduleType = $("#sel_ScheduleType");
        $.getJSON("/Call/Ajax.aspx?action=getdictionarytable&table=Programme&field=ScheduleType&sj=" + Math.random(), function (json) {

            $.each(json, function (i) {
                sel_ScheduleType.append("<option value='" + json[i].value + "'>" + json[i].label + "</option>");
            });

        });

        var sel_ins = $("#sel_Institution");
        var sel_dep = $("#sel_Department");
        var left_list_p = $("#left_list_p");
        var right_list_p = $("#right_list_p");
        var hidMembersAccountId = $("#hidMembersAccountId");

        $.getJSON("/Call/Ajax.aspx?action=getinstitution&sj=" + Math.random(), function (json) {

            $.each(json, function (i) {
                sel_ins.append("<option value='" + json[i].value + "'>" + json[i].label + "</option>");
            });

        });

        //机构选择事件
        sel_ins.change(function () {
            sel_dep.html("");
            sel_dep.append("<option value='0'>请选择部门</option>");
            left_list_p.html('');

            var insid = sel_ins.val();
            $.getJSON("/Call/Ajax.aspx?action=getdepartmentbyins&insid=" + insid + "&sj=" + Math.random(), function (json) {

                $.each(json, function (i) {
                    sel_dep.append("<option value='" + json[i].value + "'>" + json[i].label + "</option>");
                });
            });

            $.getJSON("/Call/Ajax.aspx?action=getofficeworkerbyins&insid=" + insid + "&sj=" + Math.random(), function (json2) {

                $.each(json2, function (i) {
                    //sel_dep.append("<option value='" + json[i].value + "'>" + json[i].label + "</option>");
                    left_list_p.append("<li value1='" + json2[i].value + "'>" + json2[i].label + "</li>");
                });

            });
        });

        //部门选择事件
        sel_dep.change(function () {
        
            $.getJSON("/Call/Ajax.aspx?action=getofficeworkerbydep&depid=" + sel_dep.val() + "&sj=" + Math.random(), function (json2) {
                left_list_p.html('');
                $.each(json2, function (i) {
                    left_list_p.append("<li value1='" + json2[i].value + "'>" + json2[i].label + "</li>");
                });

            });
            
        });


        var frm = $("#f3");
        $(".save").click(function () {
            var _hid = '';
            right_list_p.find("li").each(function (i) {

                _hid += $(this).attr("value1") + ",";
            });
            hidMembersAccountId.val(_hid);

            // 提交表单
            frm.ajaxSubmit({ beforeSubmit: showRequest, success: function (dat) {
                $('#tcc1').hide(); $('#tcc2').hide(); $('#mb').hide();

                $.layer({
                    area: ['300', '180'],
                    dialog: { msg: '已添加成功!', type: 1 },
                    end: function () { 
                    //window.location.href = window.location.href; 
                        //location.reload();
                    }
                });

            }
            });

            // 为了防止普通浏览器进行表单提交和产生页面导航（防止页面刷新？）返回false
            return false;
        });


    });

    //提交前验证
function showRequest() {
 
 if ($("#f3").validationEngine('validate')) {
        return true;
    }
    else {
        return false;
    }


}

function setTimeProgramme(dat)
{
	$("#txt_StartTime").val(dat);
	$("#txt_EndTime").val(dat);
	$("#txt_Note").val('');
	$("#txt_Title").val('');
	//alert(dat);
	return false;
}


	
</script>
<style>

#div_Remind{ display:none; float:left;}
#div_RepeatInterval{ display:none; float:left;}
</style>
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <form id="f2" runat="server">


<h4 class="rclb" style="display:none;">日程列表</h4>

<div id="calendar">
<asp:Calendar ID="Calendar1" SelectedDate="<%#GetDate()%>" 
        OnDayRender="calSchedule_DayRender"  runat="server" 
        onvisiblemonthchanged="Calendar1_VisibleMonthChanged"></asp:Calendar>
 	
</div>
</form>
</asp:Content>

<asp:Content ContentPlaceHolderID="cp2" runat="server">
    <div id="mb" style="display:none;"></div>

<form action="/Programme/Ajax/Item.aspx?action=addProgramme" id="f3" name="f3">
<div id="tcc1" style="display:none;">
	<h4 class="title_h4">添加日程安排<a href="javascript:;"></a></h4>
    <div class="form_box">
    	<div class="clear">
            <div class="clear inp_div"><p>主题</p><input class="validate[required]" name="txt_Title" type="text" id="txt_Title" style=" width:488px;" /></div>
            <div  style=" float:left;" class="clear inp_div"><p>地点</p>
            <select name="sel_Locale" style=" width:207px;" id="sel_Locale">
            <option value="公司办公室">公司办公室</option>
            <option value="会议室">会议室</option>
            <option value="客户办公地">客户办公地</option>
            <option value="0">其他</option>
            </select></div>
            <div  style=" float:left;" class="clear inp_div"><p>类型</p>
            <select name="sel_ScheduleType" id="sel_ScheduleType" style=" width:207px; height:20px;" >
            </select>
            </div>
        </div>
    </div>
    
    <div class="pl72">
    	<div class="clear" style=" height:61px;">
        	<div class="in_left">
            	<div class="clear inp_div1"><p>开始时间</p><input name="txt_StartTime"  type="text" id="txt_StartTime"  class="Wdate"  onClick="WdatePicker()"/><select name="sel_StartTime2" id="sel_StartTime2">	<option selected="selected" value="00:00">00:00</option>
	<option value="00:30">00:30</option>
	<option value="01:00">01:00</option>
	<option value="01:30">01:30</option>
	<option value="02:00">02:00</option>
	<option value="02:30">02:30</option>
	<option value="03:00">03:00</option>
	<option value="03:30">03:30</option>
	<option value="04:00">04:00</option>
	<option value="04:30">04:30</option>
	<option value="5:00">5:00</option>
	<option value="05:30">05:30</option>
	<option value="06:00">06:00</option>
	<option value="06:30">06:30</option>
	<option value="07:00">07:00</option>
	<option value="07:30">07:30</option>
	<option value="08:00">08:00</option>
	<option value="08:30">08:30</option>
	<option value="09:00">09:00</option>
	<option value="09:30">09:30</option>
	<option value="10:00">10:00</option>
	<option value="10:30">10:30</option>
	<option value="11:00">11:00</option>
	<option value="11:30">11:30</option>
	<option value="12:00">12:00</option>
	<option value="12:30">12:30</option>
	<option value="13:00">13:00</option>
	<option value="13:30">13:30</option>
	<option value="14:00">14:00</option>
	<option value="14:30">14:30</option>
	<option value="15:00">15:00</option>
	<option value="15:30">15:30</option>
	<option value="16:00">16:00</option>
	<option value="16:30">16:30</option>
	<option value="17:00">17:00</option>
	<option value="17:30">17:30</option>
	<option value="18:00">18:00</option>
	<option value="18:30">18:30</option>
	<option value="19:00">19:00</option>
	<option value="19:30">19:30</option>
	<option value="20:00">20:00</option>
	<option value="20:30">20:30</option>
	<option value="21:00">21:00</option>
	<option value="21:30">21:30</option>
	<option value="22:00">22:00</option>
	<option value="22:30">22:30</option>
<option value="23:00">23:00</option></select></div>
                <div class="clear inp_div1"><p>结束时间</p><input name="txt_EndTime"  type="text" id="txt_EndTime"  class="Wdate"  onClick="WdatePicker()"/><select name="sel_EndTime2" id="sel_EndTime2">	<option value="00:00">00:00</option>
	<option value="00:30">00:30</option>
	<option value="01:00">01:00</option>
	<option value="01:30">01:30</option>
	<option value="02:00">02:00</option>
	<option value="02:30">02:30</option>
	<option value="03:00">03:00</option>
	<option value="03:30">03:30</option>
	<option value="04:00">04:00</option>
	<option value="04:30">04:30</option>
	<option value="5:00">5:00</option>
	<option value="05:30">05:30</option>
	<option value="06:00">06:00</option>
	<option value="06:30">06:30</option>
	<option value="07:00">07:00</option>
	<option value="07:30">07:30</option>
	<option value="08:00">08:00</option>
	<option value="08:30">08:30</option>
	<option value="09:00">09:00</option>
	<option value="09:30">09:30</option>
	<option value="10:00">10:00</option>
	<option value="10:30">10:30</option>
	<option value="11:00">11:00</option>
	<option value="11:30">11:30</option>
	<option value="12:00">12:00</option>
	<option value="12:30">12:30</option>
	<option value="13:00">13:00</option>
	<option value="13:30">13:30</option>
	<option value="14:00">14:00</option>
	<option value="14:30">14:30</option>
	<option value="15:00">15:00</option>
	<option value="15:30">15:30</option>
	<option value="16:00">16:00</option>
	<option value="16:30">16:30</option>
	<option value="17:00">17:00</option>
	<option value="17:30">17:30</option>
	<option value="18:00">18:00</option>
	<option value="18:30">18:30</option>
	<option value="19:00">19:00</option>
	<option value="19:30">19:30</option>
	<option value="20:00">20:00</option>
	<option value="20:30">20:30</option>
	<option value="21:00">21:00</option>
	<option value="21:30">21:30</option>
	<option value="22:00">22:00</option>
	<option value="22:30">22:30</option>
	<option value="23:00" selected="selected">23:00</option>
    </select></div>
            </div>
            <div class="in_right">
            <div style="height:30px; line-height:30px; vertical-align:middle;">
            	<div style="float:left; display:inline;"><label><input name="rad_IsRemind" type="checkbox" id="rad_IsRemind" value="1"/>提醒</label></div>
                
                <div id="div_Remind">提前<input name="txt_RemindDay" type="text" id="txt_RemindDay" style=" width:20px; height:19px; border:1px solid #ccc;" value="1" />天提醒我</div>
               </div>
               
                
            </div>
        </div>
        <textarea name="txt_Note" id="txt_Note"></textarea>
        <div class="xx_b clear inp_div">
        	<a href="javascript:;" class="saveMembers"></a>
            <input type="hidden" id="hidMembersAccountId" name="hidMembersAccountId" value="0" />
            <div style="height:30px; line-height:30px; vertical-align:middle; float:right; margin-right:10px;">
                <div style="float:left;"><label><input name="rad_IsRepeat" type="checkbox" id="rad_IsRepeat" /><span>重复</span></label></div>
                <div id="div_RepeatInterval">
                <select name="sel_RepeatInterval" id="sel_RepeatInterval">
                <option selected="selected" value="0">每日</option>
                <option value="1">每周</option>
                <option value="2">每月</option>
                <option value="3">每年</option>
                <option value="4">隔周</option>
                <option value="5">隔月</option>
                </select><span>至</span>
                <input type="text" name="txt_UpToDate" id="txt_UpToDate"   class="Wdate"  onClick="WdatePicker()"/>
              </div>
          </div>
          
          
        </div>
        <div class="clear btn_box">
        	<a class="save" href="javascript:;">保存</a>
        	<a class="qx" href="javascript:;">取消</a>
        </div>
    </div>
</div>
</form>


<div id="tcc2" style=" display:none;" >
	<h4 class="title_h4">添加日程安排<a href="javascript:;"></a></h4>
	<div class="tj_t">
    	<a href="javascript:;"></a>
    	<p class="p1">选取日程预约人员</p>
    	<p class="p2">你希望选取下列哪些人员？</p>
    </div>
    <div class="xz clear">
    	<span>机构</span><select id="sel_Institution" name="sel_Institution">
        <option value="0">选择机构</option>
        </select><span>部门</span>
        
        <select id="sel_Department" name="sel_Department">
        <option value="0">请选择部门</option>
        </select>
    </div>
    <div class="clear" style=" padding-left:75px;">
    	<div class="left_list">
        	<p class="pren">可选人员：</p>
            <ul id="left_list_p" class="pren_ul">

            </ul>
        </div>
    	<div class="content_btn">
        	<input type="button" value="添加->" />
            <input type="button" value="<-删除" />
        	<input type="button" value="添加->>" />
            <input type="button" value="<<-删除" />
        </div>
    	<div class="right_list">
        	<p class="pren">已选定人员</p>
            <ul id="right_list_p" class="pren_ul"></ul>
        </div>
    </div>
    <div class="btn_a" style=" display:none;">
    	<a class="qd" href="javascript:;">确定</a>
    	<a class="qx" href="javascript:;">取消</a>
    </div>
</div>

</asp:Content>