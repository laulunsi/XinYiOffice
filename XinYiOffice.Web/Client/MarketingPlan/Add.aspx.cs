﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Client.MarketingPlan
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("MARKETINGPLAN_LIST_ADD"))
            {
                NoPermissionPage();
            }

            if(!IsPostBack)
            {
                InitData();
            }
        }

        protected void InitData()
        {
            txtStartTime.Text = DateTime.Now.ToShortDateString();
            txtEndTime.Text = DateTime.Now.AddMonths(1).ToShortDateString();

            SetDropDownList("Sate", ref DropDownList_Sate);
            SetDropDownList("Type", ref DropDownList_TYPE);

            txtSchemerAccountId.Text = CurrentAccountTrueName;
            HiddenField_SchemerAccountId.Value = CurrentAccountId.ToString();

            txtExecuteAccountId.Text = CurrentAccountTrueName;
            HiddenField_ExecuteAccountId.Value = CurrentAccountId.ToString();
        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.MarketingPlanDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string ProgramName = this.txtProgramName.Text;
            string ProgramDescription = this.txtProgramDescription.Text;
            int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue);
            DateTime StartTime = DateTime.Parse(this.txtStartTime.Text);
            DateTime EndTime = DateTime.Parse(this.txtEndTime.Text);
            int TYPE = SafeConvert.ToInt(DropDownList_TYPE.SelectedValue);
            decimal AnticipatedRevenue = decimal.Parse(this.txtAnticipatedRevenue.Text);
            decimal BudgetCost = decimal.Parse(this.txtBudgetCost.Text);
            int SchemerAccountId = SafeConvert.ToInt(HiddenField_SchemerAccountId.Value);
            int ExecuteAccountId = SafeConvert.ToInt(HiddenField_ExecuteAccountId.Value);

            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.MarketingPlan model = new XinYiOffice.Model.MarketingPlan();
            model.ProgramName = ProgramName;
            model.ProgramDescription = ProgramDescription;
            model.Sate = Sate;
            model.StartTime = StartTime;
            model.EndTime = EndTime;
            model.TYPE = TYPE;
            model.AnticipatedRevenue = AnticipatedRevenue;
            model.BudgetCost = BudgetCost;
            model.SchemerAccountId = SchemerAccountId;
            model.ExecuteAccountId = ExecuteAccountId;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.MarketingPlan bll = new XinYiOffice.BLL.MarketingPlan();
            bll.Add(model);
            xytools.web_alert_new_url("保存成功！", "List.aspx");
        }
    }
}
