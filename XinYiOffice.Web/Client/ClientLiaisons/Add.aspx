﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" enableEventValidation="false" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Client.ClientLiaisons.Add" Title="增加页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<style>
select.wd{ width:100px;}
</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
	<script src="/js/ui/jquery.ui.core.js"></script>
	<script src="/js/ui/jquery.ui.widget.js"></script>
	<script src="/js/ui/jquery.ui.position.js"></script>
	<script src="/js/ui/jquery.ui.autocomplete.js"></script>

	<link rel="stylesheet" href="/js/themes/base/jquery.ui.all.css">
    <link rel="stylesheet" href="/js/themes/base/jquery.ui.autocomplete.css">

<script>
    $(function () {

        var autoClientInfoId = $("#<%=txtClientInfoId.ClientID%>");
        var autoHiddenField_ClientInfoId = $("#<%=HiddenField_ClientInfoId.ClientID%>");
        var url_getclient = "/Call/Ajax.aspx?action=getclientinfo&sj=" + Math.random();

        //客户
        autoClientInfoId.autocomplete({
            source: url_getclient,
            select: function (event, ui) {

                autoClientInfoId.val(ui.item.label);
                autoHiddenField_ClientInfoId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                autoClientInfoId.val(ui.item.label);
                return false;
            }
        });

        //城市
        var DropDownList_Province = $("#<%=DropDownList_Province.ClientID%>");
        var DropDownList_City = $("#<%=DropDownList_County.ClientID%>");
        var HiddenField_City = $("#<%=HiddenField_county.ClientID%>");

        DropDownList_Province.click(function () {
            DropDownList_City.html("");
            $.getJSON("/Call/Ajax.aspx?action=getcity&pid=" + DropDownList_Province.val() + "&sj=" + Math.random(), function (json) {

                $.each(json.Data, function (i) {
                    DropDownList_City.append("<option value='" + json.Data[i].Id + "'>" + json.Data[i].Name + "</option>");
                });
            });

        });


        DropDownList_City.change(function () {
            HiddenField_City.val(DropDownList_City.val());
        });

        var autoAccountId = $("#<%=txtByAccountId.ClientID%>");
        var hidautoAccountId = $("#<%=HiddenField_accountId.ClientID%>");
        var url_workaccount = "/Call/Ajax.aspx?action=getofficeworkeraccount&sj=" + Math.random();
        autoAccountId.autocomplete({
            source: url_workaccount,
            select: function (event, ui) {

                autoAccountId.val(ui.item.label);
                hidautoAccountId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                autoAccountId.val(ui.item.label);
                return false;
            }
        });

    });
</script>

<form  id="f2" name="f2" runat="server">
    <div class="setup_box" style=" margin-bottom:0">
     	<div class="h_title"><h4>联系人添加</h4></div>
        
   <table cellSpacing="0" cellPadding="0" width="100%" border="0" class="plan_table">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属客户
	：</td>
	<td height="25" width="27%" align="left">
		<asp:TextBox id="txtClientInfoId" runat="server" Width="200px" CssClass="auto_input"></asp:TextBox>
	    <asp:HiddenField ID="HiddenField_ClientInfoId" runat="server" />
	</td>
	<td width="16%" align="left" class="table_left"><span class="table_left">联系人编号
	：</span></td>
	<td width="27%" align="left"><asp:TextBox id="txtPersonNumber" runat="server" Width="200px"></asp:TextBox></td>
	</tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">联系人姓名
	：</td>
	<td height="25" align="left"><asp:TextBox id="txtFullName" runat="server" Width="200px"></asp:TextBox></td>
	<td height="25" align="left" class="table_left"><span class="table_left">权限
	：</span></td>
	<td height="25" align="left"><asp:DropDownList ID="DropDownList_Power" runat="server" CssClass="wd"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">手机
	：</td>
	<td height="25" align="left"><asp:TextBox id="txtPhone" runat="server" Width="200px"></asp:TextBox></td>
	<td height="25" align="left" class="table_left"><span class="table_left">其他电话
	：</span></td>
	<td height="25" align="left"><asp:TextBox id="txtOtherPhone" runat="server" Width="200px"></asp:TextBox></td>
	</tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">传真
	：</td>
	<td height="25" align="left"><asp:TextBox id="txtFax" runat="server" Width="200px"></asp:TextBox></td>
	<td height="25" align="left" class="table_left"><span class="table_left">电子邮件
	：</span></td>
	<td height="25" align="left"><asp:TextBox id="txtEmail" runat="server" Width="200px"></asp:TextBox></td>
	</tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">QQ
	：</td>
	<td height="25" align="left"><asp:TextBox id="txtQQ" runat="server" Width="200px"></asp:TextBox></td>
	<td height="25" align="left" class="table_left"><span class="table_left">管理的职员：</span></td>
	<td height="25" align="left"><asp:TextBox id="txtByAccountId" runat="server" Width="200px"></asp:TextBox>
	  <asp:HiddenField ID="HiddenField_accountId" runat="server" /></td>
	</tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">生日
	：</td>
	<td height="25" align="left"><asp:TextBox id="txtBirthday" runat="server" Width="200px"></asp:TextBox></td>
	<td height="25" align="left" class="table_left"><span class="table_left">职务
	：</span></td>
	<td height="25" align="left"><asp:TextBox id="txtFUNCTION" runat="server" Width="200px"></asp:TextBox></td>
	</tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">部门
	：</td>
	<td height="25" align="left"><asp:TextBox id="txtDepartment" runat="server" Width="200px"></asp:TextBox></td>
	<td height="25" align="left" class="table_left"><span class="table_left">来源
	：</span></td>
	<td height="25" align="left"><asp:DropDownList ID="DropDownList_Source" runat="server" CssClass="wd"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">直属上级
	：</td>
	<td height="25" align="left"><asp:TextBox id="txtDirectSuperior" runat="server" Width="200px"></asp:TextBox></td>
	<td height="25" align="left" class="table_left"><span class="table_left">联系人分类
	：</span></td>
	<td height="25" align="left"><asp:DropDownList ID="DropDownList_ClassiFication" runat="server" CssClass="wd"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">是否可以联系
	：</td>
	<td height="25" align="left"><asp:DropDownList ID="DropDownList_IfContact" runat="server" CssClass="wd"> </asp:DropDownList></td>
	<td height="25" align="left" class="table_left"><span class="table_left">主要联系人
	：</span></td>
	<td height="25" align="left"><asp:DropDownList ID="DropDownList_PrimaryContact" runat="server" CssClass="wd"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">负责业务
	：</td>
	<td height="25" colspan="3" align="left"><asp:TextBox id="txtBusiness" runat="server" Width="200px"></asp:TextBox></td></tr>
	</table>
    
   	  <div class="h_title"><h4>联系信息</h4></div>
        <table width="100%" border="0" cellPadding="0" cellSpacing="0" class="plan_table">
  <tr>
    <td height="25" width="30%" align="right" class="table_left"> 接受短信手机
      ：</td>
    <td height="25" align="left"><asp:TextBox id="txtShortPhone" runat="server" Width="200px"></asp:TextBox></td>
    <td height="25" align="left" class="table_left"><span class="table_left">省份
      ：</span></td>
    <td height="25" align="left"><asp:DropDownList ID="DropDownList_Province" runat="server" CssClass="wd"> </asp:DropDownList></td>
  </tr>
  <tr>
    <td height="25" width="30%" align="right" class="table_left"> 性别
      ：</td>
    <td height="25" align="left"><asp:DropDownList ID="DropDownList_Sex" runat="server" CssClass="wd"> </asp:DropDownList></td>
    <td height="25" align="left" class="table_left"><span class="table_left">区县
      ：</span></td>
    <td height="25" align="left"><asp:DropDownList ID="DropDownList_County" runat="server" CssClass="wd"> </asp:DropDownList>
      <asp:HiddenField ID="HiddenField_county" runat="server" /></td>
  </tr>
  <tr>
    <td height="25" width="30%" align="right" class="table_left"> 国家地区
      ：</td>
    <td height="25" colspan="3" align="left"><asp:TextBox id="txtCountriesRegions" runat="server" Width="200px"></asp:TextBox></td>
    </tr>
  <tr>
    <td height="25" width="30%" align="right" class="table_left"> 邮编
      ：</td>
    <td height="25" colspan="3" align="left"><asp:TextBox id="txtZip" runat="server" Width="200px"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25" width="30%" align="right" class="table_left"> 地址
      ：</td>
    <td height="25" colspan="3" align="left"><asp:TextBox id="txtAddress" runat="server" Width="200px"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25" width="30%" align="right" class="table_left">备注
      ：</td>
    <td height="25" colspan="3" align="left"><asp:TextBox id="txtRemarks" runat="server" Width="200px"></asp:TextBox></td>
  </tr>
</table>

        
   </div>
   
   
   
         <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
        
    </div>
    
    </form>
</asp:Content>


