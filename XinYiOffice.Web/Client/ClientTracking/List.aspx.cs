﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;
using XinYiOffice.Common.Web;

namespace XinYiOffice.Web.Client.ClientTracking
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.ClientTracking bll = new XinYiOffice.BLL.ClientTracking();
        public int _page = 1;
        public int _pagezs = 0;
        public int _datazs = 0;
        string benye_url;//当前url
        public string pagehtml = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("CLIENTTRACKING_LIST"))
            {
                NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                BindData();
            }            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;
                }
            }
        }
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }
        
        
        #region gridView
                        
        public void BindData()
        {
            DataTable dt = new DataTable();
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            strWhere.AppendFormat(" and TenantId={0}", CurrentTenantId);
          
            string mysql = ClientServer.GetClientTrackingSql(strWhere.ToString(), CurrentTenantId);

            #region 分页
            if (!string.IsNullOrEmpty(xytools.url_get("page")))
            {
                _page = SafeConvert.ToInt(xytools.url_get("page"), 1);
            }
            PageDataSet pd = new PageDataSet();
            dt = pd.GetListPageBySql(mysql, _page, 10, ref _pagezs, ref _datazs);
            PageAttribute pa = new PageAttribute();
            pa.PagCur = _page.ToString();
            pa.PageLinage = "10";
            pa.PageShowFL = "1";
            pa.PageShowGo = "1";

            benye_url = "list.aspx?" + xyurl.GetQueryRemovePage(Request.QueryString);//获取当前url字符串
            pa.PageUrl = benye_url;
            pagehtml = ParseHTML.PageLabel(pa, _pagezs);
            #endregion

            repClientTracking.DataSource = dt;
            repClientTracking.DataBind();

        }

        #endregion

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("add.aspx");
        }





    }
}
