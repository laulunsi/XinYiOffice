﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Sys.SearchConfigItem.Modify" Title="修改页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 2%;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form  id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>修改搜索项</h4></div>
 
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		显示字段名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDisplayName" runat="server" Width="200px"></asp:TextBox>
	    <asp:HiddenField ID="HiddenField_id" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		数据库字段名
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_txtFieldName" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		表单数据来源
	：</td>
	<td height="25" width="*" align="left">
		<asp:HiddenField ID="HiddenField_SearchDataSoureId" runat="server" />
        <table cellSpacing="0" cellPadding="0" width="100%" border="0" class="source_table">
	<tr>
	<td height="25" align="right" class="table_left">
		数据源 ：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_DataSoureType" runat="server">
            <asp:ListItem Value="0">不使用数据</asp:ListItem>
            <asp:ListItem Value="1">数据库</asp:ListItem>
            <asp:ListItem Value="2">文本文件</asp:ListItem>
            <asp:ListItem Value="3">网址</asp:ListItem>
            <asp:ListItem Value="3">Execl</asp:ListItem>
            <asp:ListItem Value="4">XML文件</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" align="right" class="table_left">
		连接字符串：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDataSoureConntion" runat="server" Width="452px" 
            Height="56px" TextMode="MultiLine"></asp:TextBox>
	    <br />
        数据源,若为数据库则为sql</td></tr>
	<tr>
	<td height="25" align="right" class="table_left">
		输出的数据格式：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_DataType" runat="server">
            <asp:ListItem Value="1">字符串</asp:ListItem>
            <asp:ListItem Value="2">XML数据</asp:ListItem>
            <asp:ListItem Value="3">json</asp:ListItem>
            <asp:ListItem Value="4">DataTable</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" align="right" class="table_left">
		&nbsp;代表显示的值：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDisplayValue" runat="server" Width="200px"></asp:TextBox>
	    (比如 查询出来的 Name 作为显示的下拉框)</td></tr>
	<tr>
	<td height="25" align="right" class="table_left">
		代表实际的值：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTrueValue" runat="server" Width="200px"></asp:TextBox>
	    (而 Id 则为下拉框当中的值)</td></tr>
	<tr>
	<td height="25" align="right" class="table_left">
		输入表单类型：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_FormType" runat="server">
            <asp:ListItem Value="1">文本框</asp:ListItem>
            <asp:ListItem Value="2">下拉列表</asp:ListItem>
            <asp:ListItem Value="3">复选框</asp:ListItem>
            <asp:ListItem Value="4">单选</asp:ListItem>
            <asp:ListItem Value="5">时间选框</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" align="right" class="table_left">
		表单样式：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtFormStyle" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" align="right" class="table_left">
		表单使用Class
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtFormClass" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	</table>
        </td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		搜索项排序
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSort" runat="server" Width="200px">1</asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		值类型 ：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_ValueType" runat="server">
            <asp:ListItem Value="1">字符串/时间</asp:ListItem>
            <asp:ListItem Value="2">整型/数值</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		与值的关系
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_ValueRelChar" runat="server">
            <asp:ListItem Value="=">等于</asp:ListItem>
            <asp:ListItem Value="&lt;">小于</asp:ListItem>
            <asp:ListItem Value="&lt;=">小于等于</asp:ListItem>
            <asp:ListItem Value="&gt;">大于</asp:ListItem>
            <asp:ListItem Value="&gt;=">大于等于</asp:ListItem>
            <asp:ListItem Value="!=">不等于</asp:ListItem>
            <asp:ListItem Value="in">包含</asp:ListItem>
            <asp:ListItem Value="not in">不包含</asp:ListItem>
            <asp:ListItem Value="like">模糊</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		前置字符
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPrefixChar" runat="server" Width="200px"></asp:TextBox>
	    (没有可不填写)</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		前置关系
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_PrefixRelChar" runat="server">
            <asp:ListItem>null</asp:ListItem>
            <asp:ListItem>and</asp:ListItem>
            <asp:ListItem>or</asp:ListItem>
            <asp:ListItem>(</asp:ListItem>
            <asp:ListItem>)</asp:ListItem>
            <asp:ListItem></asp:ListItem>
        </asp:DropDownList>
	    (与之前搜索项的关系)</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		后置字符
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSuffixChar" runat="server" Width="200px"></asp:TextBox>
	    (没有可不写)</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		后置关系
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_SuffixRelChar" runat="server">
            <asp:ListItem>null</asp:ListItem>
            <asp:ListItem>and</asp:ListItem>
            <asp:ListItem>or</asp:ListItem>
            <asp:ListItem>(</asp:ListItem>
            <asp:ListItem>)</asp:ListItem>
            <asp:ListItem></asp:ListItem>
        </asp:DropDownList>
	    (与之后搜索项的关系)</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属的搜索配置
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label ID="Label_SearchConfigId" runat="server" CssClass="fred"></asp:Label>
        <asp:HiddenField ID="HiddenField_SearchConfigId" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		采用值：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_UseValueType" runat="server">
            <asp:ListItem Value="1">text</asp:ListItem>
            <asp:ListItem Value="2">value</asp:ListItem>
        </asp:DropDownList>
        (txt还是value,在前端选择时)</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		显示的默认值
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDefaultValue" runat="server" Width="200px"></asp:TextBox>
	    (没有可不填写)</td></tr>
	</table>
    </div>

     <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
        
    </div>

    </form>
</asp:Content>