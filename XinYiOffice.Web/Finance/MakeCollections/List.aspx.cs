﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;
using XinYiOffice.Common.Web;

namespace XinYiOffice.Web.Finance.MakeCollections
{
    public partial class List : BasicPage
    {
        XinYiOffice.BLL.MakeCollections bll = new XinYiOffice.BLL.MakeCollections();

        public string str_sate = string.Empty;

        public int _page = 1;
        public int _pagezs = 0;
        public int _datazs = 0;
        string benye_url;//当前url
        public string pagehtml = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.ValidatePermission("MAKECOLLECTIONS_MANAGE_LIST"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                BindData();
            }            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;
                }
            }
        }

        #region gridView  
        public void BindData()
        { 
            DataSet ds = new DataSet();
            StringBuilder strWhere = new StringBuilder("select * from vMakeCollections where ");

            string sate_1 = "Sate=1";
            string sate_0 = "Sate=0";

            if (!string.IsNullOrEmpty(xytools.url_get("state")))
            {
                strWhere.Append(sate_0);
            }
            else
            {
                strWhere.Append(sate_1);
            }
            str_sate = xytools.url_get("state");
            if (string.IsNullOrEmpty(str_sate))
            {
                str_sate = "1";
            }

            strWhere.Append(SearchApp1.SqlWhere);
            strWhere.AppendFormat(" and TenantId={0}",CurrentTenantId);
            strWhere.Append(" order by Id desc ");


            #region 分页
            if (!string.IsNullOrEmpty(xytools.url_get("page")))
            {
                _page = SafeConvert.ToInt(xytools.url_get("page"), 1);
            }
            PageDataSet pd = new PageDataSet();
            DataTable dt = pd.GetListPageBySql(strWhere.ToString(), _page, 10, ref _pagezs, ref _datazs);
            PageAttribute pa = new PageAttribute();
            pa.PagCur = _page.ToString();
            pa.PageLinage = "10";
            pa.PageShowFL = "1";
            pa.PageShowGo = "1";

            benye_url = "list.aspx?" + xyurl.GetQueryRemovePage(Request.QueryString);//获取当前url字符串
            pa.PageUrl = benye_url;
            pagehtml = ParseHTML.PageLabel(pa, _pagezs);
            #endregion

            //ds = DbHelperSQL.Query(strWhere.ToString());

            repMakeCollections.DataSource = dt;
            repMakeCollections.DataBind();
        }
        #endregion


        

    }
}
