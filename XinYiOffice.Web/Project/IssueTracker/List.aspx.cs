﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;
using XinYiOffice.Common.Web;

namespace XinYiOffice.Web.Project.IssueTracker
{
    public partial class List : BasicPage
    {

        XinYiOffice.BLL.IssueTracker bll = new XinYiOffice.BLL.IssueTracker();

        public int projectId = 0;
        public int typeId = 0;

        public int _page = 1;
        public int _pagezs = 0;
        public int _datazs = 0;
        string benye_url;//当前url
        public string pagehtml = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("ISSUETRACKER_BASIC") && base.ValidatePermission("ISSUETRACKER_LIST")))
            {
                base.NoPermissionPage();
            }
            
            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;
                }
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    BindData();
                }
            }
        }

        #region gridView

        public void BindData()
        {
            string s = xytools.url_get("s");
            DataTable dt = new DataTable();
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            typeId = SafeConvert.ToInt(xytools.url_get("TypeId"), 0);
            projectId = SafeConvert.ToInt(xytools.url_get("ProjectId"), 0);

            if (typeId != 0)
            {
                strWhere.AppendFormat(" and  Type={0} ", typeId);
            }
            if (projectId != 0)
            {
                if (typeId != 0) { strWhere.Append(" and "); }

                strWhere.AppendFormat(" ProjectInfoId={0} ", projectId);
            }

            if (typeId == 0 && projectId == 0)
            {
                addLink.Visible = false;
            }
            else
            {
                addLink.HRef = string.Format("add.aspx?projectId={0}&typeId={1}", projectId, typeId);

            }

            if (!string.IsNullOrEmpty(s))
            {
                switch (s)
                {
                    case "mylist":
                        if (strWhere.ToString() != string.Empty)
                        {
                            strWhere.Append(" and ");
                        }
                        strWhere.AppendFormat(" (AuthorsAccountId={0} or AssignerAccountId={0}) ", CurrentAccountId);
                        break;
                }
            }

            strWhere.Append(SearchApp1.SqlWhere);
            string mysql = ProjectInfoServer.GetIssueTrackerListSql(strWhere.ToString(), CurrentTenantId);

            #region 分页
            if (!string.IsNullOrEmpty(xytools.url_get("page")))
            {
                _page = SafeConvert.ToInt(xytools.url_get("page"), 1);
            }
            PageDataSet pd = new PageDataSet();
            dt = pd.GetListPageBySql(mysql, _page, 10, ref _pagezs, ref _datazs);
            PageAttribute pa = new PageAttribute();
            pa.PagCur = _page.ToString();
            pa.PageLinage = "10";
            pa.PageShowFL = "1";
            pa.PageShowGo = "1";

            benye_url = "list.aspx?" + xyurl.GetQueryRemovePage(Request.QueryString);//获取当前url字符串
            pa.PageUrl = benye_url;
            pagehtml = ParseHTML.PageLabel(pa, _pagezs);
            #endregion

            repIssueTracker.DataSource = dt;
            repIssueTracker.DataBind();

        }

        #endregion

    }
}
