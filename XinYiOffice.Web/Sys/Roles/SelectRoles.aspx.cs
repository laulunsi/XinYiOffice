﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.Roles
{
    public partial class SelectRoles : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
            }

            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "add":
                        this.Page.Visible = false;
                        Add();
                        break;
                }
            }
        }

        protected void Add()
        {
            try
            {
                object obj = Request.Params["aa"];

                int accountId = SafeConvert.ToInt(HiddenField_AccountId.Value);
                string strSelectRoles = SafeConvert.ToString(HiddenField_SelectRoles.Value);

                new BLL.AccountRoles().DeleteList(string.Format(" select Id from AccountRoles where AccountId={0} and TenantId={1} ", accountId,CurrentTenantId));

                if (!string.IsNullOrEmpty(strSelectRoles))
                {
                    strSelectRoles = strSelectRoles.TrimEnd(',');

                    string[] rolList = strSelectRoles.Split(',');

                    foreach (string rol in rolList)
                    {
                        int _rolId = SafeConvert.ToInt(rol, 0);
                        if (_rolId == 0) { continue; }

                        Model.AccountRoles ar = new Model.AccountRoles();
                        ar.AccountId = accountId;
                        ar.CreateAccountId = CurrentAccountId;
                        ar.CreateTime = DateTime.Now;
                        ar.RoleId = _rolId;
                        ar.TenantId = CurrentTenantId;

                        new BLL.AccountRoles().Add(ar);
                    }

                }

                xytools.write("1");

            }
            catch (Exception ex)
            {
                EasyLog.WriteLog(ex);
                xytools.write("-1");
            }
            finally
            {
            }


        }


        protected void InitData()
        {
            HiddenField_AccountId.Value = SafeConvert.ToString(xytools.url_get("accountId"));

            ListBox_Roles.DataSource = new BLL.Roles().GetList(string.Format("Id not in (select RoleId from AccountRoles where AccountId={0}) and TenantId={1} ", HiddenField_AccountId.Value,CurrentTenantId));
            ListBox_Roles.DataTextField = "Name";
            ListBox_Roles.DataValueField = "Id";
            ListBox_Roles.DataBind();

            ListBox_SelectRoles.DataSource = new BLL.Roles().GetList(string.Format("Id in (select RoleId from AccountRoles where AccountId={0}) and TenantId={1} ", HiddenField_AccountId.Value,CurrentTenantId));
            ListBox_SelectRoles.DataTextField = "Name";
            ListBox_SelectRoles.DataValueField = "Id";
            ListBox_SelectRoles.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {



        }
    }
}