﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 职员表
	/// </summary>
	[Serializable]
	public partial class OfficeWorker
	{
		public OfficeWorker()
		{}
		#region Model
		private int _id;
		private string _fullname;
		private string _usedname;
		private int? _sex;
		private string _email;
		private string _tel;
		private string _phone;
		private string _intnumber;
		private int? _institutionid;
		private int? _departmentid;
		private int? _position;
		private string _province;
		private string _city;
		private string _county;
		private string _street;
		private string _zipcode;
		private string _birthdate;
		private string _chinaid;
		private string _nationality;
		private string _nativeplace;
		private string _phone1;
		private string _phone2;
		private string _politicalstatus;
		private string _entrytime;
		private string _entrancemode;
		private int? _postgrades;
		private int? _wagelevel;
		private string _insurancewelfare;
		private string _graduateschool;
		private string _formalschooling;
		private string _major;
		private string _englishlevel;
		private string _prework;
		private string _preposition;
		private string _prestarttime;
		private string _preendtime;
		private string _preepartment;
		private string _turnovertime;
		private int? _sate;
		private string _remarks;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 姓名
		/// </summary>
		public string FullName
		{
			set{ _fullname=value;}
			get{return _fullname;}
		}
		/// <summary>
		/// 曾用名
		/// </summary>
		public string UsedName
		{
			set{ _usedname=value;}
			get{return _usedname;}
		}
		/// <summary>
		/// 性别
		/// </summary>
		public int? Sex
		{
			set{ _sex=value;}
			get{return _sex;}
		}
		/// <summary>
		/// 电子邮件
		/// </summary>
		public string Email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 电话
		/// </summary>
		public string Tel
		{
			set{ _tel=value;}
			get{return _tel;}
		}
		/// <summary>
		/// 移动电话
		/// </summary>
		public string Phone
		{
			set{ _phone=value;}
			get{return _phone;}
		}
		/// <summary>
		/// 内部编号
		/// </summary>
		public string IntNumber
		{
			set{ _intnumber=value;}
			get{return _intnumber;}
		}
		/// <summary>
		/// 所属机构
		/// </summary>
		public int? InstitutionId
		{
			set{ _institutionid=value;}
			get{return _institutionid;}
		}
		/// <summary>
		/// 部门
		/// </summary>
		public int? DepartmentId
		{
			set{ _departmentid=value;}
			get{return _departmentid;}
		}
		/// <summary>
		/// 职位
		/// </summary>
		public int? Position
		{
			set{ _position=value;}
			get{return _position;}
		}
		/// <summary>
		/// 住址-省
		/// </summary>
		public string Province
		{
			set{ _province=value;}
			get{return _province;}
		}
		/// <summary>
		/// 市
		/// </summary>
		public string City
		{
			set{ _city=value;}
			get{return _city;}
		}
		/// <summary>
		/// 县
		/// </summary>
		public string County
		{
			set{ _county=value;}
			get{return _county;}
		}
		/// <summary>
		/// 街道
		/// </summary>
		public string Street
		{
			set{ _street=value;}
			get{return _street;}
		}
		/// <summary>
		/// 邮编
		/// </summary>
		public string ZipCode
		{
			set{ _zipcode=value;}
			get{return _zipcode;}
		}
		/// <summary>
		/// 出生年月
		/// </summary>
		public string BirthDate
		{
			set{ _birthdate=value;}
			get{return _birthdate;}
		}
		/// <summary>
		/// 身份证
		/// </summary>
		public string ChinaID
		{
			set{ _chinaid=value;}
			get{return _chinaid;}
		}
		/// <summary>
		/// 民族
		/// </summary>
		public string Nationality
		{
			set{ _nationality=value;}
			get{return _nationality;}
		}
		/// <summary>
		/// 籍贯
		/// </summary>
		public string NativePlace
		{
			set{ _nativeplace=value;}
			get{return _nativeplace;}
		}
		/// <summary>
		/// 家庭电话1
		/// </summary>
		public string Phone1
		{
			set{ _phone1=value;}
			get{return _phone1;}
		}
		/// <summary>
		/// 家庭电话2
		/// </summary>
		public string Phone2
		{
			set{ _phone2=value;}
			get{return _phone2;}
		}
		/// <summary>
		/// 政治面貌
		/// </summary>
		public string PoliticalStatus
		{
			set{ _politicalstatus=value;}
			get{return _politicalstatus;}
		}
		/// <summary>
		/// 入职时间
		/// </summary>
		public string EntryTime
		{
			set{ _entrytime=value;}
			get{return _entrytime;}
		}
		/// <summary>
		/// 进入方式
		/// </summary>
		public string EntranceMode
		{
			set{ _entrancemode=value;}
			get{return _entrancemode;}
		}
		/// <summary>
		/// 岗位级别
		/// </summary>
		public int? PostGrades
		{
			set{ _postgrades=value;}
			get{return _postgrades;}
		}
		/// <summary>
		/// 工资级别
		/// </summary>
		public int? WageLevel
		{
			set{ _wagelevel=value;}
			get{return _wagelevel;}
		}
		/// <summary>
		/// 保险福利
		/// </summary>
		public string InsuranceWelfare
		{
			set{ _insurancewelfare=value;}
			get{return _insurancewelfare;}
		}
		/// <summary>
		/// 毕业学校
		/// </summary>
		public string GraduateSchool
		{
			set{ _graduateschool=value;}
			get{return _graduateschool;}
		}
		/// <summary>
		/// 学历
		/// </summary>
		public string FormalSchooling
		{
			set{ _formalschooling=value;}
			get{return _formalschooling;}
		}
		/// <summary>
		/// 专业
		/// </summary>
		public string Major
		{
			set{ _major=value;}
			get{return _major;}
		}
		/// <summary>
		/// 英语水平
		/// </summary>
		public string EnglishLevel
		{
			set{ _englishlevel=value;}
			get{return _englishlevel;}
		}
		/// <summary>
		/// 上一家工作单位
		/// </summary>
		public string PreWork
		{
			set{ _prework=value;}
			get{return _prework;}
		}
		/// <summary>
		/// 上一家职位
		/// </summary>
		public string PrePosition
		{
			set{ _preposition=value;}
			get{return _preposition;}
		}
		/// <summary>
		/// 上一家开始时间
		/// </summary>
		public string PreStartTime
		{
			set{ _prestarttime=value;}
			get{return _prestarttime;}
		}
		/// <summary>
		/// 上一家结束时间
		/// </summary>
		public string PreEndTime
		{
			set{ _preendtime=value;}
			get{return _preendtime;}
		}
		/// <summary>
		/// 上一家部门
		/// </summary>
		public string PreEpartment
		{
			set{ _preepartment=value;}
			get{return _preepartment;}
		}
		/// <summary>
		/// 离职时间
		/// </summary>
		public string TurnoverTime
		{
			set{ _turnovertime=value;}
			get{return _turnovertime;}
		}
		/// <summary>
		/// 状态
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string Remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

