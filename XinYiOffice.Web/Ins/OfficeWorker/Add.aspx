﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"   AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Ins.OfficeWorker.Add" Title="增加页" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>
    $(function () {

        var DropDownList_InstitutionId = $("#<%=DropDownList_InstitutionId.ClientID%>");
        var DropDownList_DepartmentId = $("#<%=DropDownList_DepartmentId.ClientID%>");
        var HiddenField_DepartmentId = $("#<%=HiddenField_DepartmentId.ClientID%>");

        //机构选择事件
        DropDownList_InstitutionId.change(function () {
            DropDownList_DepartmentId.html("");
            DropDownList_DepartmentId.append("<option value='0'>请选择部门</option>");


            var insid = DropDownList_InstitutionId.val();
            $.getJSON("/Call/Ajax.aspx?action=getdepartmentbyins&insid=" + insid + "&sj=" + Math.random(), function (json) {

                $.each(json, function (i) {
                    DropDownList_DepartmentId.append("<option value='" + json[i].value + "'>" + json[i].label + "</option>");
                });
            });


        });

        //职位选择事件
        DropDownList_DepartmentId.change(function () {
            HiddenField_DepartmentId.val(DropDownList_DepartmentId.val());
        });


    });
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">



<form  id="f2" name="f2" runat="server">
 <div class="setup_box" style="margin-bottom: 0">
 <div class="h_title"><h4>添加人员</h4></div>
                
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		姓名
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtFullName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		曾用名
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtUsedName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		性别
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_Sex" runat="server">
            <asp:ListItem Value="1">男</asp:ListItem>
            <asp:ListItem Value="0">女</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		电子邮件
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtEmail" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		电话
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTel" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		移动电话
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPhone" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		内部编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtIntNumber" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属机构
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_InstitutionId" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		部门
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_DepartmentId" runat="server">
        </asp:DropDownList>
	    <asp:HiddenField ID="HiddenField_DepartmentId" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		职位
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_Position" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		住址-省
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtProvince" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		市
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCity" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		县
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCounty" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		街道
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtStreet" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		邮编
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtZipCode" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		出生年月
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtBirthDate" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		身份证
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtChinaID" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		民族
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtNationality" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		籍贯
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtNativePlace" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		家庭电话1
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPhone1" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		家庭电话2
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPhone2" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		政治面貌
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPoliticalStatus" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		入职时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtEntryTime" runat="server" Width="200px" class="Wdate"  onClick="WdatePicker()"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		进入方式
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtEntranceMode" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		岗位级别
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_PostGrades" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		工资级别
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_WageLevel" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		保险福利
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtInsuranceWelfare" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		毕业学校
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtGraduateSchool" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		学历
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtFormalSchooling" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		专业
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtMajor" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		英语水平
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtEnglishLevel" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		上一家工作单位
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPreWork" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		上一家职位
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPrePosition" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		上一家开始时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPreStartTime" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		上一家结束时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPreEndTime" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		上一家部门
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPreEpartment" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		离职时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTurnoverTime" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_Sate" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		备注
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRemarks" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	</table>
        <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" OnClick="LinkButton1_Click">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div></div>

    </form></asp:Content>