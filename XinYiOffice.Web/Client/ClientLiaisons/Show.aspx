﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Client.ClientLiaisons.Show" Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">
<div class="setup_box" style=" margin-bottom:0">
<div class="h_title"><h4>基本信息</h4></div>

<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="14%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="43%" align="left">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td>
	<td width="19%" align="left" class="table_left">联系人编号
	：</td>
	<td width="24%" align="left"><asp:Label id="lblPersonNumber" runat="server"></asp:Label></td>
	</tr>
	<tr>
	<td height="25" width="14%" align="right" class="table_left">
		所属客户：</td>
	<td height="25" align="left">
		<asp:Label id="lblClientInfoId" runat="server"></asp:Label>
	</td>
	<td height="25" align="left" class="table_left">权限
	：</td>
	<td height="25" align="left"><asp:Label id="lblPower" runat="server"></asp:Label></td>
	</tr>
	<tr>
	<td height="25" width="14%" align="right" class="table_left">联系人姓名
	：</td>
	<td height="25" align="left"><asp:Label id="lblFullName" runat="server"></asp:Label></td>
	<td height="25" align="left" class="table_left">其他电话
	：</td>
	<td height="25" align="left"><asp:Label id="lblOtherPhone" runat="server"></asp:Label></td>
	</tr>
	<tr>
	<td height="25" width="14%" align="right" class="table_left">手机
	：</td>
	<td height="25" align="left"><asp:Label id="lblPhone" runat="server"></asp:Label></td>
	<td height="25" align="left" class="table_left">电子邮件
	：</td>
	<td height="25" align="left"><asp:Label id="lblEmail" runat="server"></asp:Label></td>
	</tr>
	<tr>
	<td height="25" width="14%" align="right" class="table_left">传真
	：</td>
	<td height="25" align="left"><asp:Label id="lblFax" runat="server"></asp:Label></td>
	<td height="25" align="left" class="table_left">所属职员
	：</td>
	<td height="25" align="left"><asp:Label id="lblByClientInfoId" runat="server"></asp:Label></td>
	</tr>
	<tr>
	<td height="25" width="14%" align="right" class="table_left">QQ
	：</td>
	<td height="25" align="left"><asp:Label id="lblQQ" runat="server"></asp:Label></td>
	<td height="25" align="left" class="table_left">职务
	：</td>
	<td height="25" align="left"><asp:Label id="lblFUNCTION" runat="server"></asp:Label></td>
	</tr>
	<tr>
	<td height="25" width="14%" align="right" class="table_left">生日
	：</td>
	<td height="25" align="left"><asp:Label id="lblBirthday" runat="server"></asp:Label></td>
	<td height="25" align="left" class="table_left">来源
	：</td>
	<td height="25" align="left"><asp:Label id="lblSource" runat="server"></asp:Label></td>
	</tr>
	<tr>
	<td height="25" width="14%" align="right" class="table_left">部门
	：</td>
	<td height="25" align="left"><asp:Label id="lblDepartment" runat="server"></asp:Label></td>
	<td height="25" align="left" class="table_left">联系人分类
	：</td>
	<td height="25" align="left"><asp:Label id="lblClassiFication" runat="server"></asp:Label></td>
	</tr>
	<tr>
	<td height="25" width="14%" align="right" class="table_left">直属上级
	：</td>
	<td height="25" align="left"><asp:Label id="lblDirectSuperior" runat="server"></asp:Label></td>
	<td height="25" align="left" class="table_left">主要联系人
	：</td>
	<td height="25" align="left"><asp:Label id="lblPrimaryContact" runat="server"></asp:Label></td>
	</tr>
	<tr>
	<td height="25" width="14%" align="right" class="table_left">是否可以联系
	：</td>
	<td height="25" align="left"><asp:Label id="lblIfContact" runat="server"></asp:Label></td>
	<td height="25" align="left" class="table_left">&nbsp;</td>
	<td height="25" align="left">&nbsp;</td>
	</tr>
	<tr>
	<td height="25" width="14%" align="right" class="table_left">负责业务
	：</td>
	<td height="25" align="left"><asp:Label id="lblBusiness" runat="server"></asp:Label></td>
	<td height="25" align="left" class="table_left">&nbsp;</td>
	<td height="25" align="left">&nbsp;</td>
	</tr>
</table>

<div class="h_title"><h4>联系信息</h4></div>
<table width="100%" border="0" cellPadding="0" cellSpacing="0">
  <tr>
    <td height="25" width="14%" align="right" class="table_left"> 接受短信手机
      ：</td>
    <td height="25" align="left"><asp:Label id="lblShortPhone" runat="server"></asp:Label></td>
    <td height="25" align="left" class="table_left">省份
      ：</td>
    <td height="25" align="left"><asp:Label id="lblProvince" runat="server"></asp:Label></td>
  </tr>
  <tr>
    <td height="25" width="14%" align="right" class="table_left"> 性别
      ：</td>
    <td height="25" align="left"><asp:Label id="lblSex" runat="server"></asp:Label></td>
    <td height="25" align="left" class="table_left">区县
      ：</td>
    <td height="25" align="left"><asp:Label id="lblCounty" runat="server"></asp:Label></td>
  </tr>
  <tr>
    <td height="25" width="14%" align="right" class="table_left"> 国家地区
      ：</td>
    <td height="25" align="left"><asp:Label id="lblCountriesRegions" runat="server"></asp:Label></td>
    <td height="25" align="left" class="table_left">&nbsp;</td>
    <td height="25" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td height="25" width="14%" align="right" class="table_left"> 邮编
      ：</td>
    <td height="25" colspan="3" align="left"><asp:Label id="lblZip" runat="server"></asp:Label></td>
  </tr>
  <tr>
    <td height="25" width="14%" align="right" class="table_left"> 地址
      ：</td>
    <td height="25" colspan="3" align="left"><asp:Label id="lblAddress" runat="server"></asp:Label></td>
  </tr>
  <tr>
    <td height="25" width="14%" align="right" class="table_left"> 备注
      ：</td>
    <td height="25" colspan="3" align="left"><asp:Label id="lblRemarks" runat="server"></asp:Label></td>
  </tr>
</table>

</div>


       <div class="clear btn_box">
      <a href="javascript:history.go(-1);;" class="cancel">返回</a>
      </div>


            </form>
</asp:Content>