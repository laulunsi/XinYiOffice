﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 客户信息表
	/// </summary>
	public partial class ClientInfo
	{
		private readonly XinYiOffice.DAL.ClientInfo dal=new XinYiOffice.DAL.ClientInfo();
		public ClientInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.ClientInfo model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.ClientInfo model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.ClientInfo GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.ClientInfo GetModelByCache(int Id)
		{
			
			string CacheKey = "ClientInfoModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.ClientInfo)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.ClientInfo> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.ClientInfo> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.ClientInfo> modelList = new List<XinYiOffice.Model.ClientInfo>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.ClientInfo model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.ClientInfo();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["ByAccountId"]!=null && dt.Rows[n]["ByAccountId"].ToString()!="")
					{
						model.ByAccountId=int.Parse(dt.Rows[n]["ByAccountId"].ToString());
					}
					if(dt.Rows[n]["Name"]!=null && dt.Rows[n]["Name"].ToString()!="")
					{
					model.Name=dt.Rows[n]["Name"].ToString();
					}
					if(dt.Rows[n]["Heat"]!=null && dt.Rows[n]["Heat"].ToString()!="")
					{
						model.Heat=int.Parse(dt.Rows[n]["Heat"].ToString());
					}
					if(dt.Rows[n]["ClientType"]!=null && dt.Rows[n]["ClientType"].ToString()!="")
					{
						model.ClientType=int.Parse(dt.Rows[n]["ClientType"].ToString());
					}
					if(dt.Rows[n]["Industry"]!=null && dt.Rows[n]["Industry"].ToString()!="")
					{
						model.Industry=int.Parse(dt.Rows[n]["Industry"].ToString());
					}
					if(dt.Rows[n]["Source"]!=null && dt.Rows[n]["Source"].ToString()!="")
					{
						model.Source=int.Parse(dt.Rows[n]["Source"].ToString());
					}
					if(dt.Rows[n]["MarketingPlanId"]!=null && dt.Rows[n]["MarketingPlanId"].ToString()!="")
					{
						model.MarketingPlanId=int.Parse(dt.Rows[n]["MarketingPlanId"].ToString());
					}
					if(dt.Rows[n]["Tag"]!=null && dt.Rows[n]["Tag"].ToString()!="")
					{
					model.Tag=dt.Rows[n]["Tag"].ToString();
					}
					if(dt.Rows[n]["BankDeposit"]!=null && dt.Rows[n]["BankDeposit"].ToString()!="")
					{
					model.BankDeposit=dt.Rows[n]["BankDeposit"].ToString();
					}
					if(dt.Rows[n]["TaxNo"]!=null && dt.Rows[n]["TaxNo"].ToString()!="")
					{
					model.TaxNo=dt.Rows[n]["TaxNo"].ToString();
					}
					if(dt.Rows[n]["IntNumber"]!=null && dt.Rows[n]["IntNumber"].ToString()!="")
					{
					model.IntNumber=dt.Rows[n]["IntNumber"].ToString();
					}
					if(dt.Rows[n]["Power"]!=null && dt.Rows[n]["Power"].ToString()!="")
					{
						model.Power=int.Parse(dt.Rows[n]["Power"].ToString());
					}
					if(dt.Rows[n]["QualityRating"]!=null && dt.Rows[n]["QualityRating"].ToString()!="")
					{
						model.QualityRating=int.Parse(dt.Rows[n]["QualityRating"].ToString());
					}
					if(dt.Rows[n]["ValueAssessment"]!=null && dt.Rows[n]["ValueAssessment"].ToString()!="")
					{
						model.ValueAssessment=int.Parse(dt.Rows[n]["ValueAssessment"].ToString());
					}
					if(dt.Rows[n]["CustomerLevel"]!=null && dt.Rows[n]["CustomerLevel"].ToString()!="")
					{
						model.CustomerLevel=int.Parse(dt.Rows[n]["CustomerLevel"].ToString());
					}
					if(dt.Rows[n]["RelBetweenGrade"]!=null && dt.Rows[n]["RelBetweenGrade"].ToString()!="")
					{
						model.RelBetweenGrade=int.Parse(dt.Rows[n]["RelBetweenGrade"].ToString());
					}
					if(dt.Rows[n]["Seedtime"]!=null && dt.Rows[n]["Seedtime"].ToString()!="")
					{
						model.Seedtime=int.Parse(dt.Rows[n]["Seedtime"].ToString());
					}
					if(dt.Rows[n]["SupClientInfoId"]!=null && dt.Rows[n]["SupClientInfoId"].ToString()!="")
					{
						model.SupClientInfoId=int.Parse(dt.Rows[n]["SupClientInfoId"].ToString());
					}
					if(dt.Rows[n]["BankAccount"]!=null && dt.Rows[n]["BankAccount"].ToString()!="")
					{
					model.BankAccount=dt.Rows[n]["BankAccount"].ToString();
					}
					if(dt.Rows[n]["MobilePhone"]!=null && dt.Rows[n]["MobilePhone"].ToString()!="")
					{
					model.MobilePhone=dt.Rows[n]["MobilePhone"].ToString();
					}
					if(dt.Rows[n]["CountriesRegions"]!=null && dt.Rows[n]["CountriesRegions"].ToString()!="")
					{
						model.CountriesRegions=int.Parse(dt.Rows[n]["CountriesRegions"].ToString());
					}
					if(dt.Rows[n]["Phone"]!=null && dt.Rows[n]["Phone"].ToString()!="")
					{
					model.Phone=dt.Rows[n]["Phone"].ToString();
					}
					if(dt.Rows[n]["Fax"]!=null && dt.Rows[n]["Fax"].ToString()!="")
					{
					model.Fax=dt.Rows[n]["Fax"].ToString();
					}
					if(dt.Rows[n]["Email"]!=null && dt.Rows[n]["Email"].ToString()!="")
					{
					model.Email=dt.Rows[n]["Email"].ToString();
					}
					if(dt.Rows[n]["ZipCode"]!=null && dt.Rows[n]["ZipCode"].ToString()!="")
					{
					model.ZipCode=dt.Rows[n]["ZipCode"].ToString();
					}
					if(dt.Rows[n]["Province"]!=null && dt.Rows[n]["Province"].ToString()!="")
					{
						model.Province=int.Parse(dt.Rows[n]["Province"].ToString());
					}
					if(dt.Rows[n]["City"]!=null && dt.Rows[n]["City"].ToString()!="")
					{
						model.City=int.Parse(dt.Rows[n]["City"].ToString());
					}
					if(dt.Rows[n]["County"]!=null && dt.Rows[n]["County"].ToString()!="")
					{
					model.County=dt.Rows[n]["County"].ToString();
					}
					if(dt.Rows[n]["Address"]!=null && dt.Rows[n]["Address"].ToString()!="")
					{
					model.Address=dt.Rows[n]["Address"].ToString();
					}
					if(dt.Rows[n]["CompanyProfile"]!=null && dt.Rows[n]["CompanyProfile"].ToString()!="")
					{
					model.CompanyProfile=dt.Rows[n]["CompanyProfile"].ToString();
					}
					if(dt.Rows[n]["Remarks"]!=null && dt.Rows[n]["Remarks"].ToString()!="")
					{
					model.Remarks=dt.Rows[n]["Remarks"].ToString();
					}
					if(dt.Rows[n]["GeneralManager"]!=null && dt.Rows[n]["GeneralManager"].ToString()!="")
					{
					model.GeneralManager=dt.Rows[n]["GeneralManager"].ToString();
					}
					if(dt.Rows[n]["BusinessEntity"]!=null && dt.Rows[n]["BusinessEntity"].ToString()!="")
					{
					model.BusinessEntity=dt.Rows[n]["BusinessEntity"].ToString();
					}
					if(dt.Rows[n]["BusinessLicence"]!=null && dt.Rows[n]["BusinessLicence"].ToString()!="")
					{
					model.BusinessLicence=dt.Rows[n]["BusinessLicence"].ToString();
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["RefreshAccountId"]!=null && dt.Rows[n]["RefreshAccountId"].ToString()!="")
					{
						model.RefreshAccountId=int.Parse(dt.Rows[n]["RefreshAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["RefreshTime"]!=null && dt.Rows[n]["RefreshTime"].ToString()!="")
					{
						model.RefreshTime=DateTime.Parse(dt.Rows[n]["RefreshTime"].ToString());
					}
					if(dt.Rows[n]["LinkClientId"]!=null && dt.Rows[n]["LinkClientId"].ToString()!="")
					{
						model.LinkClientId=int.Parse(dt.Rows[n]["LinkClientId"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

