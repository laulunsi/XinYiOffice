﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Sys.SearchDataSoure.Add" Title="增加页" %>

<form  id="f2" name="f2" runat="server">
    <table style="width: 100%;" cellpadding="2" cellspacing="1" class="border">
        <tr>
            <td class="tdbg">
                
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right">
		数据源 ：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_DataSoureType" runat="server">
            <asp:ListItem Value="1">数据库</asp:ListItem>
            <asp:ListItem Value="2">文本文件</asp:ListItem>
            <asp:ListItem Value="3">网址</asp:ListItem>
            <asp:ListItem Value="3">Execl</asp:ListItem>
            <asp:ListItem Value="4">XML文件</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		连接字符串：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDataSoureConntion" runat="server" Width="452px" 
            Height="56px" TextMode="MultiLine"></asp:TextBox>
	    <br />
        数据源,若为数据库则为sql</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		输出的数据格式：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_DataType" runat="server">
            <asp:ListItem Value="1">字符串</asp:ListItem>
            <asp:ListItem Value="2">XML数据</asp:ListItem>
            <asp:ListItem Value="3">json</asp:ListItem>
            <asp:ListItem Value="4">DataTable</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		&nbsp;代表显示的值：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDisplayValue" runat="server" Width="200px"></asp:TextBox>
	    (比如 查询出来的 Name 作为显示的下拉框)</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		代表实际的值：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTrueValue" runat="server" Width="200px"></asp:TextBox>
	    (而 Id 则为下拉框当中的值)</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		输入表单类型：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_FormType" runat="server">
            <asp:ListItem Value="1">文本框</asp:ListItem>
            <asp:ListItem Value="2">下拉列表</asp:ListItem>
            <asp:ListItem Value="3">复选框</asp:ListItem>
            <asp:ListItem Value="4">单选</asp:ListItem>
            <asp:ListItem Value="5">时间选框</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		表单样式：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtFormStyle" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		表单使用Class
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtFormClass" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	</table>
<script src="/js/calendar1.js" type="text/javascript"></script>

            </td>
        </tr>
        <tr>
            <td class="tdbg" align="center" valign="bottom">
                <asp:Button ID="btnSave" runat="server" Text="保存"
                    OnClick="btnSave_Click" class="inputbutton" onmouseover="this.className='inputbutton_hover'"
                    onmouseout="this.className='inputbutton'"></asp:Button>
                <asp:Button ID="btnCancle" runat="server" Text="取消"
                    OnClick="btnCancle_Click" class="inputbutton" onmouseover="this.className='inputbutton_hover'"
                    onmouseout="this.className='inputbutton'"></asp:Button>
            </td>
        </tr>
    </table>
    <br />
    </form>