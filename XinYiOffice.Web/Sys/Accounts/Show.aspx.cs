﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Basic;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.Accounts
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("ACCOUNT_MANAGE") && ValidatePermission("ACCOUNT_LIST_VIEW")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.Accounts bll = new XinYiOffice.BLL.Accounts();
            XinYiOffice.Model.Accounts model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.lblAccountName.Text = model.AccountName;
            this.lblAccountPassword.Text = model.AccountPassword;
            this.lblSate.Text = model.Sate.ToString();

            if (model.Sate == 1)
            {
                lblSate.Text = "正常";
            }
            else if (model.Sate == 2)
            {
                lblSate.Text = "未激活";
            }
            else if (model.Sate == 3)
            {
                lblSate.Text = "异常";
            }

            this.lblNiceName.Text = model.NiceName;

            this.lblEnName.Text = model.EnName;
            this.lblTimeZone.Text = model.TimeZone.ToString();
            this.lblEmail.Text = model.Email;
            //this.lblHeadPortrait.Text = model.HeadPortrait;

            Image1.ImageUrl = string.Format("/img/face/{0}.png", model.HeadPortrait);

            this.lblPhone.Text = model.Phone;

            if (model.AccountType == 1)
            {
                this.lblAccountType.Text = "内部职员";
            }
            else if (model.AccountType == 2)
            {
                this.lblAccountType.Text = "客户方面";
            }
            else if (model.AccountType == 3)
            {
                this.lblAccountType.Text = "合作伙伴";
            }

            

            this.lblKeyId.Text = model.KeyId.ToString();
            this.lblCreateAccountId.Text = AccountsServer.GetFullNameByAccountId(SafeConvert.ToInt(model.CreateAccountId));
            this.lblRefreshAccountId.Text = AccountsServer.GetFullNameByAccountId(SafeConvert.ToInt(model.RefreshAccountId));

            this.lblCreateTime.Text = model.CreateTime.ToString();
            this.lblRefreshTime.Text = model.RefreshTime.ToString();

        }


    }
}
