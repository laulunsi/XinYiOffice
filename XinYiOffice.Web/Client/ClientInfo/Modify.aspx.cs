﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Client.ClientInfo
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("CLIENTINFO_LIST_EDIT"))
            {
                NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));

                    ShowInfo(Id);
                }
            }
        }

        protected void InitData()
        {
            SetDropDownList("Heat", ref DropDownList_txtHeat);
            SetDropDownList("ClientType", ref DropDownList_txtClientType);
            SetDropDownList("Industry", ref DropDownList1_txtIndustry);
            SetDropDownList("Source", ref DropDownList_txtSource);
            SetDropDownList("Power", ref DropDownList_Power);
            SetDropDownList("QualityRating", ref DropDownList_txtQualityRating);
            SetDropDownList("ValueAssessment ", ref DropDownList_txtValueAssessment);
            SetDropDownList("CustomerLevel", ref DropDownList_txtCustomerLevel);
            SetDropDownList("RelBetweenGrade", ref DropDownList_txtRelBetweenGrade);
            SetDropDownList("Seedtime", ref DropDownList_txtSeedtime);
            SetDropDownList("CountriesRegions", ref DropDownListCountriesRegions);

            DropDownList_Province.DataSource = AppDataCacheServer.Province;
            DropDownList_Province.DataTextField = "Name";
            DropDownList_Province.DataValueField = "Id";
            DropDownList_Province.DataBind();


        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.ClientInfoDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        private void ShowInfo(int Id)
        {
            InitData();

            XinYiOffice.BLL.ClientInfo bll = new XinYiOffice.BLL.ClientInfo();
            XinYiOffice.Model.ClientInfo model = bll.GetModel(Id);
            Model.Accounts acc = new Model.Accounts();
            Model.MarketingPlan mp = new Model.MarketingPlan();

            acc = new BLL.Accounts().GetModel(SafeConvert.ToInt(model.ByAccountId, 0));
            mp = new BLL.MarketingPlan().GetModel(SafeConvert.ToInt(model.MarketingPlanId));

            if (mp != null)
            {
                hid_MarketingPlanId.Value = model.MarketingPlanId.ToString();
                TextBox_MarketingPlanId.Text = mp.ProgramName;
            }
            else
            {
                hid_MarketingPlanId.Value ="0";
                TextBox_MarketingPlanId.Text ="无";
            }



            this.hidId.Value = model.Id.ToString();
            this.txtByAccountId.Text = acc.FullName;

            hidByAccountId.Value = model.ByAccountId.ToString();

            this.txtName.Text = model.Name;
            DropDownList_txtHeat.SelectedValue = model.Heat.ToString();

            DropDownList_txtClientType.SelectedValue = model.ClientType.ToString();
            DropDownList1_txtIndustry.SelectedValue = model.Industry.ToString();
            DropDownList_txtSource.SelectedValue = model.Source.ToString();

            this.txtTag.Text = model.Tag;
            this.txtBankDeposit.Text = model.BankDeposit;
            this.txtTaxNo.Text = model.TaxNo;
            this.txtIntNumber.Text = model.IntNumber;
            DropDownList_Power.SelectedValue = model.Power.ToString();
            DropDownList_txtQualityRating.SelectedValue = model.QualityRating.ToString();
            DropDownList_txtValueAssessment.SelectedValue = model.ValueAssessment.ToString();
            DropDownList_txtCustomerLevel.SelectedValue = model.CustomerLevel.ToString();
            DropDownList_txtRelBetweenGrade.SelectedValue = model.RelBetweenGrade.ToString();
            DropDownList_txtSeedtime.SelectedValue = model.Seedtime.ToString();

            int clientId = SafeConvert.ToInt(model.SupClientInfoId);
            hidSupClientInfoId.Value = clientId.ToString();
            if (clientId != 0)
            {
                Model.ClientInfo client = new BLL.ClientInfo().GetModel(clientId);
                TextBox_SupClientInfoId.Text = client.Name;
            }

            this.txtBankAccount.Text = model.BankAccount;
            this.txtMobilePhone.Text = model.MobilePhone;

            DropDownListCountriesRegions.SelectedValue = model.CountriesRegions.ToString();
            this.txtPhone.Text = model.Phone;
            this.txtFax.Text = model.Fax;
            this.txtEmail.Text = model.Email;
            this.txtZipCode.Text = model.ZipCode;
            DropDownList_Province.SelectedValue = model.Province.ToString();

            DropDownList_City.DataSource = AppDataCacheServer.GetCityByPid(SafeConvert.ToInt(model.Province));
            DropDownList_City.DataTextField = "Name";
            DropDownList_City.DataValueField = "Id";
            DropDownList_City.DataBind();
            DropDownList_City.SelectedValue = model.City.ToString();



            this.txtCounty.Text = model.County;
            this.txtAddress.Text = model.Address;
            this.txtCompanyProfile.Text = model.CompanyProfile;
            this.txtRemarks.Text = model.Remarks;
            this.txtGeneralManager.Text = model.GeneralManager;
            this.txtBusinessEntity.Text = model.BusinessEntity;
            this.txtBusinessLicence.Text = model.BusinessLicence;

            HiddenField_CreateAccountId.Value = SafeConvert.ToString(model.CreateAccountId);
            HiddenField_CreateTime.Value = SafeConvert.ToString(model.CreateTime);
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = SafeConvert.ToInt(this.hidId.Value);
            int ByAccountId = SafeConvert.ToInt(hidByAccountId.Value);
            string Name = this.txtName.Text;
            int Heat = SafeConvert.ToInt(DropDownList_txtHeat.SelectedValue);
            int ClientType = SafeConvert.ToInt(DropDownList_txtClientType.SelectedValue);
            int Industry = SafeConvert.ToInt(DropDownList1_txtIndustry.SelectedValue);
            int Source = SafeConvert.ToInt(DropDownList_txtSource.SelectedValue); //this.txtSource.Text;
            string Tag = this.txtTag.Text;
            string BankDeposit = this.txtBankDeposit.Text;
            string TaxNo = this.txtTaxNo.Text;
            string IntNumber = this.txtIntNumber.Text;
            int Power = SafeConvert.ToInt(DropDownList_Power.SelectedValue);
            int QualityRating = SafeConvert.ToInt(DropDownList_txtQualityRating.SelectedValue);
            int ValueAssessment = SafeConvert.ToInt(DropDownList_txtValueAssessment.SelectedValue);
            int CustomerLevel = SafeConvert.ToInt(DropDownList_txtCustomerLevel.SelectedValue);
            int RelBetweenGrade = SafeConvert.ToInt(DropDownList_txtRelBetweenGrade.SelectedValue);
            int Seedtime = SafeConvert.ToInt(DropDownList_txtSeedtime.SelectedValue);
            int SupClientInfoId = SafeConvert.ToInt(hidSupClientInfoId.Value);
            if (string.IsNullOrEmpty(TextBox_SupClientInfoId.Text))
            {
                SupClientInfoId = 0;
            }

            string BankAccount = this.txtBankAccount.Text;
            string MobilePhone = this.txtMobilePhone.Text;
            int CountriesRegions = SafeConvert.ToInt(DropDownListCountriesRegions.SelectedValue);
            string Phone = this.txtPhone.Text;
            string Fax = this.txtFax.Text;
            string Email = this.txtEmail.Text;
            string ZipCode = this.txtZipCode.Text;
            int Province = SafeConvert.ToInt(DropDownList_Province.SelectedValue);
            int City = SafeConvert.ToInt(DropDownList_City.SelectedValue);
            string County = this.txtCounty.Text;
            string Address = this.txtAddress.Text;
            string CompanyProfile = this.txtCompanyProfile.Text;
            string Remarks = this.txtRemarks.Text;
            string GeneralManager = this.txtGeneralManager.Text;
            string BusinessEntity = this.txtBusinessEntity.Text;
            string BusinessLicence = this.txtBusinessLicence.Text;

            int RefreshAccountId = CurrentAccountId;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.ClientInfo model = new XinYiOffice.Model.ClientInfo();
            model.Id = Id;
            model.ByAccountId = ByAccountId;
            model.Name = Name;
            model.Heat = Heat;
            model.ClientType = ClientType;
            model.Industry = Industry;
            model.Source = Source;
            model.Tag = Tag;
            model.BankDeposit = BankDeposit;
            model.TaxNo = TaxNo;
            model.IntNumber = IntNumber;
            model.Power = Power;
            model.QualityRating = QualityRating;
            model.ValueAssessment = ValueAssessment;
            model.CustomerLevel = CustomerLevel;
            model.RelBetweenGrade = RelBetweenGrade;
            model.Seedtime = Seedtime;
            model.SupClientInfoId = SupClientInfoId;
            model.BankAccount = BankAccount;
            model.MobilePhone = MobilePhone;
            model.CountriesRegions = CountriesRegions;
            model.Phone = Phone;
            model.Fax = Fax;
            model.Email = Email;
            model.ZipCode = ZipCode;
            model.Province = Province;
            model.City = City;
            model.County = County;
            model.Address = Address;
            model.CompanyProfile = CompanyProfile;
            model.Remarks = Remarks;
            model.GeneralManager = GeneralManager;
            model.BusinessEntity = BusinessEntity;
            model.BusinessLicence = BusinessLicence;
            model.RefreshAccountId = RefreshAccountId;
            model.RefreshTime = RefreshTime;

            model.CreateTime = SafeConvert.ToDateTime(HiddenField_CreateTime.Value,DateTime.Now);
            model.CreateAccountId = SafeConvert.ToInt(HiddenField_CreateAccountId.Value,CurrentAccountId);

            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.ClientInfo bll = new XinYiOffice.BLL.ClientInfo();
            bll.Update(model);

            //创建销售机会,只要是热度不是冷淡
            if (SafeConvert.ToInt(model.Heat) < 3)
            {
                //添加时不需要
            }

            //创建联系人,若联系人不为空时


            xytools.web_alert("保存成功！", "list.aspx");

        }
    }
}
