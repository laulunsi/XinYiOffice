﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:SalesOpportunities
	/// </summary>
	public partial class SalesOpportunities
	{
		public SalesOpportunities()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "SalesOpportunities"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from SalesOpportunities");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.SalesOpportunities model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SalesOpportunities(");
			strSql.Append("OppName,CustomerService,PersonalAccountId,MarketingPlanId,Source,Requirement,EstAmount,CurrencyNotes,Feasibility,PresentStage,NextStep,Sate,StageRemarks,OccTime,EstDate,RefreshAccountId,CreateAccountId,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@OppName,@CustomerService,@PersonalAccountId,@MarketingPlanId,@Source,@Requirement,@EstAmount,@CurrencyNotes,@Feasibility,@PresentStage,@NextStep,@Sate,@StageRemarks,@OccTime,@EstDate,@RefreshAccountId,@CreateAccountId,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@OppName", SqlDbType.VarChar,4000),
					new SqlParameter("@CustomerService", SqlDbType.Int,4),
					new SqlParameter("@PersonalAccountId", SqlDbType.Int,4),
					new SqlParameter("@MarketingPlanId", SqlDbType.Int,4),
					new SqlParameter("@Source", SqlDbType.Int,4),
					new SqlParameter("@Requirement", SqlDbType.VarChar,4000),
					new SqlParameter("@EstAmount", SqlDbType.Float,8),
					new SqlParameter("@CurrencyNotes", SqlDbType.VarChar,4000),
					new SqlParameter("@Feasibility", SqlDbType.Int,4),
					new SqlParameter("@PresentStage", SqlDbType.Int,4),
					new SqlParameter("@NextStep", SqlDbType.Int,4),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@StageRemarks", SqlDbType.VarChar,4000),
					new SqlParameter("@OccTime", SqlDbType.DateTime),
					new SqlParameter("@EstDate", SqlDbType.DateTime),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.OppName;
			parameters[1].Value = model.CustomerService;
			parameters[2].Value = model.PersonalAccountId;
			parameters[3].Value = model.MarketingPlanId;
			parameters[4].Value = model.Source;
			parameters[5].Value = model.Requirement;
			parameters[6].Value = model.EstAmount;
			parameters[7].Value = model.CurrencyNotes;
			parameters[8].Value = model.Feasibility;
			parameters[9].Value = model.PresentStage;
			parameters[10].Value = model.NextStep;
			parameters[11].Value = model.Sate;
			parameters[12].Value = model.StageRemarks;
			parameters[13].Value = model.OccTime;
			parameters[14].Value = model.EstDate;
			parameters[15].Value = model.RefreshAccountId;
			parameters[16].Value = model.CreateAccountId;
			parameters[17].Value = model.CreateTime;
			parameters[18].Value = model.RefreshTime;
			parameters[19].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.SalesOpportunities model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SalesOpportunities set ");
			strSql.Append("OppName=@OppName,");
			strSql.Append("CustomerService=@CustomerService,");
			strSql.Append("PersonalAccountId=@PersonalAccountId,");
			strSql.Append("MarketingPlanId=@MarketingPlanId,");
			strSql.Append("Source=@Source,");
			strSql.Append("Requirement=@Requirement,");
			strSql.Append("EstAmount=@EstAmount,");
			strSql.Append("CurrencyNotes=@CurrencyNotes,");
			strSql.Append("Feasibility=@Feasibility,");
			strSql.Append("PresentStage=@PresentStage,");
			strSql.Append("NextStep=@NextStep,");
			strSql.Append("Sate=@Sate,");
			strSql.Append("StageRemarks=@StageRemarks,");
			strSql.Append("OccTime=@OccTime,");
			strSql.Append("EstDate=@EstDate,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@OppName", SqlDbType.VarChar,4000),
					new SqlParameter("@CustomerService", SqlDbType.Int,4),
					new SqlParameter("@PersonalAccountId", SqlDbType.Int,4),
					new SqlParameter("@MarketingPlanId", SqlDbType.Int,4),
					new SqlParameter("@Source", SqlDbType.Int,4),
					new SqlParameter("@Requirement", SqlDbType.VarChar,4000),
					new SqlParameter("@EstAmount", SqlDbType.Float,8),
					new SqlParameter("@CurrencyNotes", SqlDbType.VarChar,4000),
					new SqlParameter("@Feasibility", SqlDbType.Int,4),
					new SqlParameter("@PresentStage", SqlDbType.Int,4),
					new SqlParameter("@NextStep", SqlDbType.Int,4),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@StageRemarks", SqlDbType.VarChar,4000),
					new SqlParameter("@OccTime", SqlDbType.DateTime),
					new SqlParameter("@EstDate", SqlDbType.DateTime),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.OppName;
			parameters[1].Value = model.CustomerService;
			parameters[2].Value = model.PersonalAccountId;
			parameters[3].Value = model.MarketingPlanId;
			parameters[4].Value = model.Source;
			parameters[5].Value = model.Requirement;
			parameters[6].Value = model.EstAmount;
			parameters[7].Value = model.CurrencyNotes;
			parameters[8].Value = model.Feasibility;
			parameters[9].Value = model.PresentStage;
			parameters[10].Value = model.NextStep;
			parameters[11].Value = model.Sate;
			parameters[12].Value = model.StageRemarks;
			parameters[13].Value = model.OccTime;
			parameters[14].Value = model.EstDate;
			parameters[15].Value = model.RefreshAccountId;
			parameters[16].Value = model.CreateAccountId;
			parameters[17].Value = model.CreateTime;
			parameters[18].Value = model.RefreshTime;
			parameters[19].Value = model.TenantId;
			parameters[20].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SalesOpportunities ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SalesOpportunities ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.SalesOpportunities GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,OppName,CustomerService,PersonalAccountId,MarketingPlanId,Source,Requirement,EstAmount,CurrencyNotes,Feasibility,PresentStage,NextStep,Sate,StageRemarks,OccTime,EstDate,RefreshAccountId,CreateAccountId,CreateTime,RefreshTime,TenantId from SalesOpportunities ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.SalesOpportunities model=new XinYiOffice.Model.SalesOpportunities();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["OppName"]!=null && ds.Tables[0].Rows[0]["OppName"].ToString()!="")
				{
					model.OppName=ds.Tables[0].Rows[0]["OppName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CustomerService"]!=null && ds.Tables[0].Rows[0]["CustomerService"].ToString()!="")
				{
					model.CustomerService=int.Parse(ds.Tables[0].Rows[0]["CustomerService"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PersonalAccountId"]!=null && ds.Tables[0].Rows[0]["PersonalAccountId"].ToString()!="")
				{
					model.PersonalAccountId=int.Parse(ds.Tables[0].Rows[0]["PersonalAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["MarketingPlanId"]!=null && ds.Tables[0].Rows[0]["MarketingPlanId"].ToString()!="")
				{
					model.MarketingPlanId=int.Parse(ds.Tables[0].Rows[0]["MarketingPlanId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Source"]!=null && ds.Tables[0].Rows[0]["Source"].ToString()!="")
				{
					model.Source=int.Parse(ds.Tables[0].Rows[0]["Source"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Requirement"]!=null && ds.Tables[0].Rows[0]["Requirement"].ToString()!="")
				{
					model.Requirement=ds.Tables[0].Rows[0]["Requirement"].ToString();
				}
				if(ds.Tables[0].Rows[0]["EstAmount"]!=null && ds.Tables[0].Rows[0]["EstAmount"].ToString()!="")
				{
					model.EstAmount=decimal.Parse(ds.Tables[0].Rows[0]["EstAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CurrencyNotes"]!=null && ds.Tables[0].Rows[0]["CurrencyNotes"].ToString()!="")
				{
					model.CurrencyNotes=ds.Tables[0].Rows[0]["CurrencyNotes"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Feasibility"]!=null && ds.Tables[0].Rows[0]["Feasibility"].ToString()!="")
				{
					model.Feasibility=int.Parse(ds.Tables[0].Rows[0]["Feasibility"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PresentStage"]!=null && ds.Tables[0].Rows[0]["PresentStage"].ToString()!="")
				{
					model.PresentStage=int.Parse(ds.Tables[0].Rows[0]["PresentStage"].ToString());
				}
				if(ds.Tables[0].Rows[0]["NextStep"]!=null && ds.Tables[0].Rows[0]["NextStep"].ToString()!="")
				{
					model.NextStep=int.Parse(ds.Tables[0].Rows[0]["NextStep"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Sate"]!=null && ds.Tables[0].Rows[0]["Sate"].ToString()!="")
				{
					model.Sate=int.Parse(ds.Tables[0].Rows[0]["Sate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["StageRemarks"]!=null && ds.Tables[0].Rows[0]["StageRemarks"].ToString()!="")
				{
					model.StageRemarks=ds.Tables[0].Rows[0]["StageRemarks"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OccTime"]!=null && ds.Tables[0].Rows[0]["OccTime"].ToString()!="")
				{
					model.OccTime=DateTime.Parse(ds.Tables[0].Rows[0]["OccTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EstDate"]!=null && ds.Tables[0].Rows[0]["EstDate"].ToString()!="")
				{
					model.EstDate=DateTime.Parse(ds.Tables[0].Rows[0]["EstDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,OppName,CustomerService,PersonalAccountId,MarketingPlanId,Source,Requirement,EstAmount,CurrencyNotes,Feasibility,PresentStage,NextStep,Sate,StageRemarks,OccTime,EstDate,RefreshAccountId,CreateAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM SalesOpportunities ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,OppName,CustomerService,PersonalAccountId,MarketingPlanId,Source,Requirement,EstAmount,CurrencyNotes,Feasibility,PresentStage,NextStep,Sate,StageRemarks,OccTime,EstDate,RefreshAccountId,CreateAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM SalesOpportunities ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM SalesOpportunities ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from SalesOpportunities T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "SalesOpportunities";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

