﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 搜索项所使用的数据源
	/// </summary>
	[Serializable]
	public partial class SearchDataSoure
	{
		public SearchDataSoure()
		{}
		#region Model
		private int _id;
		private int? _datasouretype;
		private string _datasoureconntion;
		private int? _datatype;
		private string _displayvalue;
		private string _truevalue;
		private int? _formtype;
		private string _formstyle;
		private string _formclass;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 数据源 1-数据库,2-文本文件,3-网址,4-Execl,5-xml文件
		/// </summary>
		public int? DataSoureType
		{
			set{ _datasouretype=value;}
			get{return _datasouretype;}
		}
		/// <summary>
		/// 数据源连接字符串 若为数据库则是sql语句,若是其他 则为绝对或相对路径
		/// </summary>
		public string DataSoureConntion
		{
			set{ _datasoureconntion=value;}
			get{return _datasoureconntion;}
		}
		/// <summary>
		/// 数据格式 字符串,xml,json,DataTable
		/// </summary>
		public int? DataType
		{
			set{ _datatype=value;}
			get{return _datatype;}
		}
		/// <summary>
		/// 代表显示的值 比如 查询出来的 Name 作为显示的下拉框
		/// </summary>
		public string DisplayValue
		{
			set{ _displayvalue=value;}
			get{return _displayvalue;}
		}
		/// <summary>
		/// 代表实际的值 而 Id 则为下拉框当中的值
		/// </summary>
		public string TrueValue
		{
			set{ _truevalue=value;}
			get{return _truevalue;}
		}
		/// <summary>
		/// 输入表单类型 1-文本,2-下拉,3-复选,4-单选,5-时间选框
		/// </summary>
		public int? FormType
		{
			set{ _formtype=value;}
			get{return _formtype;}
		}
		/// <summary>
		/// 表单样式 作为style=""中使用
		/// </summary>
		public string FormStyle
		{
			set{ _formstyle=value;}
			get{return _formstyle;}
		}
		/// <summary>
		/// 表单使用Class
		/// </summary>
		public string FormClass
		{
			set{ _formclass=value;}
			get{return _formclass;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

