﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:SearchConfigItem
	/// </summary>
	public partial class SearchConfigItem
	{
		public SearchConfigItem()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "SearchConfigItem"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from SearchConfigItem");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.SearchConfigItem model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SearchConfigItem(");
			strSql.Append("DisplayName,FieldName,SearchDataSoureId,Sort,ValueType,ValueRelChar,PrefixChar,PrefixRelChar,SuffixChar,SuffixRelChar,SearchConfigId,UseValueType,DefaultValue,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@DisplayName,@FieldName,@SearchDataSoureId,@Sort,@ValueType,@ValueRelChar,@PrefixChar,@PrefixRelChar,@SuffixChar,@SuffixRelChar,@SearchConfigId,@UseValueType,@DefaultValue,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@DisplayName", SqlDbType.VarChar,4000),
					new SqlParameter("@FieldName", SqlDbType.VarChar,4000),
					new SqlParameter("@SearchDataSoureId", SqlDbType.Int,4),
					new SqlParameter("@Sort", SqlDbType.Int,4),
					new SqlParameter("@ValueType", SqlDbType.Int,4),
					new SqlParameter("@ValueRelChar", SqlDbType.VarChar,4000),
					new SqlParameter("@PrefixChar", SqlDbType.VarChar,4000),
					new SqlParameter("@PrefixRelChar", SqlDbType.VarChar,4000),
					new SqlParameter("@SuffixChar", SqlDbType.VarChar,4000),
					new SqlParameter("@SuffixRelChar", SqlDbType.VarChar,4000),
					new SqlParameter("@SearchConfigId", SqlDbType.Int,4),
					new SqlParameter("@UseValueType", SqlDbType.Int,4),
					new SqlParameter("@DefaultValue", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.DisplayName;
			parameters[1].Value = model.FieldName;
			parameters[2].Value = model.SearchDataSoureId;
			parameters[3].Value = model.Sort;
			parameters[4].Value = model.ValueType;
			parameters[5].Value = model.ValueRelChar;
			parameters[6].Value = model.PrefixChar;
			parameters[7].Value = model.PrefixRelChar;
			parameters[8].Value = model.SuffixChar;
			parameters[9].Value = model.SuffixRelChar;
			parameters[10].Value = model.SearchConfigId;
			parameters[11].Value = model.UseValueType;
			parameters[12].Value = model.DefaultValue;
			parameters[13].Value = model.CreateAccountId;
			parameters[14].Value = model.RefreshAccountId;
			parameters[15].Value = model.CreateTime;
			parameters[16].Value = model.RefreshTime;
			parameters[17].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.SearchConfigItem model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SearchConfigItem set ");
			strSql.Append("DisplayName=@DisplayName,");
			strSql.Append("FieldName=@FieldName,");
			strSql.Append("SearchDataSoureId=@SearchDataSoureId,");
			strSql.Append("Sort=@Sort,");
			strSql.Append("ValueType=@ValueType,");
			strSql.Append("ValueRelChar=@ValueRelChar,");
			strSql.Append("PrefixChar=@PrefixChar,");
			strSql.Append("PrefixRelChar=@PrefixRelChar,");
			strSql.Append("SuffixChar=@SuffixChar,");
			strSql.Append("SuffixRelChar=@SuffixRelChar,");
			strSql.Append("SearchConfigId=@SearchConfigId,");
			strSql.Append("UseValueType=@UseValueType,");
			strSql.Append("DefaultValue=@DefaultValue,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@DisplayName", SqlDbType.VarChar,4000),
					new SqlParameter("@FieldName", SqlDbType.VarChar,4000),
					new SqlParameter("@SearchDataSoureId", SqlDbType.Int,4),
					new SqlParameter("@Sort", SqlDbType.Int,4),
					new SqlParameter("@ValueType", SqlDbType.Int,4),
					new SqlParameter("@ValueRelChar", SqlDbType.VarChar,4000),
					new SqlParameter("@PrefixChar", SqlDbType.VarChar,4000),
					new SqlParameter("@PrefixRelChar", SqlDbType.VarChar,4000),
					new SqlParameter("@SuffixChar", SqlDbType.VarChar,4000),
					new SqlParameter("@SuffixRelChar", SqlDbType.VarChar,4000),
					new SqlParameter("@SearchConfigId", SqlDbType.Int,4),
					new SqlParameter("@UseValueType", SqlDbType.Int,4),
					new SqlParameter("@DefaultValue", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.DisplayName;
			parameters[1].Value = model.FieldName;
			parameters[2].Value = model.SearchDataSoureId;
			parameters[3].Value = model.Sort;
			parameters[4].Value = model.ValueType;
			parameters[5].Value = model.ValueRelChar;
			parameters[6].Value = model.PrefixChar;
			parameters[7].Value = model.PrefixRelChar;
			parameters[8].Value = model.SuffixChar;
			parameters[9].Value = model.SuffixRelChar;
			parameters[10].Value = model.SearchConfigId;
			parameters[11].Value = model.UseValueType;
			parameters[12].Value = model.DefaultValue;
			parameters[13].Value = model.CreateAccountId;
			parameters[14].Value = model.RefreshAccountId;
			parameters[15].Value = model.CreateTime;
			parameters[16].Value = model.RefreshTime;
			parameters[17].Value = model.TenantId;
			parameters[18].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SearchConfigItem ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SearchConfigItem ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.SearchConfigItem GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,DisplayName,FieldName,SearchDataSoureId,Sort,ValueType,ValueRelChar,PrefixChar,PrefixRelChar,SuffixChar,SuffixRelChar,SearchConfigId,UseValueType,DefaultValue,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId from SearchConfigItem ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.SearchConfigItem model=new XinYiOffice.Model.SearchConfigItem();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DisplayName"]!=null && ds.Tables[0].Rows[0]["DisplayName"].ToString()!="")
				{
					model.DisplayName=ds.Tables[0].Rows[0]["DisplayName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FieldName"]!=null && ds.Tables[0].Rows[0]["FieldName"].ToString()!="")
				{
					model.FieldName=ds.Tables[0].Rows[0]["FieldName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SearchDataSoureId"]!=null && ds.Tables[0].Rows[0]["SearchDataSoureId"].ToString()!="")
				{
					model.SearchDataSoureId=int.Parse(ds.Tables[0].Rows[0]["SearchDataSoureId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Sort"]!=null && ds.Tables[0].Rows[0]["Sort"].ToString()!="")
				{
					model.Sort=int.Parse(ds.Tables[0].Rows[0]["Sort"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ValueType"]!=null && ds.Tables[0].Rows[0]["ValueType"].ToString()!="")
				{
					model.ValueType=int.Parse(ds.Tables[0].Rows[0]["ValueType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ValueRelChar"]!=null && ds.Tables[0].Rows[0]["ValueRelChar"].ToString()!="")
				{
					model.ValueRelChar=ds.Tables[0].Rows[0]["ValueRelChar"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PrefixChar"]!=null && ds.Tables[0].Rows[0]["PrefixChar"].ToString()!="")
				{
					model.PrefixChar=ds.Tables[0].Rows[0]["PrefixChar"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PrefixRelChar"]!=null && ds.Tables[0].Rows[0]["PrefixRelChar"].ToString()!="")
				{
					model.PrefixRelChar=ds.Tables[0].Rows[0]["PrefixRelChar"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SuffixChar"]!=null && ds.Tables[0].Rows[0]["SuffixChar"].ToString()!="")
				{
					model.SuffixChar=ds.Tables[0].Rows[0]["SuffixChar"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SuffixRelChar"]!=null && ds.Tables[0].Rows[0]["SuffixRelChar"].ToString()!="")
				{
					model.SuffixRelChar=ds.Tables[0].Rows[0]["SuffixRelChar"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SearchConfigId"]!=null && ds.Tables[0].Rows[0]["SearchConfigId"].ToString()!="")
				{
					model.SearchConfigId=int.Parse(ds.Tables[0].Rows[0]["SearchConfigId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UseValueType"]!=null && ds.Tables[0].Rows[0]["UseValueType"].ToString()!="")
				{
					model.UseValueType=int.Parse(ds.Tables[0].Rows[0]["UseValueType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DefaultValue"]!=null && ds.Tables[0].Rows[0]["DefaultValue"].ToString()!="")
				{
					model.DefaultValue=ds.Tables[0].Rows[0]["DefaultValue"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,DisplayName,FieldName,SearchDataSoureId,Sort,ValueType,ValueRelChar,PrefixChar,PrefixRelChar,SuffixChar,SuffixRelChar,SearchConfigId,UseValueType,DefaultValue,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM SearchConfigItem ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,DisplayName,FieldName,SearchDataSoureId,Sort,ValueType,ValueRelChar,PrefixChar,PrefixRelChar,SuffixChar,SuffixRelChar,SearchConfigId,UseValueType,DefaultValue,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM SearchConfigItem ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM SearchConfigItem ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from SearchConfigItem T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "SearchConfigItem";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

