﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.ProjectMembers
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.ProjectMembers bll = new XinYiOffice.BLL.ProjectMembers();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindData();
            }            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;
                }
            }
        }
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        public string GetAccountsHeadPortrait(string _head)
        {
            return AccountsServer.GetAccountsHeadPortrait(_head);
        }
        
        #region gridView
                        
        public void BindData()
        {
            #region
            //if (!Context.User.Identity.IsAuthenticated)
            //{
            //    return;
            //}
            //AccountsPrincipal user = new AccountsPrincipal(Context.User.Identity.Name);
            //if (user.HasPermissionID(PermId_Modify))
            //{
            //    gridView.Columns[6].Visible = true;
            //}
            //if (user.HasPermissionID(PermId_Delete))
            //{
            //    gridView.Columns[7].Visible = true;
            //}
            #endregion

            DataSet ds = new DataSet();
            StringBuilder strWhere = new StringBuilder(" 1=1 ");

            repProjectMembers.DataSource = ProjectInfoServer.GetProjectMembers(strWhere.ToString(),CurrentTenantId);
            repProjectMembers.DataBind();
        }
        #endregion





    }
}
