﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinYiOffice.Common.Web
{
    public class ParseHTML
    {
        /// <summary>
        /// 分页HTML[动态调用],调用前请确定pagecss.css文件
        /// </summary>
        /// <param name="url">链接地址</param>
        /// <param name="CurPage">当前页</param>
        /// <param name="PageCount">总页数</param>
        /// <returns></returns>
        public static string PageLabel(PageAttribute pFY, int PageCount)
        {
            string url = pFY.PageUrl;
            string PageShowFL = pFY.PageShowFL;
            string PageShowGo = pFY.PageShowGo;
            int CurPage = xytools.StringToInt(pFY.PagCur, 1);


            #region page分页HTML代码
            StringBuilder PageHtml = new StringBuilder();

            PageHtml.Append("<div align='center'><div class='viciao'>");

            #region //首页(选择性输出)
            if (PageShowFL == "1")
            {
                string fristnl = url.Replace("$page$", "1");
                PageHtml.Append("<a href='" + fristnl + "'><language>首页</language></a>");
            }
            #endregion

            #region //上一页判断
            if (CurPage != 1)
            {
                string nl = url.Replace("$page$", (CurPage - 1).ToString());

                PageHtml.Append("<a href='" + nl + "'> < <language>上一页</language></a>");
            }
            else
            {
                PageHtml.Append("<span class='disabled'> <  <language>上一页</language></span>");
            }
            #endregion

            int pv = 10;//底部显示的页码数量

            if (PageCount < pv)
            {
                pv = PageCount;
            }

            int firstpage = CurPage - (pv / 2);//开始序号

            if (CurPage > pv)
            {//当前页数大于页码个数
                int s = firstpage;
                for (int i = 1; i <= pv; i++)
                {
                    if (s > PageCount)
                    {
                        break;
                    }

                    if (CurPage == s)
                    {
                        //当前页
                        PageHtml.Append("<span class='current'>" + s + "</span>");
                    }
                    else
                    {
                        string nul = url.Replace("$page$", s.ToString());

                        PageHtml.Append("<a href='" + nul + "'>" + s + "</a>");
                    }

                    s++;
                }
            }
            else
            {
                for (int i = 1; i <= pv; i++)
                {

                    if (CurPage == i)
                    {
                        //当前页
                        PageHtml.Append("<span class='current'>" + i + "</span>");
                    }
                    else
                    {
                        string nul = url.Replace("$page$", i.ToString());

                        PageHtml.Append("<a href='" + nul + "'>" + i + "</a>");
                    }
                }
            }

            #region 下一页/尾页判断
            if (CurPage != PageCount)
            {
                string nul = url.Replace("$page$", (CurPage + 1).ToString());

                PageHtml.Append("<a href='" + nul + "'><language>下一页</language>  > </a>");
            }
            else
            {
                if (CurPage != 1)
                {
                    PageHtml.Append("<span class='current'><language>未页</language></span>");
                }

            }
            #endregion


            #region //尾页(选择性输出)
            if (PageShowFL == "1")
            {
                if (PageCount > 1)
                {
                    string nl = url.Replace("$page$", PageCount.ToString());

                    PageHtml.Append("<a href='" + nl + "'><language>尾页</language></a>");
                }
            }
            #endregion

            //跳转
            if (PageShowGo == "1")
            {
                string[] sPlit = xyStringClass.Split(url.ToString(), "$page$");//将页数两端相加 page=1 
                string s = string.Empty, e = string.Empty;
                if (sPlit != null)
                {
                    if (sPlit.Length == 1)
                    {
                        s = sPlit[0].ToString();
                    }
                    if (sPlit.Length >= 2)
                    {
                        s = sPlit[0].ToString();
                        e = sPlit[1].ToString();
                    }
                }

                StringBuilder sbjs = new StringBuilder();
                sbjs.Append("<script>");
                sbjs.Append("function xycjs_goskip()");
                sbjs.Append("{");
                sbjs.Append("var xycobj=document.getElementById('xycpscode');");
                sbjs.Append("window.location='" + s.ToString() + "'+xycobj.value+'" + e.ToString() + "';");
                sbjs.Append("}");
                sbjs.Append("</script>");
                sbjs.Append("<input class='pskip_code' type='text' value='' style='width:35px;vertical-align:middle;' id='xycpscode' />");
                sbjs.Append("<a href=\"#\" OnClick='xycjs_goskip();'>GO</a>");

                PageHtml.Append(sbjs.ToString());

            }

            PageHtml.Append("</div></div>");

            return PageHtml.ToString();//生成的分页代码 
            #endregion
        }
    }
}
