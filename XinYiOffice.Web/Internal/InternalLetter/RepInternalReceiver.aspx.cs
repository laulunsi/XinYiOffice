﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using XinYiOffice.Common;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Internal.InternalLetter
{
    public partial class RepInternalReceiver : BasicPage
    {
        public int InternalReceiverId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        protected void BindData()
        {
            #region //原信数据
            InternalReceiverId=SafeConvert.ToInt(xytools.url_get("intrid"));
            HiddenField_InternalLetterId.Value = InternalReceiverId.ToString();

            Model.InternalReceiver ir=new Model.InternalReceiver();
            Model.InternalLetter il=null;//内部信

            ir=new BLL.InternalReceiver().GetModel(InternalReceiverId);
            if(ir!=null)
            {
                il=new BLL.InternalLetter().GetModel(SafeConvert.ToInt(ir.InternalLetterId));
                txtTitle.Text = string.Format("RE:{0}",il.Title);
                txtCon.Text = string.Format("------------------------------------------   \n {0}",il.Con);
            }
            #endregion


            DataTable dtAccountList = AccountsServer.GetAccountAllList(CurrentTenantId);
            foreach (DataRow dr in dtAccountList.Rows)
            {
                ListItem li = new ListItem(string.Format("{0}({1})", SafeConvert.ToString(dr["FullName"]), SafeConvert.ToString(dr["AccountName"])), SafeConvert.ToString(dr["Id"]));
                if (SafeConvert.ToInt(dr["Id"])==il.AuthorAccountId)
                {
                    li.Selected = true;
                    HiddenField_InternalReceiver.Value = SafeConvert.ToString(dr["Id"]);
                } 
                DropDownList_Account.Items.Add(li);
            }


        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {
                Model.InternalLetter il = new Model.InternalLetter();
                il.AuthorAccountId = CurrentAccountId;
                il.Con = txtCon.Text;
                il.CreateAccountId = CurrentAccountId;
                il.CreateTime = DateTime.Now;
                il.ImpDegree = SafeConvert.ToInt(RadioButtonList1.SelectedValue);
                il.InternalLetterId = SafeConvert.ToInt(HiddenField_InternalLetterId.Value);
                il.IsDelete = 0;
                il.RefreshAccountId = CurrentAccountId;
                il.RefreshTime = DateTime.Now;
                il.Remarks = string.Empty;
                il.Title = txtTitle.Text;
                il.Id = new BLL.InternalLetter().Add(il);

                #region //添加接受表
                Model.InternalReceiver ir = new Model.InternalReceiver();
                ir.CheckDate = null;
                ir.CreateAccountId = CurrentAccountId;
                ir.CreateTime = DateTime.Now;
                ir.InternalLetterId = il.Id;
                ir.IsDelete = 0;
                ir.IsReply = 0;
                ir.IsView = 0;
                ir.LetterPosition = 1;
                ir.PositionFoler = string.Empty;
                ir.RecipientAccountId = SafeConvert.ToInt(HiddenField_InternalReceiver.Value);
                ir.RecipientType = 1;
                ir.RefreshAccountId = CurrentAccountId;
                ir.RefreshTime = DateTime.Now;

                try { new BLL.InternalReceiver().Add(ir); }
                catch (Exception ex) { EasyLog.WriteLog(ex); }
                #endregion

                xytools.web_alert("已发送", "HairList.aspx");
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
                
            }
        }
    }
}