﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 部门
	/// </summary>
	[Serializable]
	public partial class Department
	{
		public Department()
		{}
		#region Model
		private int _id;
		private string _departmentname;
		private string _departmentcon;
		private int? _institutionid;
		private int? _torchbeareraccountid;
		private string _phonecode;
		private string _phonecode2;
		private string _fax;
		private string _zipcode;
		private string _address;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 部门名称
		/// </summary>
		public string DepartmentName
		{
			set{ _departmentname=value;}
			get{return _departmentname;}
		}
		/// <summary>
		/// 部门介绍
		/// </summary>
		public string DepartmentCon
		{
			set{ _departmentcon=value;}
			get{return _departmentcon;}
		}
		/// <summary>
		/// 所属机构
		/// </summary>
		public int? InstitutionId
		{
			set{ _institutionid=value;}
			get{return _institutionid;}
		}
		/// <summary>
		/// 负责人
		/// </summary>
		public int? TorchbearerAccountId
		{
			set{ _torchbeareraccountid=value;}
			get{return _torchbeareraccountid;}
		}
		/// <summary>
		/// 电话号码
		/// </summary>
		public string PhoneCode
		{
			set{ _phonecode=value;}
			get{return _phonecode;}
		}
		/// <summary>
		/// 电话号码
		/// </summary>
		public string PhoneCode2
		{
			set{ _phonecode2=value;}
			get{return _phonecode2;}
		}
		/// <summary>
		/// 传真
		/// </summary>
		public string Fax
		{
			set{ _fax=value;}
			get{return _fax;}
		}
		/// <summary>
		/// 邮编
		/// </summary>
		public string ZipCode
		{
			set{ _zipcode=value;}
			get{return _zipcode;}
		}
		/// <summary>
		/// 地址
		/// </summary>
		public string Address
		{
			set{ _address=value;}
			get{return _address;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

