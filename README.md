![桌面](http://www.xinyicms.com/Template/img/xinyioffice/1.jpg "桌面")

![帐号管理界面](http://www.xinyicms.com/Template/img/xinyioffice/5.jpg "帐号管理界面")

![权限管理界面](http://www.xinyicms.com/Template/img/xinyioffice/6.jpg "权限管理界面")

![网盘](http://www.xinyicms.com/Template/img/xinyioffice/2.jpg "网盘-文件管理")

**功能介绍**

    基于微软NET架构的云在线办公系统，支持多公司架构，SaaS软件按模式。
非常适合公司或团队使用，可以作为通用OA的基础模式，代码完整，功能几乎无错，正在使用运行。
若需定制开发，请联系server@xinyicms.com



**菜单功能**

    1、桌面：邮件、信息、签到、快捷方式，最近项目

    2、信息：写信、收件箱、发件箱

    3、网盘：类似网盘模式每个账户的共享。

    4、客户：客户管理、联系人管理、营销计划、商业机会、客户跟进

    5、项目：项目管理、项目类型管理、项目跟踪、bug提交、项目组人员管理

    6、日程：可以添加个人或几个人的日程。

    7、财务：应收、应付。工资管理，银行帐号。费用报销。

    8、组织：机构分公司、部门管理、员工管理

    9、系统：日志、帐号管理、全局字典、搜索设置、权限、菜单管理


**官网**
http://www.xinyioffice.com/


**QQ群**

143771644

入群可查看演示帐号和地址