﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.IO;
using XinYiOffice.Config;

namespace XinYiOffice.Common
{
    /// <summary>
    /// Web应用程序上传方法
    /// </summary>
    public class WebUpLoad
    {
        public string serverPath = string.Empty;//服务器端需要的web地址即: /user_file/article/2010/10/2/23424234.jpg
        public HttpContext httpcontext;

        public WebUpLoad()
        {
            if (HttpContext.Current != null)
                httpcontext = HttpContext.Current;
        }

        /// <summary>
        /// 删除通过此工具类上传的文件,一般文件路径符合如 /user_file/article/20110506201154/201105124516.gif 格式
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static bool DelUpLoadFile(string filepath)
        {
            WebUpLoad wu = new WebUpLoad();

            bool t = false;

            if (!string.IsNullOrEmpty(filepath))
            {
                string new_serverfilepath = wu.httpcontext.Server.MapPath("~" + filepath);
                if (File.Exists(new_serverfilepath))
                {
                    System.IO.File.Delete(new_serverfilepath);
                    t = true;
                }
            }

            return t;
        }

        /// <summary>
        /// 根据当前页的file元素的id直接提交
        /// </summary>
        /// <param name="filesEleName"></param>
        public static string PostActionUpLoad(string filesEleName)
        {
            return PostActionUpLoad(HttpContext.Current.Request.Files[filesEleName],string.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filesEleName">上传控件的id</param>
        /// <param name="To">将文件上传至 服务器的文件夹 格式如 my 或 my/ProductBarter/ </param>
        /// <returns></returns>
        public static string PostActionUpLoad(string filesEleName,string To)
        {
            return PostActionUpLoad(HttpContext.Current.Request.Files[filesEleName], To);
        }

        public static string PostActionUpLoad(HttpPostedFile PostedFile, string To)
        {
            WebUpLoad wu = new WebUpLoad();
            HttpPostedFile postedFile = PostedFile;
            string WebResPath = string.Empty;

            if (null != postedFile&&postedFile.ContentLength>1)
            {
                //如~/upload/
                wu.serverPath = StaticSettings.ResFilePath;

                if (!string.IsNullOrEmpty(To))
                {
                    wu.serverPath += To + "/";
                }

                //自增文件夹 结果如 ~/upload/2011-5-21/
                string dyDirPath = wu.serverPath + DateTime.Now.ToString(@"yyyyMMdd") + "/";

                //自增后服务器端路径
                string serdyDirPath = wu.httpcontext.Server.MapPath(dyDirPath);

                //随机名 如 saf34as12dfz.jpg
                string dyFileName = string.Format("{0}{1}"
                , System.IO.Path.GetRandomFileName().Replace(".", "")
                , System.IO.Path.GetExtension(postedFile.FileName)
                );

                //上传至服务器的最新路径
                string newServFullPath = System.IO.Path.Combine(serdyDirPath, dyFileName);

                //文件浏览时web页引用路径
                WebResPath = (dyDirPath + dyFileName).Replace("~", "");

                //为确保成功,创建路径
                if (!System.IO.Directory.Exists(wu.httpcontext.Server.MapPath(dyDirPath)))
                {
                    System.IO.Directory.CreateDirectory(wu.httpcontext.Server.MapPath(dyDirPath));
                }

                postedFile.SaveAs(newServFullPath);
                
            }
            return WebResPath;

        }

        /// <summary>
        /// 指定一个路径压缩成浓缩图并返回浓缩图地址
        /// </summary>
        /// <param name="originalImagePath"></param>
        /// <param name="thumbnailPath"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="mode"></param>
        /// <param name="isdelorig">是否在服务器上删除原图</param>
        public static void Thumbnail(string originalImagePath, ref string thumbnailPath, int width, int height, string mode,bool isdelorig)
        {
            if (string.IsNullOrEmpty(thumbnailPath))
            {
                string dir=System.IO.Path.GetDirectoryName(originalImagePath);
                string tname=System.IO.Path.GetFileNameWithoutExtension(originalImagePath)+"_"+width.ToString()+"_"+height.ToString();
                thumbnailPath = string.Format("{0}/{1}{2}",
                    dir,
                    tname,
                    System.IO.Path.GetExtension(originalImagePath)
                    ).Replace(@"\","/");
            }

            WebUpLoad wu = new WebUpLoad();
            string server_oimg=wu.httpcontext.Server.MapPath("~"+originalImagePath);
            string server_timg=wu.httpcontext.Server.MapPath("~"+thumbnailPath);

            ImageTools.MakeThumbnail(server_oimg, server_timg,width,height,mode);

            if (isdelorig)
            {
                DelUpLoadFile(originalImagePath);
            }
        }
    }
}
