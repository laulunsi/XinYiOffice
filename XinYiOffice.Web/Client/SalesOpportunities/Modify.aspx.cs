﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Client.SalesOpportunities
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!ValidatePermission("SALESOPPORTUNITIES_LIST_EDIT"))
            {
                NoPermissionPage();
            }


            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            SetDropDownList("Source", ref DropDownList_Source);
            SetDropDownList("PresentStage", ref DropDownList_PresentStage);
            SetDropDownList("PresentStage", ref DropDownList_NextStep);
            SetDropDownList("Sate", ref DropDownList_Sate);


            XinYiOffice.BLL.SalesOpportunities bll = new XinYiOffice.BLL.SalesOpportunities();
            XinYiOffice.Model.SalesOpportunities model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtOppName.Text = model.OppName;

            this.txtCustomerService.Text = new BLL.ClientInfo().GetModel(SafeConvert.ToInt(model.CustomerService)).Name;
            HiddenField_CustomerService.Value = model.CustomerService.ToString();

            this.txtPersonalAccountId.Text = AccountsServer.GetFullNameByAccountId(SafeConvert.ToInt(model.PersonalAccountId));
            HiddenField_PersonalAccountId.Value = model.PersonalAccountId.ToString();

            this.txtMarketingPlanId.Text = model.MarketingPlanId.ToString();
            hidtxtMarketingPlanId.Value = model.MarketingPlanId.ToString();

            DropDownList_Source.SelectedValue = model.Source.ToString();
            this.txtRequirement.Text = model.Requirement;
            this.txtEstAmount.Text = model.EstAmount.ToString();
            this.txtCurrencyNotes.Text = model.CurrencyNotes;
            this.txtFeasibility.Text = model.Feasibility.ToString();
            DropDownList_PresentStage.SelectedValue = model.PresentStage.ToString();
            DropDownList_NextStep.SelectedValue = model.NextStep.ToString();
            DropDownList_Sate.SelectedValue = model.Sate.ToString();
            this.txtStageRemarks.Text = model.StageRemarks;
            this.txtOccTime.Text = model.OccTime.ToString();
            this.txtEstDate.Text = model.EstDate.ToString();

            HiddenField_CreateTime.Value = model.CreateTime.ToString();
            HiddenField_RefreshTime.Value = model.RefreshTime.ToString();

            HiddenField_CreateAccountId.Value = model.RefreshTime.ToString();

        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.SalesOpportunitiesDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void btnSave_Click1(object sender, EventArgs e)
        {
            try
            { 
                 int Id = SafeConvert.ToInt(this.lblId.Text);
            string OppName = this.txtOppName.Text;
            int CustomerService = SafeConvert.ToInt(this.HiddenField_CustomerService.Value);
            int PersonalAccountId = SafeConvert.ToInt(this.HiddenField_PersonalAccountId.Value);
            int MarketingPlanId = SafeConvert.ToInt(this.hidtxtMarketingPlanId.Value);
            int Source = SafeConvert.ToInt(DropDownList_Sate.SelectedValue);
            string Requirement = this.txtRequirement.Text;
            decimal EstAmount = decimal.Parse(this.txtEstAmount.Text);
            string CurrencyNotes = this.txtCurrencyNotes.Text;
            int Feasibility = SafeConvert.ToInt(this.txtFeasibility.Text);
            int PresentStage = SafeConvert.ToInt(DropDownList_PresentStage.SelectedValue);
            int NextStep = SafeConvert.ToInt(DropDownList_NextStep.SelectedValue);
            int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue);
            string StageRemarks = this.txtStageRemarks.Text;
            DateTime OccTime = DateTime.Parse(this.txtOccTime.Text);
            DateTime EstDate = DateTime.Parse(this.txtEstDate.Text);

            XinYiOffice.Model.SalesOpportunities model = new XinYiOffice.Model.SalesOpportunities();
            model.Id = Id;
            model.OppName = OppName;
            model.CustomerService = CustomerService;
            model.PersonalAccountId = PersonalAccountId;
            model.MarketingPlanId = MarketingPlanId;
            model.Source = Source;
            model.Requirement = Requirement;
            model.EstAmount = EstAmount;
            model.CurrencyNotes = CurrencyNotes;
            model.Feasibility = Feasibility;
            model.PresentStage = PresentStage;
            model.NextStep = NextStep;
            model.Sate = Sate;
            model.StageRemarks = StageRemarks;
            model.OccTime = OccTime;
            model.EstDate = EstDate;
            model.RefreshAccountId = CurrentAccountId;
            model.CreateAccountId = SafeConvert.ToInt(HiddenField_CreateAccountId.Value);
            model.CreateTime = SafeConvert.ToDateTime(HiddenField_CreateTime.Value);
            model.RefreshTime = DateTime.Now;
            model.TenantId = CurrentTenantId;
            XinYiOffice.BLL.SalesOpportunities bll = new XinYiOffice.BLL.SalesOpportunities();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");
            }
            catch( Exception ex)
            {
                EasyLog.WriteLog(ex);
                xytools.web_alert("服务请忙,请稍候再试！");
            }
            finally
            {
            }
        }
    }
}
