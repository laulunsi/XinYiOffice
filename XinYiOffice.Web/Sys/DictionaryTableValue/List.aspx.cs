﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.DictionaryTableValue
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.DictionaryTableValue bll = new XinYiOffice.BLL.DictionaryTableValue();

        public string strDictionaryTableId = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                
                BindData();
            }
        }
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }
        

        #region gridView
                        
        public void BindData()
        {
            DataSet ds = new DataSet();
            StringBuilder strWhere = new StringBuilder();

             strDictionaryTableId =xytools.url_get("DictionaryTableId");
            if (!string.IsNullOrEmpty(strDictionaryTableId))
            {
                strWhere.AppendFormat(" DictionaryTableId={0} and TenantId={1} ", SafeConvert.ToInt(strDictionaryTableId, 0),CurrentTenantId);
                //hid_DictionaryTableId.Value= strDictionaryTableId;

            }

            ds = bll.GetList(strWhere.ToString());
            repDictionaryTableValue.DataSource = ds;
            repDictionaryTableValue.DataBind();

        }
        #endregion

    }
}
