﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Project.IssueTracker.Add" Title="增加页" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
<script src="/js/ui/jquery.ui.core.js"></script>
	<script src="/js/ui/jquery.ui.widget.js"></script>
	<script src="/js/ui/jquery.ui.position.js"></script>
	<script src="/js/ui/jquery.ui.autocomplete.js"></script>
	<link rel="stylesheet" href="/js/themes/base/jquery.ui.all.css">
    <link rel="stylesheet" href="/js/themes/base/jquery.ui.autocomplete.css">

<script>
    $(function () {

        var txtAuthorsAccountId = $("#<%=txtAuthorsAccountId.ClientID%>");
        var HiddenField_AuthorsAccountId = $("<%=HiddenField_AuthorsAccountId.ClientID%>");

        var txtAssignerAccountId = $("#<%=txtAssignerAccountId.ClientID%>");
        var HiddenField_txtAssignerAccountId = $("#<%=HiddenField_txtAssignerAccountId.ClientID%>");

        var url_getwork = "/Call/Ajax.aspx?action=getofficeworkeraccount&sj=" + Math.random();

        //发起人
        txtAuthorsAccountId.autocomplete({
            source: url_getwork,
            select: function (event, ui) {
                //alert(ui.item.value);
                txtAuthorsAccountId.val(ui.item.label);
                HiddenField_AuthorsAccountId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                txtAuthorsAccountId.val(ui.item.label);
                return false;
            }
        });


        //解决人
        txtAssignerAccountId.autocomplete({
            source: url_getwork,
            select: function (event, ui) {
                //alert(ui.item.value);
                txtAssignerAccountId.val(ui.item.label);
                HiddenField_txtAssignerAccountId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                txtAssignerAccountId.val(ui.item.label);
                return false;
            }
        });


    });
</script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form  id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>添加议题追踪</h4></div>
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属项目
	：</td>
	<td height="25" width="*" align="left">
		
	    <asp:Label ID="Label_ProjectInfoName" runat="server" Text="Label"></asp:Label>
		
	    <asp:HiddenField ID="HiddenField_ProjectInfoId" runat="server" />
		
	</td></tr>
	<tr>
	  <td height="25" align="right" class="table_left">追踪类型：</td>
	  <td height="25" align="left">
          <asp:Label ID="Label_TypeName" runat="server" Text="Label"></asp:Label>
          <asp:HiddenField ID="HiddenField_TypeId" runat="server" />
        </td>
	  </tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		标题
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTitle" runat="server" Width="582px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		摘要
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTabloid" runat="server" Width="582px" Height="80px" 
            TextMode="MultiLine"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		内容明细
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtItem" runat="server" Width="582px" Height="119px" 
            TextMode="MultiLine"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		优先级
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_Priority" runat="server">
            <asp:ListItem>非常高</asp:ListItem>
            <asp:ListItem>高</asp:ListItem>
            <asp:ListItem>一般</asp:ListItem>
            <asp:ListItem>低</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	  <td height="25" width="30%" align="right" class="table_left">
	    状态
	    ：</td>
	  <td height="25" width="*" align="left">
	      <asp:DropDownList ID="DropDownList_Sate" runat="server">
          </asp:DropDownList>
	    </td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		版本
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtVersion" runat="server" Width="200px">v1.0</asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		URL
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtUrl" runat="server" Width="582px">http://</asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		组成原因
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtConstitute" runat="server" Width="582px" Height="119px" 
            TextMode="MultiLine"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		严重程度
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_Serious" runat="server">
            <asp:ListItem>影响全局</asp:ListItem>
            <asp:ListItem>影响多数</asp:ListItem>
            <asp:ListItem>影响自身</asp:ListItem>
            <asp:ListItem>未知影响</asp:ListItem>
            <asp:ListItem>无影响</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		发起人
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtAuthorsAccountId" runat="server" Width="200px"></asp:TextBox>
		(默认为当前用户) 
        <asp:HiddenField ID="HiddenField_AuthorsAccountId" runat="server" />
        </td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		解决人
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtAssignerAccountId" runat="server" Width="200px"></asp:TextBox>
		(默认为当前用户) 
        <asp:HiddenField ID="HiddenField_txtAssignerAccountId" runat="server" />
        </td></tr>
	</table>
    </div>

     <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>
    </form>
</asp:Content>

