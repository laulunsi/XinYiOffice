﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 客户信息扩展配置表
	/// </summary>
	[Serializable]
	public partial class ClientInfoExpand
	{
		public ClientInfoExpand()
		{}
		#region Model
		private int _id;
		private string _fieldname;
		private int? _formtype;
		private string _initdata;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 扩展字段名
		/// </summary>
		public string FieldName
		{
			set{ _fieldname=value;}
			get{return _fieldname;}
		}
		/// <summary>
		/// 扩展的表单类型 1-文本，2-下拉，3-复选，4-单选，5-富文本
		/// </summary>
		public int? FormType
		{
			set{ _formtype=value;}
			get{return _formtype;}
		}
		/// <summary>
		/// 表单初始化数据 json
		/// </summary>
		public string InitData
		{
			set{ _initdata=value;}
			get{return _initdata;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

