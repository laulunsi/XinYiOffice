﻿<%@ Page Language="C#"  MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Sys.SearchConfigItem.Show" Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">
<div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>搜索项信息</h4></div>


   <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		显示字段名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDisplayName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		数据库字段名
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblFieldName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		表单数据来源
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSearchDataSoureId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		搜索项排序
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSort" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		值类型 string为='1'
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblValueType" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		与值的关系
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblValueRelChar" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		前置字符
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPrefixChar" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		前置关系
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPrefixRelChar" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		后置字符
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSuffixChar" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		后置关系
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSuffixRelChar" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属的搜索配置
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSearchConfigId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		采用值的txt还是value 
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblUseValueType" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		显示的默认值
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDefaultValue" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		添加者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		更新者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRefreshAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		创建时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateTime" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		更新时间 ：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRefreshTime" runat="server"></asp:Label>
	</td></tr>
</table>

</div>

            </form>


</asp:Content>



