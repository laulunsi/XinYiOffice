﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using XinYiOffice.Common;

namespace XinYiOffice.Basic
{
    public class ResAttachmentServer
    {
        /// <summary>
        /// 上传当前请求的文件域
        /// </summary>
        /// <param name="hfc"></param>
        /// <param name="tableName"></param>
        /// <param name="keyId"></param>
        public static void UpLoadRes(HttpFileCollection hfc, string tableName, string keyId, Model.Accounts acc)
        {
            for (int i = 0; i < hfc.Count; i++)
            {
                HttpPostedFile hpf = (HttpPostedFile)hfc[i] as HttpPostedFile;
                if (string.IsNullOrEmpty(hpf.FileName))
                {
                    continue;
                }

                string an = Common.WebUpLoad.PostActionUpLoad(hpf,tableName);

                Model.ResAttachment ra = new Model.ResAttachment();
                ra.Annex = an;
                ra.Con = string.Empty;
                ra.CreateAccountId = acc.Id;
                ra.CreateTime = DateTime.Now;
                ra.KeyTable = tableName;
                ra.KeyId = SafeConvert.ToInt(keyId);
                ra.ResFolderId = 0;
                ra.Title = System.IO.Path.GetFileName(an);
                ra.Views = 1;
                try
                {
                    new BLL.ResAttachment().Add(ra);
                }
                catch (Exception ex)
                {
                    EasyLog.WriteLog(ex);
                }

            }
           
        }
    }
}
