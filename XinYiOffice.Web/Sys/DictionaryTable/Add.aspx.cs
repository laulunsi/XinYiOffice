﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Sys.DictionaryTable
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("DICTIONARYTABLE_LIST_ADD"))
            {
                base.NoPermissionPage();
            }

            if(!IsPostBack)
            {
                InitData();
            }

        }

        protected void InitData()
        {
            try
            {
                DropDownList_TableName.DataSource = AppDataCacheServer.GetAllTableName();
                DropDownList_TableName.DataTextField = "name";
                DropDownList_TableName.DataValueField = "name";
                DropDownList_TableName.DataBind();

                DropDownList_TableName.Items.Insert(0, new ListItem("选择一个系统表名", "0"));
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
            }
            finally
            { 
            }

        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string Name = this.txtName.Text;
            string Con = this.txtCon.Text;
            string TableName = SafeConvert.ToString(DropDownList_TableName.SelectedValue);
            string FieldName = SafeConvert.ToString(hid_FieldName.Value);
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now; ;

            XinYiOffice.Model.DictionaryTable model = new XinYiOffice.Model.DictionaryTable();
            model.Name = Name;
            model.Con = Con;
            model.TableName = TableName;
            model.FieldName = FieldName;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.DictionaryTable bll = new XinYiOffice.BLL.DictionaryTable();
            bll.Add(model);
            xytools.web_alert("保存成功！", "add.aspx");
        }
    }
}
