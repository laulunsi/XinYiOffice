﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="QianDaoList.aspx.cs" Inherits="XinYiOffice.Web.Ins.OfficeWorker.QianDaoList" %>
<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/pagecss.css">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2">
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear">
            <%if (ValidatePermission("CLIENTINFO_LIST"))
              { %>
            <a href="javascript:;" id="search_a"  class="sj">条件查询</a>
            <%} %>
            <%if (ValidatePermission("CLIENTINFO_LIST_ADD"))
              { %>
            <a href="add.aspx">添加</a>
            <%} %>
            <%if (ValidatePermission("CLIENTINFO_LIST_EDIT"))
              { %>
            <a href="javascript:;" class="edit">修改</a>
            <%} %>
             <%if (ValidatePermission("CLIENTINFO_LIST_DEL"))
              { %>
            <a href="javascript:;" class="tdel">删除</a>
            <%} %>

            </div>
                <div class="del clear"></div>
            </div>
          
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">
                <uc1:searchapp ID="SearchApp1" runat="server" TableName="vClientInfo"/>

                </div>
                <div class="clear btn_box btn_box_p">
<a href="javascript:;" class="save btn_search">查询</a>
<a href="javascript:;" class="cancel res_search">取消</a>
                </div>
        	</div>
    
        </div>
        
   
   <div class="marketing_table">
        <table cellpadding="0" cellspacing="0" id="_table">

                <asp:Repeater ID="repQianDao" runat="server" >
                <HeaderTemplate>
                <tr><th><input type="checkbox" id="cb_all"></th><th>姓名</th><th>账户名</th><th>签到时间</th><th>签退时间</th><th>小时数</th></tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                <td><%#DataBinder.Eval(Container.DataItem, "FullName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "AccountName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "StartTime")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "EndTime")%></td>
                <td><%#GetDateTime(DataBinder.Eval(Container.DataItem, "StartTime").ToString(), DataBinder.Eval(Container.DataItem, "EndTime").ToString())%></td>
                </tr>    
                </ItemTemplate>
                </asp:Repeater>

                

            </table>
        </div>
        

</div>
<%=pagehtml%>
</form>
<script>
    $('#_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cp2" runat="server">
</asp:Content>
