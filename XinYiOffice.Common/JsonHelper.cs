﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Newtonsoft.Json;

using System.IO;

namespace XinYiOffice.Common
{
    public class JsonHelper
    {
        public static string DataTable2Json(DataTable dt)
        {
            var sb = new StringBuilder();

            JsonWriter writer = new JsonTextWriter(new StringWriter(sb));
            writer.Formatting = Formatting.Indented;
            writer.WriteStartObject();
            writer.WritePropertyName("TableName");
            writer.WriteValue(dt.TableName);
            writer.WritePropertyName("Column");
            writer.WriteStartArray();
            foreach (DataColumn dc in dt.Columns)
            {
                writer.WriteValue(dc.ColumnName);
            }

            writer.WriteEndArray();
            writer.WritePropertyName("Data");
            writer.WriteRawValue(JsonConvert.SerializeObject(dt, Formatting.Indented));
            writer.WriteEndObject();
            return sb.ToString();

        }

        public static string DataTableToJsonClear(DataTable dt)
        {
            var sb = new StringBuilder();

            //JsonWriter writer = new JsonTextWriter(new StringWriter(sb));
            //writer.Formatting = Formatting.Indented;
            //writer.WriteStartObject();



            //writer.WritePropertyName("TableName");
            //writer.WriteValue(dt.TableName);
            //writer.WritePropertyName("Column");
            //writer.WriteStartArray();
            //foreach (DataColumn dc in dt.Columns)
            //{
            //    writer.WriteValue(dc.ColumnName);
            //}

            //writer.WriteEndArray();
            //writer.WritePropertyName("Data");



            //writer.WriteRawValue(JsonConvert.SerializeObject(dt, Formatting.Indented));
           // writer.WriteEndObject();

            sb.Append(JsonConvert.SerializeObject(dt, Formatting.Indented));
            return sb.ToString();
        }


    }
}
