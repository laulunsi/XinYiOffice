﻿<%@ Page Title="菜单管理 此表作为" MasterPageFile="~/BasicContent.Master" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Sys.MenuTree.List" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="/css/jquery.treetable.css" />

<script src="/js/vendor/jquery-ui.js"></script>
<script src="/js/jquery.treetable.js"></script>

</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<table id="example-advanced">
        <caption>
          <a href="#" onClick="jQuery('#example-advanced').treetable('expandAll'); return false;">展开</a>
          <a href="#" onClick="jQuery('#example-advanced').treetable('collapseAll'); return false;">收起</a>
        </caption>
        <thead>
        <tr>
          <th>菜单名称</th>
                <th>是否显示</th>
                <th>排序</th>
          </tr>
        </thead>
        <tbody runat="server" id="tbody_cell">

        </tbody>
  </table>
  
  

<script>
          $("#example-advanced").treetable({ expandable: true });

          // Highlight selected row
          $("#example-advanced tbody tr").mousedown(function () {
              $("tr.selected").removeClass("selected");
              $(this).addClass("selected");
          });

          // Drag & Drop Example Code
          $("#example-advanced .file, #example-advanced .folder").draggable({
              helper: "clone",
              opacity: .75,
              refreshPositions: true, // Performance?
              revert: "invalid",
              revertDuration: 300,
              scroll: true
          });

          $("#example-advanced .folder").each(function () {
              $(this).parents("tr").droppable({
                  accept: ".file, .folder",
                  drop: function (e, ui) {
                      var droppedEl = ui.draggable.parents("tr");
                      $("#example-advanced").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
                  },
                  hoverClass: "accept",
                  over: function (e, ui) {
                      var droppedEl = ui.draggable.parents("tr");
                      if (this != droppedEl[0] && !$(this).is(".expanded")) {
                          $("#example-advanced").treetable("expandNode", $(this).data("ttId"));
                      }
                  }
              });
          });

          jQuery('#example-advanced').treetable('expandAll');
                
    </script>

</asp:Content>
