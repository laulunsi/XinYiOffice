﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 客户跟踪表 对某个销售机会展开的客户跟踪
	/// </summary>
	[Serializable]
	public partial class ClientTracking
	{
		public ClientTracking()
		{}
		#region Model
		private int _id;
		private int? _clientheat;
		private int? _salesopportunitiesid;
		private int? _salesopportunitiessate;
		private string _backnotes;
		private int? _sate;
		private int? _isfollowup;
		private string _closenotes;
		private int? _returnaccountid;
		private DateTime? _actualtime;
		private DateTime? _plantime;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 当前客户热度
		/// </summary>
		public int? ClientHeat
		{
			set{ _clientheat=value;}
			get{return _clientheat;}
		}
		/// <summary>
		/// 销售机会Id
		/// </summary>
		public int? SalesOpportunitiesId
		{
			set{ _salesopportunitiesid=value;}
			get{return _salesopportunitiesid;}
		}
		/// <summary>
		/// 销售机会阶段 是阶段,不是状态
		/// </summary>
		public int? SalesOpportunitiesSate
		{
			set{ _salesopportunitiessate=value;}
			get{return _salesopportunitiessate;}
		}
		/// <summary>
		/// 回访备注
		/// </summary>
		public string BackNotes
		{
			set{ _backnotes=value;}
			get{return _backnotes;}
		}
		/// <summary>
		/// 回访状态 1-未回访，2-已回访，3-取消回访
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 是否下步跟进 -1-暂不处理,1-是,0-否
		/// </summary>
		public int? IsFollowUp
		{
			set{ _isfollowup=value;}
			get{return _isfollowup;}
		}
		/// <summary>
		/// 不跟进原因 (关闭原因,取消原因)
		/// </summary>
		public string CloseNotes
		{
			set{ _closenotes=value;}
			get{return _closenotes;}
		}
		/// <summary>
		/// 回访人
		/// </summary>
		public int? ReturnAccountId
		{
			set{ _returnaccountid=value;}
			get{return _returnaccountid;}
		}
		/// <summary>
		/// 实际回访时间
		/// </summary>
		public DateTime? ActualTime
		{
			set{ _actualtime=value;}
			get{return _actualtime;}
		}
		/// <summary>
		/// 计划回访时间
		/// </summary>
		public DateTime? PlanTime
		{
			set{ _plantime=value;}
			get{return _plantime;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

