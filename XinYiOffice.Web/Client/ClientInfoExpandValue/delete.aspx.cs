﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Client.ClientInfoExpandValue
{
    public partial class delete : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            			if (!Page.IsPostBack)
			{
				XinYiOffice.BLL.ClientInfoExpandValue bll=new XinYiOffice.BLL.ClientInfoExpandValue();
				 string idList = xytools.url_get("id");
                if (!string.IsNullOrEmpty(idList))
                {
                    idList = idList.TrimEnd(',');

                    bool isList = xyStringClass.StringIsExistIN(idList, ",");
                    if (isList)
                    {
                        bll.DeleteList(idList);
                    }
                    else
                    {
                        bll.Delete(SafeConvert.ToInt(idList, 0));
                    }

                    Response.Redirect("list.aspx");
                }
			}

        }
    }
}