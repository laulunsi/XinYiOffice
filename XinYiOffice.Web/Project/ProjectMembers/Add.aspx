﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Project.ProjectMembers.Add" Title="增加页" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
<script src="/js/ui/jquery.ui.core.js"></script>
	<script src="/js/ui/jquery.ui.widget.js"></script>
	<script src="/js/ui/jquery.ui.position.js"></script>
	<script src="/js/ui/jquery.ui.autocomplete.js"></script>
	<link rel="stylesheet" href="/js/themes/base/jquery.ui.all.css">
    <link rel="stylesheet" href="/js/themes/base/jquery.ui.autocomplete.css">

    <script>
        $(function () {

            var autotxtAccountId = $("#<%=txtAccountId.ClientID%>");
            var hidautoAccountId = $("#<%=HiddenField_AccountId.ClientID%>");
            var url_getwork = "/Call/Ajax.aspx?action=getofficeworkeraccount&sj=" + Math.random();


            //用户选择
            autotxtAccountId.autocomplete({
                source: url_getwork,
                select: function (event, ui) {
                    //alert(ui.item.value);
                    autotxtAccountId.val(ui.item.label);
                    hidautoAccountId.val(ui.item.value);
                    return false;
                },
                focus: function (event, ui) {
                    autotxtAccountId.val(ui.item.label);
                    return false;
                }
            });


        })
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form  id="f2" name="f2" runat="server">
    <div class="setup_box" style=" margin-bottom:0">
    	<div class="h_title"><h4>增加项目成员</h4></div>
        
<table cellSpacing="0" cellPadding="0" width="100%" border="0" class="plan_table">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		项目：</td>
	<td height="25" width="*" align="left">
		<asp:Label ID="Label_Project" runat="server" Text="Label"></asp:Label>
        <asp:HiddenField ID="HiddenField_ProjectId" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		添加用户：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtAccountId" runat="server" Width="200px"></asp:TextBox>
	    <asp:HiddenField ID="HiddenField_AccountId" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所担当的角色
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_ProjectRoleId" runat="server" Width="143px">
        </asp:DropDownList>
	</td></tr>
</table>
</div>
 <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
        
    </div>
    
    </form>
</asp:Content>

