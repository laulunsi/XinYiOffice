﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="XinYiOffice.Web.Index" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>XinYiOffice V<%=sysversion%></title>
<link href="/css/init.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.8.2.js"></script>
<script>
    $(function () {
        var num = 0;
        var oUl = $('#min_title_list');
        var hide_nav = $('#hide_nav');

        $('#min_title_list li').live('click', function () {
            var bStopIndex = $('#min_title_list li').index($(this));
            var iframe_box = $('#iframe_box');
            $('#min_title_list li').removeClass('act').eq(bStopIndex).addClass('act');
            iframe_box.find('.show_iframe').hide().eq(bStopIndex).show();
        });
        $('#min_title_list li a').live('click', function () {
            var aCloseIndex = $('#min_title_list li a').index($(this));
            if (aCloseIndex == 0) { return; }//桌面不能被关闭

            $(this).parent().remove();
            $('#iframe_box').find('.show_iframe').eq(aCloseIndex).remove();
            num == 0 ? num = 0 : num--;
            oUl.css({ 'left': -num * 116, 'width': oUl.find('li').length * 116 })
            if (oUl.width() < hide_nav.width()) {
                $('.lr').hide();
            }
        });


        $('.next').click(function () {
            num == oUl.find('li').length - 1 ? num = oUl.find('li').length - 1 : num++;
            toNavPos()
        });
        $('.prev').click(function () {
            num == 0 ? num = 0 : num--;
            toNavPos()
        });
        function toNavPos() {
            oUl.stop().animate({ 'left': -num * 116 }, 200)
        }



        $('#open_close a').click(function () {
            var _this = this;
            if ($(this).hasClass('toopen')) {
                $('#left_list').animate({ 'width': 190 }, 200, function () {
                    $(_this).removeClass('toopen')
                })
                $('#right_show').animate({ 'left': 190 }, 200);
            }
            else {
                $('#left_list').animate({ 'width': 10 }, 200, function () {
                    $(_this).addClass('toopen')
                })
                $('#right_show').animate({ 'left': 10 }, 200);
            }
        })




    })
</script>

</head>
<body>
<div id="top"><iframe src="Header.aspx" frameborder="0"></iframe></div>
<div id="left_list">
	<iframe src="LeftMent.aspx" name="leftlist" frameborder="0"></iframe>
    <div id="open_close">
        <a href="javascript:;"></a>
    </div>
</div>
<div id="right_show">
	<div class="show_nav">
    	<div id="hide_nav">
            <ul id="min_title_list" class="clear">
                <li class="act"><span>我的桌面</span><a href="javascript:;"></a></li>
            </ul>
        </div>
    	<div class="lr"><a href="javascript:;" class="prev"></a><a href="javascript:;" class="next"></a></div>
    </div>
	<div id="iframe_box">
    	<div class="show_iframe"><div style=" display:none" class="mb"></div><iframe frameborder="0" src="Desktop.aspx"></iframe></div>
    </div>
    <!--<iframe src="index_content.html" frameborder="0"></iframe>-->
</div>
<div id="maxcon"></div>
</body>
</html>