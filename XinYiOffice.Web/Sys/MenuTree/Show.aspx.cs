﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.MenuTree
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("MENUTREE_LIST_VIEW"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.MenuTree bll = new XinYiOffice.BLL.MenuTree();
            XinYiOffice.Model.MenuTree model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.lblGuid.Text = model.Guid;
            this.lblName.Text = model.Name;
            this.lblMenuTreeId.Text = model.MenuTreeId.ToString();
            this.lblChainedAddress.Text = model.ChainedAddress;
            this.lblIsShow.Text = model.IsShow.ToString();
            this.lblSort.Text = model.Sort.ToString();
            this.lblIcon.Text = model.Icon;
            this.lblIconMax.Text = model.IconMax;
            this.lblDescription.Text = model.Description;
            this.lblCreateAccountId.Text = model.CreateAccountId.ToString();
            this.lblRefreshAccountId.Text = model.RefreshAccountId.ToString();
            this.lblCreateTime.Text = model.CreateTime.ToString();
            this.lblRefreshTime.Text = model.RefreshTime.ToString();

        }


    }
}
