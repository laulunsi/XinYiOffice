﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 内信接收表 一个人有没有信件
	/// </summary>
	public partial class InternalReceiver
	{
		private readonly XinYiOffice.DAL.InternalReceiver dal=new XinYiOffice.DAL.InternalReceiver();
		public InternalReceiver()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.InternalReceiver model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.InternalReceiver model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.InternalReceiver GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.InternalReceiver GetModelByCache(int Id)
		{
			
			string CacheKey = "InternalReceiverModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.InternalReceiver)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.InternalReceiver> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.InternalReceiver> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.InternalReceiver> modelList = new List<XinYiOffice.Model.InternalReceiver>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.InternalReceiver model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.InternalReceiver();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["InternalLetterId"]!=null && dt.Rows[n]["InternalLetterId"].ToString()!="")
					{
						model.InternalLetterId=int.Parse(dt.Rows[n]["InternalLetterId"].ToString());
					}
					if(dt.Rows[n]["RecipientAccountId"]!=null && dt.Rows[n]["RecipientAccountId"].ToString()!="")
					{
						model.RecipientAccountId=int.Parse(dt.Rows[n]["RecipientAccountId"].ToString());
					}
					if(dt.Rows[n]["RecipientType"]!=null && dt.Rows[n]["RecipientType"].ToString()!="")
					{
						model.RecipientType=int.Parse(dt.Rows[n]["RecipientType"].ToString());
					}
					if(dt.Rows[n]["IsView"]!=null && dt.Rows[n]["IsView"].ToString()!="")
					{
						model.IsView=int.Parse(dt.Rows[n]["IsView"].ToString());
					}
					if(dt.Rows[n]["IsDelete"]!=null && dt.Rows[n]["IsDelete"].ToString()!="")
					{
						model.IsDelete=int.Parse(dt.Rows[n]["IsDelete"].ToString());
					}
					if(dt.Rows[n]["IsReply"]!=null && dt.Rows[n]["IsReply"].ToString()!="")
					{
						model.IsReply=int.Parse(dt.Rows[n]["IsReply"].ToString());
					}
					if(dt.Rows[n]["LetterPosition"]!=null && dt.Rows[n]["LetterPosition"].ToString()!="")
					{
						model.LetterPosition=int.Parse(dt.Rows[n]["LetterPosition"].ToString());
					}
					if(dt.Rows[n]["PositionFoler"]!=null && dt.Rows[n]["PositionFoler"].ToString()!="")
					{
					model.PositionFoler=dt.Rows[n]["PositionFoler"].ToString();
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["RefreshAccountId"]!=null && dt.Rows[n]["RefreshAccountId"].ToString()!="")
					{
						model.RefreshAccountId=int.Parse(dt.Rows[n]["RefreshAccountId"].ToString());
					}
					if(dt.Rows[n]["CheckDate"]!=null && dt.Rows[n]["CheckDate"].ToString()!="")
					{
						model.CheckDate=DateTime.Parse(dt.Rows[n]["CheckDate"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["RefreshTime"]!=null && dt.Rows[n]["RefreshTime"].ToString()!="")
					{
						model.RefreshTime=DateTime.Parse(dt.Rows[n]["RefreshTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

